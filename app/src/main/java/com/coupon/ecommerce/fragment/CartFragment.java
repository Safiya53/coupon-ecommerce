package com.coupon.ecommerce.fragment;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;


import androidx.annotation.Nullable;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.coupon.ecommerce.R;
import com.coupon.ecommerce.activity.LoginActivity;
import com.coupon.ecommerce.adapter.CartRecyclerviewAdapter;
import com.coupon.ecommerce.application.MyApplication;
import com.coupon.ecommerce.constants.ConstantCodes;
import com.coupon.ecommerce.database.DatabaseHandler;
import com.coupon.ecommerce.pojo.CartModel;
import com.coupon.ecommerce.utils.Utils;

import java.util.List;


public class CartFragment extends Fragment {

    private View mParentView;
    MyApplication mApplication;
    private Context mContext;
    private SharedPreferences mSharedPreference;
    private RelativeLayout mRelativeMain;
    private ProgressBar mProgressBar;
    private RecyclerView.LayoutManager mLayoutManager;
    private RecyclerView mRecyclerviewCart;
    DatabaseHandler db;
    private CartRecyclerviewAdapter adapter;
    private LinearLayout llCheckout;
    private TextView mTextviewTotalAmount, tvNoItems;
    Toolbar mToolbar;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable
            Bundle savedInstanceState) {
        mParentView = inflater.inflate(R.layout.fragment_cart, container, false);
        mApplication = (MyApplication) getActivity().getApplicationContext();
        mContext = getActivity();
        setHasOptionsMenu(true);
        mSharedPreference = PreferenceManager.getDefaultSharedPreferences(getActivity());

        db = new DatabaseHandler(mContext);
        mProgressBar = mParentView.findViewById(R.id.progressbar_cart);
        mRelativeMain = mParentView.findViewById(R.id.activity_main);
        mToolbar = getActivity().findViewById(R.id.toolbar);

        mTextviewTotalAmount = mParentView.findViewById(R.id.tvToPay);
        tvNoItems = mParentView.findViewById(R.id.tvNoItems);

        llCheckout = mParentView.findViewById(R.id.llCheckOut);
        llCheckout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mSharedPreference.getBoolean(ConstantCodes.IS_LOGIN, false)) {

                    if (db.getAllCart().size() <= 0) {
                        Utils.showSnackBar(getActivity().findViewById(android.R.id.content), "Sorry No Items in Cart");
                        llCheckout.setVisibility(View.GONE);
                        tvNoItems.setVisibility(View.VISIBLE);
                    } else {
                        mSharedPreference.edit().putString("cartTotalAmount", mTextviewTotalAmount.getText().toString()).apply();
                        Fragment fragment = new MainCheckoutFragmentForSteps();
                        FragmentTransaction ft = getFragmentManager().beginTransaction();
                        ft.replace(R.id.frame_container, fragment).addToBackStack("BillingAndDeliveryFragment").commit();
                    }
                } else {
                    Intent i = new Intent(mContext, LoginActivity.class);
                    startActivity(i);
                }
            }
        });

        mRecyclerviewCart = mParentView.findViewById(R.id.recyclerviewCart);

        mLayoutManager = new LinearLayoutManager(mContext);
        mRecyclerviewCart.setLayoutManager(mLayoutManager);

        //populate recyclerview
        populaterecyclerView();

        mToolbar = getActivity().findViewById(R.id.toolbar);
        mToolbar.setTitle("Cart");

        int totalPrice = 0;
        int cal = 0;
        for (int i = 0; i < db.getAllCart().size(); i++) {

            Log.e("sizeee", "" + db.getAllCart().size());
            Log.e("qtyyy", "" + Integer.parseInt(db.getAllCart().get(i).getProduct_quantity()));
            Log.e("pricee", "" + db.getAllCart().get(i).getProduct_price());
            cal = Integer.parseInt(db.getAllCart().get(i).getProduct_quantity()) * Integer.parseInt(db.getAllCart().get(i).getProduct_price());
            totalPrice += cal;
        }

        mTextviewTotalAmount.setText("Total Rs." + totalPrice);

        if (db.getAllCart().size() <= 0) {
            llCheckout.setVisibility(View.GONE);
            tvNoItems.setVisibility(View.VISIBLE);
        } else {
            llCheckout.setVisibility(View.VISIBLE);
            tvNoItems.setVisibility(View.GONE);
        }

       /* if (mSharedPreference.getBoolean(ConstantCodes.IS_LOGIN, false)) {
            llCheckout.setVisibility(View.VISIBLE);
        } else {
            llCheckout.setVisibility(View.GONE);
        }*/

        return mParentView;
    }

    private void populaterecyclerView() {

        if (db.getAllCart().size() > 0) {
            tvNoItems.setVisibility(View.GONE);
            adapter = new CartRecyclerviewAdapter(db.getAllCart(), mContext, mRecyclerviewCart, itemClickListener);
            mRecyclerviewCart.setAdapter(adapter);
        } else {
            mRecyclerviewCart.setAdapter(null);
            tvNoItems.setVisibility(View.VISIBLE);
        }

    }

    @Override
    public void onResume() {
        super.onResume();
    }

    CartRecyclerviewAdapter.ItemClickListener itemClickListener = new CartRecyclerviewAdapter.ItemClickListener() {
        @Override
        public void onItemClicked(int pos) {

            List<CartModel> cartModelList = db.getAllCart();
            CartModel cartModel = cartModelList.get(pos);
            db = new DatabaseHandler(mContext);
            CartModel cartModel1 = db.getCart(cartModel.getProduct_code());
            db.deleteCart(cartModel1.getProduct_id());
            adapter.notifyItemRemoved(pos);
            Toast.makeText(mContext, "Item removed from cart", Toast.LENGTH_SHORT).show();
            cartModelList.remove(pos);
            adapter.notifyDataSetChanged();

            populaterecyclerView();

            int totalPriceAfterClick = 0;
            for (int i = 0; i < cartModelList.size(); i++) {
                totalPriceAfterClick += Integer.parseInt(cartModelList.get(i).getProduct_price());
            }
            mTextviewTotalAmount.setText("Total Rs." + totalPriceAfterClick);

        }
    };

}
