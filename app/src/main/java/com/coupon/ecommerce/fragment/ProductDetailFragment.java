package com.coupon.ecommerce.fragment;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Paint;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;


import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.coupon.ecommerce.R;
import com.coupon.ecommerce.application.MyApplication;
import com.coupon.ecommerce.constants.ConstantCodes;
import com.coupon.ecommerce.database.DatabaseHandler;
import com.coupon.ecommerce.pojo.PojoAddWishlist;
import com.coupon.ecommerce.pojo.PojoProductList;
import com.coupon.ecommerce.utils.Utils;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


/**
 * Created by Rp on 8/30/2016.
 */
public class ProductDetailFragment extends Fragment {

    private View mParentView;

    private Context mContext;
    RelativeLayout mRelativeMain;
    ProgressBar mProgressBar;

    MyApplication mApplication;
    private SharedPreferences mSharedPreference;

    RecyclerView rvProductPhotos, rvReviews;

    /*ProductDetailPhotoAdapter productDetailPhotoAdapter;
    ProductDetailReviewAdapter productDetailReviewAdapter;*/

    List<PojoProductList.Datum> mArrayProductList = new ArrayList<>();

    TextView tvPlus, tvMinus, tvQty, btnAddToCart, tvCancelPrice;

    DatabaseHandler db;

    ImageView ivProductImage;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mParentView = inflater.inflate(R.layout.fragment_product_detail, container, false);
        mApplication = (MyApplication) getActivity().getApplicationContext();
        mContext = getActivity();
        setHasOptionsMenu(true);
        mSharedPreference = PreferenceManager.getDefaultSharedPreferences(getActivity());
        db = new DatabaseHandler(mContext);


        initialise();
        listners();

        //networkCallProductDetails();

        return mParentView;

    }


    void initialise() {

        Toolbar mToolbar = (Toolbar) getActivity().findViewById(R.id.toolbar);
        mToolbar.setTitle("Products Detail");

        mRelativeMain = mParentView.findViewById(R.id.relative_main);
        mProgressBar = mParentView.findViewById(R.id.progressbar);

        tvPlus = mParentView.findViewById(R.id.textview_product_detail_plus);
        tvQty = mParentView.findViewById(R.id.textview_product_detail_quantity);
        tvMinus = mParentView.findViewById(R.id.textview_product_detail_minus);
        btnAddToCart = mParentView.findViewById(R.id.btnAddToCart);

        tvCancelPrice = (TextView) mParentView.findViewById(R.id.tvCancelPrice);
        tvCancelPrice.setPaintFlags(tvCancelPrice.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);

        ivProductImage = mParentView.findViewById(R.id.ivProductImage);

        rvProductPhotos = mParentView.findViewById(R.id.rvProductPhotos);
        RecyclerView.LayoutManager mLayoutManagerHappyHours1 = new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false);
        rvProductPhotos.setLayoutManager(mLayoutManagerHappyHours1);
        rvProductPhotos.setNestedScrollingEnabled(false);
        rvProductPhotos.setItemAnimator(new DefaultItemAnimator());
        rvProductPhotos.setHasFixedSize(false);

        String[] names = {"dkj", "djkf", "lijg", "if"};
      /*  productDetailPhotoAdapter = new ProductDetailPhotoAdapter(mContext, names);

        rvProductPhotos.setAdapter(productDetailPhotoAdapter);
*/

        rvReviews = mParentView.findViewById(R.id.rvReviews);
        RecyclerView.LayoutManager mLayoutManagerReview = new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false);
        rvReviews.setLayoutManager(mLayoutManagerReview);
        rvReviews.setNestedScrollingEnabled(false);
        rvReviews.setItemAnimator(new DefaultItemAnimator());
        rvReviews.setHasFixedSize(false);


        String[] reviews = {"dkj", "djkf", "lijg", "if"};
       /* productDetailReviewAdapter = new ProductDetailReviewAdapter(mContext, reviews);

        rvReviews.setAdapter(productDetailReviewAdapter);*/
    }

    void listners() {

        tvPlus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int i = Integer.parseInt(tvQty.getText().toString());
                int cnt = i + 1;
                tvQty.setText(cnt + "");
            }
        });

        tvMinus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int i = Integer.parseInt(tvQty.getText().toString());
                if (i != 1) {
                    int cnt = i - 1;
                    tvQty.setText(cnt + "");
                }
            }
        });

        btnAddToCart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

              //  db.addCart(new CartModel("1" + new Date(), "Name", "200", "10"));

                Toast.makeText(mContext, " Item added to cart", Toast.LENGTH_SHORT).show();

            }
        });

    }


   /* void networkCallProductDetails() {
        if (mApplication.isInternetConnected()) {
            if (mSharedPreference.getBoolean(ConstantCodes.IS_LOGIN, false)) {
                mApplication.getRetroFitInterface().ProductDetails(ConstantCodes.API_ACCESS_TOKEN,
                        getArguments().getString("productId"),
                        mSharedPreference.getString(ConstantCodes.LOGIN_USER_ID, "")).enqueue(mCallbackProductList);
            } else {
                mApplication.getRetroFitInterface().ProductDetails(ConstantCodes.API_ACCESS_TOKEN,
                        getArguments().getString("productId"),
                        "0").enqueue(mCallbackProductList);
            }


            mProgressBar.setVisibility(View.VISIBLE);
        } else {
            Utils.showSnackBar(getActivity().findViewById(android.R.id.content), mContext.getResources().getString(R.string
                    .message_connection));
            mProgressBar.setVisibility(View.GONE);
        }
    }

    *//**
     * Callback for network call for sub sub category
     *//*
    private Callback<PojoProductDetails> mCallbackProductList = new Callback<PojoProductDetails>() {

        @Override
        public void onResponse(Call<PojoProductDetails> call, Response<PojoProductDetails>
                response) {
            if (response != null && response.isSuccessful() && response.body() != null) {
                PojoProductDetails pojoParticipants = response.body();
                if (isAdded()) {

                    if (pojoParticipants.getStatus()) {


                    } else {
                        Utils.showSnackBar(mRelativeMain, getString(R.string.message_something_wrong));
                    }

                }
            }
            mProgressBar.setVisibility(View.GONE);
        }

        @Override
        public void onFailure(Call<PojoProductDetails> call, Throwable t) {
            Utils.showSnackBar(mRelativeMain, getString(R.string
                    .message_something_wrong));
            mProgressBar.setVisibility(View.GONE);

        }
    };
*/
    void networkCallAddWishlist() {
        if (mApplication.isInternetConnected()) {

            mApplication.getRetroFitInterface().AddToWishlist(ConstantCodes.API_ACCESS_TOKEN,
                    mSharedPreference.getString(ConstantCodes.LOGIN_USER_ID, ""),
                    getArguments().getString("productId")
            ).enqueue(mCallbackAddWishlist);


            mProgressBar.setVisibility(View.VISIBLE);
        } else {
            Utils.showSnackBar(getActivity().findViewById(android.R.id.content), mContext.getResources().getString(R.string
                    .message_connection));
            mProgressBar.setVisibility(View.GONE);
        }
    }

    /**
     * Callback for network call for sub sub category
     */
    private Callback<PojoAddWishlist> mCallbackAddWishlist = new Callback<PojoAddWishlist>() {

        @Override
        public void onResponse(Call<PojoAddWishlist> call, Response<PojoAddWishlist>
                response) {
            if (response != null && response.isSuccessful() && response.body() != null) {
                PojoAddWishlist pojoParticipants = response.body();
                if (isAdded()) {

                    if (pojoParticipants.getStatus()) {


                    } else {
                        Utils.showSnackBar(mRelativeMain, getString(R.string.message_something_wrong));
                    }

                }
            }
            mProgressBar.setVisibility(View.GONE);
        }

        @Override
        public void onFailure(Call<PojoAddWishlist> call, Throwable t) {
            Utils.showSnackBar(mRelativeMain, getString(R.string
                    .message_something_wrong));
            mProgressBar.setVisibility(View.GONE);

        }
    };


    void networkCallRemoveWishlist() {
        if (mApplication.isInternetConnected()) {

            mApplication.getRetroFitInterface().RemoveToWishlist(ConstantCodes.API_ACCESS_TOKEN,
                    mSharedPreference.getString(ConstantCodes.LOGIN_USER_ID, ""),
                    getArguments().getString("productId")
            ).enqueue(mCallbackRemoveWishlist);


            mProgressBar.setVisibility(View.VISIBLE);
        } else {
            Utils.showSnackBar(getActivity().findViewById(android.R.id.content), mContext.getResources().getString(R.string
                    .message_connection));
            mProgressBar.setVisibility(View.GONE);
        }
    }

    /**
     * Callback for network call for sub sub category
     */
    private Callback<PojoAddWishlist> mCallbackRemoveWishlist = new Callback<PojoAddWishlist>() {

        @Override
        public void onResponse(Call<PojoAddWishlist> call, Response<PojoAddWishlist>
                response) {
            if (response != null && response.isSuccessful() && response.body() != null) {
                PojoAddWishlist pojoParticipants = response.body();
                if (isAdded()) {

                    if (pojoParticipants.getStatus()) {


                    } else {
                        Utils.showSnackBar(mRelativeMain, getString(R.string.message_something_wrong));
                    }

                }
            }
            mProgressBar.setVisibility(View.GONE);
        }

        @Override
        public void onFailure(Call<PojoAddWishlist> call, Throwable t) {
            Utils.showSnackBar(mRelativeMain, getString(R.string
                    .message_something_wrong));
            mProgressBar.setVisibility(View.GONE);

        }
    };


}
