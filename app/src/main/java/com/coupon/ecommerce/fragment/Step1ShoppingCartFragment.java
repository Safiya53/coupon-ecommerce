package com.coupon.ecommerce.fragment;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.coupon.ecommerce.R;
import com.coupon.ecommerce.adapter.CartRecyclerviewAdapter;
import com.coupon.ecommerce.application.MyApplication;
import com.coupon.ecommerce.constants.ConstantCodes;
import com.coupon.ecommerce.database.DatabaseHandler;
import com.coupon.ecommerce.pojo.CartModel;
import com.coupon.ecommerce.utils.Utils;
import com.stepstone.stepper.BlockingStep;
import com.stepstone.stepper.StepperLayout;
import com.stepstone.stepper.VerificationError;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


/**
 * Created by Rp on 8/30/2016.
 */
public class Step1ShoppingCartFragment extends Fragment implements BlockingStep {

    private View mParentView;

    private Context mContext;
    RelativeLayout mRelativeMain;
    ProgressBar mProgressBar;
    MyApplication mApplication;
    private SharedPreferences mSharedPreference;
    RecyclerView rvProductList;
    private CartRecyclerviewAdapter cartRecyclerviewAdapter;
    DatabaseHandler db;
    TextView tvSubTotal, tvShippingCharges, tvDiscount, tvGrandTotal, tvApplyPromo;
    EditText etPromoCode;

    double subPrice = 0;
    double shippingPrice = 0;
    double grandTotal = 0;
    double discountPrice = 0;
    String promoCode = "";

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mParentView = inflater.inflate(R.layout.fragment_checkout_shopping_cart, container, false);
        mApplication = (MyApplication) getActivity().getApplicationContext();
        mContext = getActivity();
        setHasOptionsMenu(true);
        mSharedPreference = PreferenceManager.getDefaultSharedPreferences(getActivity());
        db = new DatabaseHandler(mContext);

        initialise();
        listners();


        return mParentView;

    }


    void initialise() {

        mRelativeMain = mParentView.findViewById(R.id.relative_main);
        mProgressBar = mParentView.findViewById(R.id.progressbar);

        tvSubTotal = mParentView.findViewById(R.id.tvSubTotal);
        tvShippingCharges = mParentView.findViewById(R.id.tvShippingCharges);
        tvDiscount = mParentView.findViewById(R.id.tvDiscount);
        tvGrandTotal = mParentView.findViewById(R.id.tvGrandTotal);
        etPromoCode = mParentView.findViewById(R.id.etPromoCode);
        tvApplyPromo = mParentView.findViewById(R.id.tvApplyPromo);

        subPrice = 0;
        shippingPrice = 0;
        grandTotal = 0;
        discountPrice = 0;
        int cal = 0;
        for (int i = 0; i < db.getAllCart().size(); i++) {

            Log.e("sizeee", "" + db.getAllCart().size());
            Log.e("qtyyy", "" + Integer.parseInt(db.getAllCart().get(i).getProduct_quantity()));
            Log.e("pricee", "" + db.getAllCart().get(i).getProduct_price());
            cal = Integer.parseInt(db.getAllCart().get(i).getProduct_quantity()) * Integer.parseInt(db.getAllCart().get(i).getProduct_price());
            subPrice += cal;
        }

        tvSubTotal.setText("\u20B9 " + subPrice);

        if (subPrice < 600) {
            shippingPrice = 30;
            tvShippingCharges.setText("\u20B9 30");
        } else {
            tvShippingCharges.setText("Free Shipping");
        }

        grandTotal = subPrice + shippingPrice + discountPrice;
        tvGrandTotal.setText("\u20B9 " + grandTotal);

        rvProductList = mParentView.findViewById(R.id.recyclerviewCart);

        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(mContext);
        rvProductList.setLayoutManager(mLayoutManager);

        cartRecyclerviewAdapter = new CartRecyclerviewAdapter(db.getAllCart(), mContext, rvProductList, itemClickListener);
        rvProductList.setAdapter(cartRecyclerviewAdapter);


    }

    void listners() {

        tvApplyPromo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!etPromoCode.getText().toString().equals("")) {
                    Utils.hideKeyboard(getActivity());
                 //   networkCallCheckPromoCode(etPromoCode.getText().toString() + "");
                } else {
                    Utils.showSnackBar(mRelativeMain, "Please Enter Promo Code");
                }
            }
        });

    }

    CartRecyclerviewAdapter.ItemClickListener itemClickListener = new CartRecyclerviewAdapter.ItemClickListener() {
        @Override
        public void onItemClicked(int pos) {

            List<CartModel> cartModelList = db.getAllCart();
            CartModel cartModel = cartModelList.get(pos);
            db = new DatabaseHandler(mContext);
            CartModel cartModel1 = db.getCart(cartModel.getProduct_code());
            db.deleteCart(cartModel1.getProduct_id());
            cartRecyclerviewAdapter.notifyItemRemoved(pos);
            Toast.makeText(mContext, "Item removed from cart", Toast.LENGTH_SHORT).show();
            cartModelList.remove(pos);
            cartRecyclerviewAdapter.notifyDataSetChanged();

            cartRecyclerviewAdapter = new CartRecyclerviewAdapter(db.getAllCart(), mContext, rvProductList, itemClickListener);
            rvProductList.setAdapter(cartRecyclerviewAdapter);

            if (cartModelList.size() > 0) {
                int totalPriceAfterClick = 0;
                for (int i = 0; i < cartModelList.size(); i++) {
                    totalPriceAfterClick += Integer.parseInt(db.getAllCart().get(i).getProduct_quantity()) * Integer.parseInt(db.getAllCart().get(i).getProduct_price());
                }

                subPrice = 0;
                shippingPrice = 0;
                grandTotal = 0;

                subPrice = totalPriceAfterClick;

                tvSubTotal.setText("\u20B9 " + subPrice);

                if (subPrice < 600) {
                    shippingPrice = 30;
                    tvShippingCharges.setText("\u20B9 30");
                } else {
                    tvShippingCharges.setText("Free Shipping");
                }

                grandTotal = subPrice + shippingPrice + discountPrice;
                tvGrandTotal.setText("\u20B9 " + grandTotal);
            } else {

                subPrice = 0;
                shippingPrice = 0;
                grandTotal = 0;
                discountPrice = 0;

                tvSubTotal.setText("\u20B9 0");
                tvShippingCharges.setText("\u20B9 0");
                tvDiscount.setText("\u20B9 0");
                tvGrandTotal.setText("\u20B9 0");

            }


            tvApplyPromo.performClick();

            //mTextviewTotalAmount.setText("Total Rs." + totalPriceAfterClick);

        }
    };

    /*void networkCallCheckPromoCode(String promoCode) {
        if (mApplication.isInternetConnected()) {

            mApplication.getRetroFitInterface().CheckPromoCode(ConstantCodes.API_ACCESS_TOKEN,
                    promoCode, subPrice + "").enqueue(mCallbackPromo);


            mProgressBar.setVisibility(View.VISIBLE);
        } else {
            Utils.showSnackBar(getActivity().findViewById(android.R.id.content), mContext.getResources().getString(R.string
                    .message_connection));
            mProgressBar.setVisibility(View.GONE);
        }
    }

    *//**
     * Callback for network call for sub sub category
     *//*
    private Callback<PojoCheckPromo> mCallbackPromo = new Callback<PojoCheckPromo>() {

        @Override
        public void onResponse(Call<PojoCheckPromo> call, Response<PojoCheckPromo>
                response) {
            if (response != null && response.isSuccessful() && response.body() != null) {
                PojoCheckPromo pojoParticipants = response.body();
                if (isAdded()) {

                    if (pojoParticipants.getStatus()) {

                        promoCode = etPromoCode.getText().toString();

                        discountPrice = Double.parseDouble(pojoParticipants.getDiscount());
                        if (!TextUtils.isEmpty(pojoParticipants.getMsg())) {
                            Utils.showSnackBar(mRelativeMain, pojoParticipants.getMsg());
                        } else {
                            Utils.showSnackBar(mRelativeMain, getString(R.string
                                    .message_something_wrong));
                        }

                        tvDiscount.setText("\u20B9 " + discountPrice);
                        grandTotal = subPrice + shippingPrice + discountPrice;
                        tvGrandTotal.setText("\u20B9 " + grandTotal);


                    } else {
                        promoCode = "";
                        discountPrice = 0;
                        tvDiscount.setText("\u20B9 0");
                        if (!TextUtils.isEmpty(pojoParticipants.getMsg())) {
                            Utils.showSnackBar(mRelativeMain, pojoParticipants.getMsg());
                        } else {
                            Utils.showSnackBar(mRelativeMain, getString(R.string
                                    .message_something_wrong));
                        }
                    }

                }
            }
            mProgressBar.setVisibility(View.GONE);
        }

        @Override
        public void onFailure(Call<PojoCheckPromo> call, Throwable t) {
            Utils.showSnackBar(mRelativeMain, getString(R.string
                    .message_something_wrong));
            mProgressBar.setVisibility(View.GONE);

        }
    };*/

    @Override
    public void onNextClicked(StepperLayout.OnNextClickedCallback onNextClickedCallback) {

        db = new DatabaseHandler(mContext);
        if (db.getAllCart().size() > 0) {

            mSharedPreference.edit().putString(ConstantCodes.ORDER_SUB_TOTAL, subPrice + "").apply();
            mSharedPreference.edit().putString(ConstantCodes.ORDER_GRAND_TOTAL, grandTotal + "").apply();
            mSharedPreference.edit().putString(ConstantCodes.ORDER_DISCOUNT, discountPrice + "").apply();
            mSharedPreference.edit().putString(ConstantCodes.ORDER_PROMO_CODE, promoCode).apply();

            onNextClickedCallback.goToNextStep();
        } else {
            Utils.showSnackBar(mRelativeMain, "Please Add Item To Cart");
        }

    }

    @Override
    public void onCompleteClicked(StepperLayout.OnCompleteClickedCallback onCompleteClickedCallback) {

    }

    @Override
    public void onBackClicked(StepperLayout.OnBackClickedCallback onBackClickedCallback) {

    }

    @Nullable
    @Override
    public VerificationError verifyStep() {
        return null;
    }

    @Override
    public void onSelected() {

    }

    @Override
    public void onError(@NonNull VerificationError verificationError) {

    }
}
