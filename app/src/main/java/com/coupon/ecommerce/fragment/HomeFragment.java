package com.coupon.ecommerce.fragment;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Patterns;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;


import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.coupon.ecommerce.R;
import com.coupon.ecommerce.adapter.HomeLatestProductAdapter;
import com.coupon.ecommerce.adapter.HomeOurCategoryAdapter;
import com.coupon.ecommerce.application.MyApplication;
import com.coupon.ecommerce.constants.ConstantCodes;
import com.coupon.ecommerce.pojo.PojoHomeSection1;
import com.coupon.ecommerce.pojo.PojoHomeSection2;
import com.coupon.ecommerce.utils.Utils;
import com.smarteist.autoimageslider.SliderView;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


/**
 * Created by Rp on 8/30/2016.
 */
public class HomeFragment extends Fragment {

    private View mParentView;

    private Context mContext;
    RelativeLayout mRelativeMain;
    ProgressBar mProgressBar;

    MyApplication mApplication;
    private SharedPreferences mSharedPreference;

    SliderView sliderView1, sliderView2;

    TextView tvShopByCategory;

    RecyclerView rvLatestProducts, rvOurCategory;

    HomeLatestProductAdapter homeLatestProductAdapter;
    HomeOurCategoryAdapter homeOurCategoryAdapter;

    List<PojoHomeSection1.Banner> mArrayBannerFirst = new ArrayList<>();
    List<PojoHomeSection1.OfferBanner> mArrayBannerOffer = new ArrayList<>();
    List<PojoHomeSection1.LatestProduct> mArrayLatestProduct = new ArrayList<>();
    List<PojoHomeSection2.TopCategory> mArrayTopCategory = new ArrayList<>();
    List<PojoHomeSection2.SpecialOfferImage> mArrayBannerSpecialOffer = new ArrayList<>();

    EditText etNewsLetterEmail;
    TextView tvNewsletterSubmit;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mParentView = inflater.inflate(R.layout.fragment_home, container, false);
        mApplication = (MyApplication) getActivity().getApplicationContext();
        mContext = getActivity();
        setHasOptionsMenu(true);
        mSharedPreference = PreferenceManager.getDefaultSharedPreferences(getActivity());


        initialise();
        listners();

       /* networkCallSection1();
        networkCallSection2();
        networkCallSection3();*/

        return mParentView;

    }


    void initialise() {

        Toolbar mToolbar = (Toolbar) getActivity().findViewById(R.id.toolbar);
        mToolbar.setTitle("Home");

        mRelativeMain = mParentView.findViewById(R.id.relative_main);
        mProgressBar = mParentView.findViewById(R.id.progressbar);

        tvNewsletterSubmit = mParentView.findViewById(R.id.tvNewsletterSubmit);
        etNewsLetterEmail = mParentView.findViewById(R.id.etNewsLetterEmail);

        sliderView1 = mParentView.findViewById(R.id.imageSlider1);
        sliderView2 = mParentView.findViewById(R.id.imageSlider2);

        tvShopByCategory = mParentView.findViewById(R.id.tvShopByCategory);

        rvLatestProducts = mParentView.findViewById(R.id.rvLatestProducts);

        LinearLayoutManager mLLatestProduct = new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false);
        rvLatestProducts.setLayoutManager(mLLatestProduct);
        rvLatestProducts.setNestedScrollingEnabled(false);
        rvLatestProducts.setItemAnimator(new DefaultItemAnimator());
        rvLatestProducts.setHasFixedSize(false);

        homeLatestProductAdapter = new HomeLatestProductAdapter(mContext, mArrayLatestProduct, latestProductClick);
        rvLatestProducts.setAdapter(homeLatestProductAdapter);


        rvOurCategory = mParentView.findViewById(R.id.rvOurCategory);

        GridLayoutManager mOurCategory = new GridLayoutManager(getActivity(), 2);
        rvOurCategory.setLayoutManager(mOurCategory);
        rvOurCategory.setNestedScrollingEnabled(false);
        rvOurCategory.setItemAnimator(new DefaultItemAnimator());
        rvOurCategory.setHasFixedSize(false);

        homeOurCategoryAdapter = new HomeOurCategoryAdapter(mContext, mArrayTopCategory, ourCategoryClick);
        rvOurCategory.setAdapter(homeOurCategoryAdapter);

    }

    void listners() {

        tvShopByCategory.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Fragment fragment = new CategoryFragment();
                FragmentTransaction ft = getFragmentManager().beginTransaction();
                ft.replace(R.id.frame_container, fragment).addToBackStack("").commit();
            }
        });

        tvNewsletterSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!etNewsLetterEmail.getText().toString().equals("")) {
                    if (Patterns.EMAIL_ADDRESS.matcher(etNewsLetterEmail.getText().toString().trim()).matches()) {
                        //  networkCallNewsletterSubscription();
                    } else {
                        Utils.showSnackBar(mRelativeMain, "Enter Valid Email Address");
                    }
                } else {
                    Utils.showSnackBar(mRelativeMain, "Enter Email Address");
                }
            }
        });

    }


   /* void networkCallSection1() {
        if (mApplication.isInternetConnected()) {
            mApplication.getRetroFitInterface().HomeSection1(
                    ConstantCodes.API_ACCESS_TOKEN)
                    .enqueue(mCallbackSection1);

            mProgressBar.setVisibility(View.VISIBLE);

        } else {
            Utils.showSnackBar(getActivity().findViewById(android.R.id.content), mContext.getResources().getString(R.string
                    .message_connection));
            mProgressBar.setVisibility(View.GONE);
        }
    }

    *//**
     * Callback for network call for sub sub category
     *//*
    private Callback<PojoHomeSection1> mCallbackSection1 = new Callback<PojoHomeSection1>() {

        @Override
        public void onResponse(Call<PojoHomeSection1> call, Response<PojoHomeSection1>
                response) {
            if (response != null && response.isSuccessful() && response.body() != null) {
                PojoHomeSection1 pojoParticipants = response.body();
                if (isAdded()) {
                    if (pojoParticipants.getStatus()) {

                        if (pojoParticipants.getData() != null) {

                            mArrayBannerFirst.clear();
                            mArrayLatestProduct.clear();
                            mArrayBannerOffer.clear();

                            if (pojoParticipants.getData().getBanners() != null) {
                                if (pojoParticipants.getData().getBanners().size() > 0) {
                                    mArrayBannerFirst.addAll(pojoParticipants.getData().getBanners());
                                    getSliderFirst();
                                }
                            }

                            if (pojoParticipants.getData().getLatestProducts() != null) {
                                if (pojoParticipants.getData().getLatestProducts().size() > 0) {
                                    mArrayLatestProduct.addAll(pojoParticipants.getData().getLatestProducts());

                                    homeLatestProductAdapter = new HomeLatestProductAdapter(mContext, mArrayLatestProduct, latestProductClick);
                                    rvLatestProducts.setAdapter(homeLatestProductAdapter);
                                }
                            }

                            if (pojoParticipants.getData().getOfferBanners() != null) {
                                if (pojoParticipants.getData().getOfferBanners().size() > 0) {
                                    mArrayBannerOffer.addAll(pojoParticipants.getData().getOfferBanners());
                                    getSliderOffer();
                                }
                            }
                        }

                    }
                }
            }

            mProgressBar.setVisibility(View.GONE);
        }

        @Override
        public void onFailure(Call<PojoHomeSection1> call, Throwable t) {
            Utils.showSnackBar(mRelativeMain, getString(R.string
                    .message_something_wrong));
            mProgressBar.setVisibility(View.GONE);

        }
    };


    void networkCallSection2() {
        if (mApplication.isInternetConnected()) {
            mApplication.getRetroFitInterface().HomeSection2(
                    ConstantCodes.API_ACCESS_TOKEN)
                    .enqueue(mCallbackSection2);


            mProgressBar.setVisibility(View.VISIBLE);

        } else {
            Utils.showSnackBar(getActivity().findViewById(android.R.id.content), mContext.getResources().getString(R.string
                    .message_connection));
            mProgressBar.setVisibility(View.GONE);
        }
    }

    *//**
     * Callback for network call for sub sub category
     *//*
    private Callback<PojoHomeSection2> mCallbackSection2 = new Callback<PojoHomeSection2>() {

        @Override
        public void onResponse(Call<PojoHomeSection2> call, Response<PojoHomeSection2>
                response) {
            if (response != null && response.isSuccessful() && response.body() != null) {
                PojoHomeSection2 pojoParticipants = response.body();
                if (isAdded()) {
                    if (pojoParticipants.getStatus()) {

                        if (pojoParticipants.getData() != null) {

                            mArrayTopCategory.clear();
                            mArrayTopSelling.clear();
                            mArrayBannerSpecialOffer.clear();

                            if (pojoParticipants.getData().getTopCategories() != null) {
                                if (pojoParticipants.getData().getTopCategories().size() > 0) {
                                    mArrayTopCategory.addAll(pojoParticipants.getData().getTopCategories());

                                    homeOurCategoryAdapter = new HomeOurCategoryAdapter(mContext, mArrayTopCategory, ourCategoryClick);
                                    rvOurCategory.setAdapter(homeOurCategoryAdapter);
                                }
                            }

                            if (pojoParticipants.getData().getTopSellingProducts() != null) {
                                if (pojoParticipants.getData().getTopSellingProducts().size() > 0) {
                                    mArrayTopSelling.addAll(pojoParticipants.getData().getTopSellingProducts());

                                    homeTopSellingProductAdapter = new HomeTopSellingProductAdapter(mContext, mArrayTopSelling, topSellingProductClick);
                                    rvTopSelling.setAdapter(homeTopSellingProductAdapter);
                                }
                            }

                            if (pojoParticipants.getData().getSpecialOfferImages() != null) {
                                if (pojoParticipants.getData().getSpecialOfferImages().size() > 0) {
                                    mArrayBannerSpecialOffer.addAll(pojoParticipants.getData().getSpecialOfferImages());
                                    getSliderSpecialOffer();
                                }
                            }


                        }

                    }
                }
            }

            mProgressBar.setVisibility(View.GONE);
        }

        @Override
        public void onFailure(Call<PojoHomeSection2> call, Throwable t) {
            Utils.showSnackBar(mRelativeMain, getString(R.string
                    .message_something_wrong));
            mProgressBar.setVisibility(View.GONE);

        }
    };

    void networkCallSection3() {
        if (mApplication.isInternetConnected()) {
            mApplication.getRetroFitInterface().HomeSection3(
                    ConstantCodes.API_ACCESS_TOKEN)
                    .enqueue(mCallbackSection3);


            mProgressBar.setVisibility(View.VISIBLE);

        } else {
            Utils.showSnackBar(getActivity().findViewById(android.R.id.content), mContext.getResources().getString(R.string
                    .message_connection));
            mProgressBar.setVisibility(View.GONE);
        }
    }

    *//**
     * Callback for network call for sub sub category
     *//*
    private Callback<PojoHomeSection3> mCallbackSection3 = new Callback<PojoHomeSection3>() {

        @Override
        public void onResponse(Call<PojoHomeSection3> call, Response<PojoHomeSection3>
                response) {
            if (response != null && response.isSuccessful() && response.body() != null) {
                PojoHomeSection3 pojoParticipants = response.body();
                if (isAdded()) {
                    if (pojoParticipants.getStatus()) {

                        if (pojoParticipants.getData() != null) {

                            mArrayFeaturedProduct.clear();
                            mArrayBrandImage.clear();

                            if (pojoParticipants.getData().getFeatureProducts() != null) {
                                if (pojoParticipants.getData().getFeatureProducts().size() > 0) {
                                    mArrayFeaturedProduct.addAll(pojoParticipants.getData().getFeatureProducts());

                                    homeFeaturedProductAdapter = new HomeFeaturedProductAdapter(mContext, mArrayFeaturedProduct, featuredProductClick);
                                    rvFeaturedProducts.setAdapter(homeFeaturedProductAdapter);
                                }
                            }

                            if (pojoParticipants.getData().getBrandImages() != null) {
                                if (pojoParticipants.getData().getBrandImages().size() > 0) {
                                    mArrayBrandImage.addAll(pojoParticipants.getData().getBrandImages());

                                    homeOurBrandAdapter = new HomeOurBrandAdapter(mContext, mArrayBrandImage, brandClick);
                                    rvBrands.setAdapter(homeOurBrandAdapter);
                                }
                            }

                        }

                    }
                }
            }

            mProgressBar.setVisibility(View.GONE);
        }

        @Override
        public void onFailure(Call<PojoHomeSection3> call, Throwable t) {
            Utils.showSnackBar(mRelativeMain, getString(R.string
                    .message_something_wrong));
            mProgressBar.setVisibility(View.GONE);

        }
    };


    void networkCallNewsletterSubscription() {
        if (mApplication.isInternetConnected()) {

            mApplication.getRetroFitInterface().NewsletterSubscription(ConstantCodes.API_ACCESS_TOKEN,
                    etNewsLetterEmail.getText().toString()).enqueue(mCallbackData);


            mProgressBar.setVisibility(View.VISIBLE);
        } else {
            Utils.showSnackBar(getActivity().findViewById(android.R.id.content), mContext.getResources().getString(R.string
                    .message_connection));
            mProgressBar.setVisibility(View.GONE);
        }
    }

    */
    /**
     * Callback for network call for sub sub category
     *//*
    private Callback<PojoSubscribe> mCallbackData = new Callback<PojoSubscribe>() {

        @Override
        public void onResponse(Call<PojoSubscribe> call, Response<PojoSubscribe>
                response) {
            if (response != null && response.isSuccessful() && response.body() != null) {
                PojoSubscribe pojoParticipants = response.body();
                if (isAdded()) {

                    Utils.showSnackBar(mRelativeMain, "" + pojoParticipants.getMessage());
                    etNewsLetterEmail.setText("");

                }
            }
            mProgressBar.setVisibility(View.GONE);
        }

        @Override
        public void onFailure(Call<PojoSubscribe> call, Throwable t) {
            Utils.showSnackBar(mRelativeMain, getString(R.string
                    .message_something_wrong));
            mProgressBar.setVisibility(View.GONE);

        }
    };

*/

    HomeLatestProductAdapter.CustomClick latestProductClick = new HomeLatestProductAdapter.CustomClick() {
        @Override
        public void itemClick(String productId, int type) {

        }
    };

    HomeOurCategoryAdapter.CustomClick ourCategoryClick = new HomeOurCategoryAdapter.CustomClick() {
        @Override
        public void itemClick(int id) {

          /*  Fragment fragment = new ProductFragment();
            FragmentTransaction ft = getFragmentManager().beginTransaction();

            Bundle b = new Bundle();
            b.putString("catId", "" + id);

            fragment.setArguments(b);

            ft.replace(R.id.frame_container, fragment).addToBackStack("").commit();*/

        }
    };

}
