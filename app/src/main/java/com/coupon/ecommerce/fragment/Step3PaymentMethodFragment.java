package com.coupon.ecommerce.fragment;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.RelativeLayout;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.coupon.ecommerce.R;
import com.coupon.ecommerce.application.MyApplication;
import com.coupon.ecommerce.constants.ConstantCodes;
import com.coupon.ecommerce.database.DatabaseHandler;
import com.coupon.ecommerce.pojo.CartModel;
import com.coupon.ecommerce.pojo.PojoOrderSubmitObject;
import com.coupon.ecommerce.pojo.PojoSubmitOrder;
import com.coupon.ecommerce.utils.Utils;
import com.stepstone.stepper.BlockingStep;
import com.stepstone.stepper.StepperLayout;
import com.stepstone.stepper.VerificationError;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Set;
import java.util.UUID;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


/**
 * Created by Rp on 8/30/2016.
 */
public class Step3PaymentMethodFragment extends Fragment implements BlockingStep/*, PaytmPaymentTransactionCallback */{

    private static final String TAG = "paytmactivity";

    private View mParentView;

    private Context mContext;
    RelativeLayout mRelativeMain;
    ProgressBar mProgressBar;

    MyApplication mApplication;
    private SharedPreferences mSharedPreference;
    RadioButton rbCod, rbNetBanking;

    private String merchantId, orderId, customerId, channelId, website,
            callbackUrl, taxAmount = "", industryType, checksumHash;

    PojoOrderSubmitObject pojoOrderSubmitObject;

    DatabaseHandler db;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mParentView = inflater.inflate(R.layout.fragment_checkout_payment_method, container, false);
        mApplication = (MyApplication) getActivity().getApplicationContext();
        mContext = getActivity();
        setHasOptionsMenu(true);
        mSharedPreference = PreferenceManager.getDefaultSharedPreferences(getActivity());


        initialise();
        listners();


        return mParentView;

    }


    void initialise() {


        mRelativeMain = mParentView.findViewById(R.id.relative_main);
        mProgressBar = mParentView.findViewById(R.id.progressbar);

        rbCod = mParentView.findViewById(R.id.rbCod);
        rbNetBanking = mParentView.findViewById(R.id.rbNetBanking);

     /*   //   merchantId = "rfExfs07852562297342"; //YCjIbVZREyXhviN6  -> production
        merchantId = "YCjIbVZREyXhviN6"; //taken from web code productflowconterller
        // merchantId  = "xDscAl22401455162334";
        customerId = generateStringID();
        orderId = generateStringID();
        channelId = "WAP";
        website = "DEFAULT"; *//*-> production*//*
        //  website = "WEBSTAGING";
        callbackUrl = "https://pguat.paytm.com/paytmchecksum/paytmCallback.jsp";
        industryType = "Retail";
        taxAmount = mSharedPreference.getString(ConstantCodes.ORDER_GRAND_TOTAL, "");
*/

    }

    void listners() {

        rbCod.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    networkCallAddPayment();
                }
            }
        });

        rbNetBanking.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {

                 //   networkCallgenerateChecksum();


                }
            }
        });

    }


    /*void networkCallgenerateChecksum() {
        if (mApplication.isInternetConnected()) {
            mApplication.getRetroFitInterface().generateChecksum(ConstantCodes.API_ACCESS_TOKEN,
                    merchantId, orderId, customerId, channelId, taxAmount, website, callbackUrl, industryType)
                    .enqueue(callbacl);
            mProgressBar.setVisibility(View.VISIBLE);
        } else {
            Utils.showSnackBar(mRelativeMain, getResources().getString(R.string
                    .message_connection));
            mProgressBar.setVisibility(View.GONE);
        }
    }

    private Callback<CallbackChecksum> callbacl = new Callback<CallbackChecksum>() {

        @Override
        public void onResponse(Call<CallbackChecksum> call, Response<CallbackChecksum>
                response) {
            if (response != null && response.isSuccessful() && response.body() != null) {

                CallbackChecksum pojoParticipamts = response.body();

                if (pojoParticipamts.getCHECKSUMHASH() != null) {
                    checksumHash = pojoParticipamts.getCHECKSUMHASH() + "";
                    initializePaytmPayment();
                } else {
                    Utils.showSnackBar(mRelativeMain, "" + pojoParticipamts.getMessage());
                }

            }
            mProgressBar.setVisibility(View.GONE);
        }

        @Override
        public void onFailure(Call<CallbackChecksum> call, Throwable t) {


        }
    };

    private String generateStringID() {
        String uuid = UUID.randomUUID().toString();
        return uuid.replaceAll("-", "");
    }

    private void initializePaytmPayment() {
        PaytmPGService paytmPGService = PaytmPGService.getProductionService();

        //creating a hashmap and adding all the required values
        HashMap<String, String> paramMap = new HashMap<>();
        paramMap.put("MID", merchantId);
        paramMap.put("ORDER_ID", orderId);
        paramMap.put("CUST_ID", customerId);
        paramMap.put("CHANNEL_ID", channelId);
        paramMap.put("TXN_AMOUNT", taxAmount);
        paramMap.put("WEBSITE", website);
        paramMap.put("CALLBACK_URL", callbackUrl);
        paramMap.put("CHECKSUMHASH", checksumHash);
        paramMap.put("INDUSTRY_TYPE_ID", industryType);

        //creating a paytmOrder instance using the hashmap
        PaytmOrder order = new PaytmOrder(paramMap);

        //initializing a PaytmPGService
        paytmPGService.initialize(order, null);

        //finally starting the paytm payment transaction using PaytmPGService
        paytmPGService.startPaymentTransaction(mContext, true, true, this);
    }

    private void handleError(Throwable throwable) {
        Log.e("error", "handleError: " + throwable.getLocalizedMessage());
    }

*/
    @Override
    public void onPause() {
        super.onPause();

    }

   /* @Override
    public void onTransactionResponse(Bundle inResponse) {

        JSONObject json = new JSONObject();
        Set<String> keys = inResponse.keySet();
        for (String key : keys) {
            try {
                // json.put(key, bundle.get(key)); see edit below
                json.put(key, JSONObject.wrap(inResponse.get(key)));
            } catch (JSONException e) {
                //Handle exception here
            }
        }

        try {
            Log.e("response", json.get("STATUS").toString());
            if (json.get("STATUS").toString().equals("TXN_SUCCESS")) {

                //  networkCallAddPayment();

            } else {
                Toast.makeText(mContext, "Payment Fail!!! Please try again later.", Toast.LENGTH_SHORT).show();
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        Log.e(TAG, "onTransactionResponse: " + inResponse.toString());
    }

    @Override
    public void networkNotAvailable() {
        Log.e(TAG, "networkNotAvailable: ");
    }

    @Override
    public void clientAuthenticationFailed(String inErrorMessage) {
        Log.e(TAG, "clientAuthenticationFailed: " + inErrorMessage);
    }

    @Override
    public void someUIErrorOccurred(String inErrorMessage) {
        Log.e(TAG, "someUIErrorOccurred: " + inErrorMessage);
    }

    @Override
    public void onErrorLoadingWebPage(int iniErrorCode, String inErrorMessage, String inFailingUrl) {
        Log.e(TAG, "onErrorLoadingWebPage: " + String.valueOf(iniErrorCode));
        Log.e(TAG, "onErrorLoadingWebPage: " + inErrorMessage);
        Log.e(TAG, "onErrorLoadingWebPage: " + inFailingUrl);
    }

    @Override
    public void onBackPressedCancelTransaction() {
        Log.e(TAG, "onBackPressedCancelTransaction: ");
    }

    @Override
    public void onTransactionCancel(String inErrorMessage, Bundle inResponse) {
        Log.e(TAG, "onTransactionCancel: " + inErrorMessage);
        Log.e(TAG, "onTransactionCancel: " + inResponse);
    }
*/

    void networkCallAddPayment() {
        if (mApplication.isInternetConnected()) {

            String paymentMethod = "";
            if (rbCod.isChecked()) {
                paymentMethod = "2";
            } else {
                paymentMethod = "1";
            }

            ArrayList<CartModel> cartModel = new ArrayList<>();

            db = new DatabaseHandler(mContext);
            cartModel.addAll(db.getAllCart());

            JSONArray jsonArray = new JSONArray();
            for (int i = 0; i < cartModel.size(); i++) {
                JSONObject jsonObject = new JSONObject();
                try {
                    jsonObject.put("id", cartModel.get(i).getProduct_id());
                    jsonObject.put("code", cartModel.get(i).getProduct_code());
                    jsonObject.put("price", cartModel.get(i).getProduct_price());
                    jsonObject.put("quantity", cartModel.get(i).getProduct_quantity());

                    jsonArray.put(jsonObject);

                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }

            Log.e("json data","" + jsonArray.toString());

            mApplication.getRetroFitInterface().OrderSubmit(ConstantCodes.API_ACCESS_TOKEN,
                    mSharedPreference.getString(ConstantCodes.LOGIN_USER_ID, ""),
                    paymentMethod,
                    mSharedPreference.getString(ConstantCodes.ORDER_SUB_TOTAL, ""),
                    mSharedPreference.getString(ConstantCodes.ORDER_GRAND_TOTAL, ""),
                    mSharedPreference.getString(ConstantCodes.ORDER_DISCOUNT, ""),
                    mSharedPreference.getString(ConstantCodes.ORDER_PROMO_CODE, ""),
                    mSharedPreference.getString(ConstantCodes.LOGIN_USER_NAME, ""),
                    mSharedPreference.getString(ConstantCodes.LOGIN_USER_EMAIL, ""),
                    mSharedPreference.getString(ConstantCodes.ORDER_ADDRESS, ""),
                    jsonArray.toString()).
                    enqueue(mCallbackAddPayment);
            mProgressBar.setVisibility(View.VISIBLE);
        } else {
            Utils.showSnackBar(mRelativeMain, getResources().getString(R.string
                    .message_connection));
            mProgressBar.setVisibility(View.GONE);
        }
    }


    private Callback<PojoSubmitOrder> mCallbackAddPayment = new Callback<PojoSubmitOrder>() {

        @Override
        public void onResponse(Call<PojoSubmitOrder> call, Response<PojoSubmitOrder>
                response) {
            if (response != null && response.isSuccessful() && response.body() != null) {

                PojoSubmitOrder pojoParticipants = response.body();

                if (pojoParticipants.getStatus()) {

                    Utils.showSnackBar(mRelativeMain, "" + pojoParticipants.getMessage());


                }
            }
            mProgressBar.setVisibility(View.GONE);
        }

        @Override
        public void onFailure(Call<PojoSubmitOrder> call, Throwable t) {
            Utils.showSnackBar(mRelativeMain, getString(R.string
                    .message_something_wrong));
            mProgressBar.setVisibility(View.GONE);

        }
    };

    /*void addJsonData() {


        String paymentMethod = "";
        if (rbCod.isChecked()) {
            paymentMethod = "2";
        } else {
            paymentMethod = "1";
        }

        ArrayList<CartModel> cartModel = new ArrayList<>();

        db = new DatabaseHandler(mContext);
        cartModel.addAll(db.getAllCart());

        JSONArray jsonArray = new JSONArray();
        for (int i = 0; i < cartModel.size(); i++) {
            JSONObject jsonObject = new JSONObject();
            try {
                jsonObject.put("product_id", cartModel.get(i).getProduct_id());
                jsonObject.put("product_code", cartModel.get(i).getProduct_code());
                jsonObject.put("product_price", cartModel.get(i).getProduct_price());
                jsonObject.put("product_quantity", cartModel.get(i).getProduct_quantity());

                jsonArray.put(jsonObject);

            } catch (JSONException e) {
                e.printStackTrace();
            }

        }

        pojoOrderSubmitObject = new PojoOrderSubmitObject(ConstantCodes.API_ACCESS_TOKEN,
                mSharedPreference.getString(ConstantCodes.LOGIN_USER_ID, ""),
                paymentMethod,
                mSharedPreference.getString(ConstantCodes.ORDER_SUB_TOTAL, ""),
                mSharedPreference.getString(ConstantCodes.ORDER_GRAND_TOTAL, ""),
                mSharedPreference.getString(ConstantCodes.ORDER_DISCOUNT, ""),
                mSharedPreference.getString(ConstantCodes.ORDER_PROMO_CODE, ""),
                mSharedPreference.getString(ConstantCodes.LOGIN_USER_NAME, ""),
                mSharedPreference.getString(ConstantCodes.LOGIN_USER_EMAIL, ""),
                mSharedPreference.getString(ConstantCodes.ORDER_ADDRESS, ""),
                cartModel
        );

    }*/


    @Override
    public void onNextClicked(StepperLayout.OnNextClickedCallback onNextClickedCallback) {


    }

    @Override
    public void onCompleteClicked(StepperLayout.OnCompleteClickedCallback onCompleteClickedCallback) {

    }

    @Override
    public void onBackClicked(StepperLayout.OnBackClickedCallback onBackClickedCallback) {

    }

    @Nullable
    @Override
    public VerificationError verifyStep() {
        return null;
    }

    @Override
    public void onSelected() {

    }

    @Override
    public void onError(@NonNull VerificationError verificationError) {

    }
}
