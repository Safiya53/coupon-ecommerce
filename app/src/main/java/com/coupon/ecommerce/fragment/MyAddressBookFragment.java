package com.coupon.ecommerce.fragment;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;


import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;

import com.coupon.ecommerce.R;
import com.coupon.ecommerce.application.MyApplication;
import com.coupon.ecommerce.constants.ConstantCodes;
import com.coupon.ecommerce.pojo.PojoCity;
import com.coupon.ecommerce.pojo.PojoGetUserAddress;
import com.coupon.ecommerce.pojo.PojoSaveUserAddress;
import com.coupon.ecommerce.utils.Utils;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


/**
 * Created by Rp on 8/30/2016.
 */
public class MyAddressBookFragment extends Fragment {

    private View mParentView;

    private Context mContext;
    RelativeLayout mRelativeMain;
    ProgressBar mProgressBar;

    MyApplication mApplication;
    private SharedPreferences mSharedPreference;

    RadioButton rbHome, rbOffice, rbOther;
    Spinner spCity;
    EditText etTitle, etAddress, etLandmark, etZipcode, etMobile;
    CheckBox cbDefault;
    ArrayAdapter<String> arrayAdapter;
    String[] CITY = {"Select City"};
    String addressId = "";
    TextView tvUpdateAddress;

    List<PojoGetUserAddress.Datum> mArraySavedAddress = new ArrayList<>();
    List<PojoCity.Datum> mArrayCity = new ArrayList<>();

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mParentView = inflater.inflate(R.layout.fragment_address_book, container, false);
        mApplication = (MyApplication) getActivity().getApplicationContext();
        mContext = getActivity();
        setHasOptionsMenu(true);
        mSharedPreference = PreferenceManager.getDefaultSharedPreferences(getActivity());


        initialise();
        listners();

        networkCallGetAddress();
        networkCallGetCity();

        return mParentView;

    }


    void initialise() {

        Toolbar mToolbar = (Toolbar) getActivity().findViewById(R.id.toolbar);
        mToolbar.setTitle("Address Book");

        mRelativeMain = mParentView.findViewById(R.id.relative_main);
        mProgressBar = mParentView.findViewById(R.id.progressbar);

        spCity = mParentView.findViewById(R.id.spCity);
        etTitle = mParentView.findViewById(R.id.etTitle);
        etAddress = mParentView.findViewById(R.id.etAddress);
        etLandmark = mParentView.findViewById(R.id.etLandmark);
        etZipcode = mParentView.findViewById(R.id.etZipcode);
        etMobile = mParentView.findViewById(R.id.etMobile);
        cbDefault = mParentView.findViewById(R.id.cbDefault);
        tvUpdateAddress = mParentView.findViewById(R.id.tvUpdate);

        rbHome = mParentView.findViewById(R.id.rbHome);
        rbOffice = mParentView.findViewById(R.id.rbOffice);
        rbOther = mParentView.findViewById(R.id.rbOther);


    }

    void listners() {

        tvUpdateAddress.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Utils.hideKeyboard(getActivity());

                if (rbHome.isChecked() || rbOffice.isChecked() || rbOther.isChecked()) {

                    if (!etTitle.getText().toString().equals("")) {
                        if (spCity.getSelectedItemPosition() > 0) {
                            if (!etAddress.getText().toString().equals("")) {
                                if (!etLandmark.getText().toString().equals("")) {
                                    if (!etZipcode.getText().toString().equals("")) {

                                        if (!etMobile.getText().toString().equals("")) {

                                            String isDefault = "";
                                            if (cbDefault.isChecked()) {
                                                isDefault = "1";
                                            } else {
                                                isDefault = "0";
                                            }

                                            String addressType = "";
                                            if (rbHome.isChecked()) {
                                                addressType = "1";
                                            } else if (rbOffice.isChecked()) {
                                                addressType = "2";
                                            } else if (rbOther.isChecked()) {
                                                addressType = "3";
                                            }

                                            networkCallSaveAddress(mArrayCity.get(spCity.getSelectedItemPosition() - 1).getCityId() + "",
                                                    etTitle.getText().toString() + "",
                                                    etAddress.getText().toString() + "",
                                                    etLandmark.getText().toString() + "",
                                                    etZipcode.getText().toString() + "",
                                                    isDefault,
                                                    addressType,
                                                    addressId,
                                                    etMobile.getText().toString());

                                        } else {
                                            Utils.showSnackBar(mRelativeMain, "Please Enter Mobile No");
                                        }
                                    } else {
                                        Utils.showSnackBar(mRelativeMain, "Please Enter Zipcode");
                                    }

                                } else {
                                    Utils.showSnackBar(mRelativeMain, "Please Enter Landmark");
                                }
                            } else {
                                Utils.showSnackBar(mRelativeMain, "Please Enter Address");
                            }
                        } else {
                            Utils.showSnackBar(mRelativeMain, "Please Select City");
                        }

                    } else {
                        Utils.showSnackBar(mRelativeMain, "Please Enter Title");
                    }
                } else {
                    Utils.showSnackBar(mRelativeMain, "Please Select Address Type");
                }

            }
        });

        rbHome.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {

                    for (int i = 0; i < mArraySavedAddress.size(); i++) {
                        if (mArraySavedAddress.get(i).getAddressType().equals("1")) {

                            addressId = mArraySavedAddress.get(i).getId() + "";

                            etTitle.setText("" + mArraySavedAddress.get(i).getTitle());
                            etAddress.setText("" + mArraySavedAddress.get(i).getAddress());
                            etLandmark.setText("" + mArraySavedAddress.get(i).getLandmark());
                            etZipcode.setText("" + mArraySavedAddress.get(i).getZipcode());

                            if (mArraySavedAddress.get(i).getPhone() != null) {
                                if (!mArraySavedAddress.get(i).getPhone().equals("")) {
                                    etMobile.setText("" + mArraySavedAddress.get(i).getPhone());
                                } else {
                                    etMobile.setText(mSharedPreference.getString(ConstantCodes.LOGIN_USER_PHONE, ""));
                                }
                            } else {
                                etMobile.setText(mSharedPreference.getString(ConstantCodes.LOGIN_USER_PHONE, ""));
                            }

                            if (mArraySavedAddress.get(i).getDefaultAddressSet().equals("1")) {
                                cbDefault.setChecked(true);
                            } else {
                                cbDefault.setChecked(false);
                            }
                            for (int j = 0; j < mArrayCity.size(); j++) {
                                if (mArraySavedAddress.get(i).getCityId().equals(mArrayCity.get(j).getCityId())) {
                                    spCity.setSelection(j + 1);
                                }
                            }
                        }
                    }
                }
            }
        });

        rbOffice.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    for (int i = 0; i < mArraySavedAddress.size(); i++) {
                        if (mArraySavedAddress.get(i).getAddressType().equals("2")) {

                            addressId = mArraySavedAddress.get(i).getId() + "";

                            etTitle.setText("" + mArraySavedAddress.get(i).getTitle());
                            etAddress.setText("" + mArraySavedAddress.get(i).getAddress());
                            etLandmark.setText("" + mArraySavedAddress.get(i).getLandmark());
                            etZipcode.setText("" + mArraySavedAddress.get(i).getZipcode());

                            if (mArraySavedAddress.get(i).getPhone() != null) {
                                if (!mArraySavedAddress.get(i).getPhone().equals("")) {
                                    etMobile.setText("" + mArraySavedAddress.get(i).getPhone());
                                } else {
                                    etMobile.setText(mSharedPreference.getString(ConstantCodes.LOGIN_USER_PHONE, ""));
                                }
                            } else {
                                etMobile.setText(mSharedPreference.getString(ConstantCodes.LOGIN_USER_PHONE, ""));
                            }

                            if (mArraySavedAddress.get(i).getDefaultAddressSet().equals("1")) {
                                cbDefault.setChecked(true);
                            } else {
                                cbDefault.setChecked(false);
                            }
                            for (int j = 0; j < mArrayCity.size(); j++) {
                                if (mArraySavedAddress.get(i).getCityId().equals(mArrayCity.get(j).getCityId())) {
                                    spCity.setSelection(j + 1);
                                }
                            }
                        }
                    }
                }
            }
        });

        rbOther.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    for (int i = 0; i < mArraySavedAddress.size(); i++) {
                        if (mArraySavedAddress.get(i).getAddressType().equals("3")) {

                            addressId = mArraySavedAddress.get(i).getId() + "";

                            etTitle.setText("" + mArraySavedAddress.get(i).getTitle());
                            etAddress.setText("" + mArraySavedAddress.get(i).getAddress());
                            etLandmark.setText("" + mArraySavedAddress.get(i).getLandmark());
                            etZipcode.setText("" + mArraySavedAddress.get(i).getZipcode());
                            if (mArraySavedAddress.get(i).getDefaultAddressSet().equals("1")) {
                                cbDefault.setChecked(true);
                            } else {
                                cbDefault.setChecked(false);
                            }
                            for (int j = 0; j < mArrayCity.size(); j++) {
                                if (mArraySavedAddress.get(i).getCityId().equals(mArrayCity.get(j).getCityId())) {
                                    spCity.setSelection(j + 1);
                                }
                            }
                        }
                    }
                }
            }
        });

    }


    void networkCallGetAddress() {
        if (mApplication.isInternetConnected()) {

            mApplication.getRetroFitInterface().GetUserAddress(ConstantCodes.API_ACCESS_TOKEN,
                    mSharedPreference.getString(ConstantCodes.LOGIN_USER_ID, "")).enqueue(mCallbackGetAddress);


            mProgressBar.setVisibility(View.VISIBLE);
        } else {
            Utils.showSnackBar(getActivity().findViewById(android.R.id.content), mContext.getResources().getString(R.string
                    .message_connection));
            mProgressBar.setVisibility(View.GONE);
        }
    }

    /**
     * Callback for network call for sub sub category
     */
    private Callback<PojoGetUserAddress> mCallbackGetAddress = new Callback<PojoGetUserAddress>() {

        @Override
        public void onResponse(Call<PojoGetUserAddress> call, Response<PojoGetUserAddress>
                response) {
            if (response != null && response.isSuccessful() && response.body() != null) {
                PojoGetUserAddress pojoParticipants = response.body();
                if (isAdded()) {

                    if (pojoParticipants.getStatus()) {

                        if (pojoParticipants.getData() != null) {
                            if (pojoParticipants.getData().size() > 0) {

                                mArraySavedAddress.clear();
                                mArraySavedAddress.addAll(pojoParticipants.getData());

                                rbHome.setChecked(true);
                            }

                        }
                    }

                }
            }
            mProgressBar.setVisibility(View.GONE);
        }

        @Override
        public void onFailure(Call<PojoGetUserAddress> call, Throwable t) {
            Utils.showSnackBar(mRelativeMain, getString(R.string
                    .message_something_wrong));
            mProgressBar.setVisibility(View.GONE);

        }
    };


    void networkCallGetCity() {
        if (mApplication.isInternetConnected()) {

            mApplication.getRetroFitInterface().City(ConstantCodes.API_ACCESS_TOKEN).enqueue(mCallbackGetCity);


            mProgressBar.setVisibility(View.VISIBLE);
        } else {
            Utils.showSnackBar(getActivity().findViewById(android.R.id.content), mContext.getResources().getString(R.string
                    .message_connection));
            mProgressBar.setVisibility(View.GONE);
        }
    }

    /**
     * Callback for network call for sub sub category
     */
    private Callback<PojoCity> mCallbackGetCity = new Callback<PojoCity>() {

        @Override
        public void onResponse(Call<PojoCity> call, Response<PojoCity>
                response) {
            if (response != null && response.isSuccessful() && response.body() != null) {
                PojoCity pojoParticipants = response.body();
                if (isAdded()) {

                    if (pojoParticipants.getStatus()) {

                        if (pojoParticipants.getData() != null) {
                            if (pojoParticipants.getData().size() > 0) {

                                mArrayCity.clear();
                                mArrayCity.addAll(pojoParticipants.getData());

                                CITY = new String[mArrayCity.size() + 1];
                                CITY[0] = "Select City";
                                for (int i = 0; i < mArrayCity.size(); i++) {
                                    CITY[i + 1] = mArrayCity.get(i).getCityName();
                                }

                                arrayAdapter = new ArrayAdapter<String>(mContext, R.layout.spinner_item, R.id.spinner_text, CITY);
                                spCity.setAdapter(arrayAdapter);


                            }

                        }
                    }

                }
            }
            mProgressBar.setVisibility(View.GONE);
        }

        @Override
        public void onFailure(Call<PojoCity> call, Throwable t) {
            Utils.showSnackBar(mRelativeMain, getString(R.string
                    .message_something_wrong));
            mProgressBar.setVisibility(View.GONE);

        }
    };

    void networkCallSaveAddress(String city, String title, String address, String landmark, String zipcode, String isDefault, String addressType, String addressId, String mobile) {
        if (mApplication.isInternetConnected()) {

            mApplication.getRetroFitInterface().SaveUserAddress(ConstantCodes.API_ACCESS_TOKEN,
                    mSharedPreference.getString(ConstantCodes.LOGIN_USER_ID, ""),
                    city,
                    landmark,
                    address,
                    zipcode,
                    addressType,
                    isDefault,
                    mobile,
                    addressId,
                    title).enqueue(mCallbackSaveAddress);


            mProgressBar.setVisibility(View.VISIBLE);
        } else {
            Utils.showSnackBar(getActivity().findViewById(android.R.id.content), mContext.getResources().getString(R.string
                    .message_connection));
            mProgressBar.setVisibility(View.GONE);
        }
    }

    /**
     * Callback for network call for sub sub category
     */
    private Callback<PojoSaveUserAddress> mCallbackSaveAddress = new Callback<PojoSaveUserAddress>() {

        @Override
        public void onResponse(Call<PojoSaveUserAddress> call, Response<PojoSaveUserAddress>
                response) {
            if (response != null && response.isSuccessful() && response.body() != null) {
                PojoSaveUserAddress pojoParticipants = response.body();
                if (isAdded()) {

                    if (pojoParticipants.getStatus()) {

                        networkCallGetAddress();

                        Utils.showSnackBar(mRelativeMain, "Address Updated Successfully");
                    } else {
                        if (!TextUtils.isEmpty(pojoParticipants.getData())) {
                            Utils.showSnackBar(mRelativeMain, pojoParticipants.getData());
                        } else {
                            Utils.showSnackBar(mRelativeMain, getString(R.string
                                    .message_something_wrong));
                        }
                    }

                }
            }
            mProgressBar.setVisibility(View.GONE);
        }

        @Override
        public void onFailure(Call<PojoSaveUserAddress> call, Throwable t) {
            Utils.showSnackBar(mRelativeMain, getString(R.string
                    .message_something_wrong));
            mProgressBar.setVisibility(View.GONE);

        }
    };


}
