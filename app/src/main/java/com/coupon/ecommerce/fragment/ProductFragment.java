package com.coupon.ecommerce.fragment;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ExpandableListView;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;


import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.coupon.ecommerce.R;
import com.coupon.ecommerce.adapter.ProductFilterAdapter;
import com.coupon.ecommerce.adapter.ProductListAdapter;
import com.coupon.ecommerce.application.MyApplication;
import com.coupon.ecommerce.constants.ConstantCodes;
import com.coupon.ecommerce.database.DatabaseHandler;
import com.coupon.ecommerce.pojo.CartModel;
import com.coupon.ecommerce.pojo.PojoHomeShopCategory;
import com.coupon.ecommerce.pojo.PojoProductList;
import com.coupon.ecommerce.utils.Utils;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


/**
 * Created by Rp on 8/30/2016.
 */
public class ProductFragment extends Fragment {

    private View mParentView;

    private Context mContext;
    RelativeLayout mRelativeMain;
    ProgressBar mProgressBar;

    MyApplication mApplication;
    private SharedPreferences mSharedPreference;

    RecyclerView rvProduct;

    ProductListAdapter productListAdapter;

    List<PojoProductList.Datum> mArrayProductList = new ArrayList<>();
    int mTotalCount = 0, mPage = 1;
    boolean mIsFromLastItem = false;
    boolean mIsLoading;

    TextView tv_no_record, tvTotalCounter;

    GridLayoutManager mLayoutManagerHappyHours1;
    ExpandableListView expandableListView;

    ProductFilterAdapter productFilterAdapter;
    RelativeLayout llFilterData, llFilter;
    ImageView ivTransparent;
    String apiSubSubCatId = "", apiSubCatId = "";

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mParentView = inflater.inflate(R.layout.fragment_product, container, false);
        mApplication = (MyApplication) getActivity().getApplicationContext();
        mContext = getActivity();
        setHasOptionsMenu(true);
        mSharedPreference = PreferenceManager.getDefaultSharedPreferences(getActivity());


        initialise();
        listners();

        if (getArguments().getString("subCatId") != null) {
            apiSubCatId = getArguments().getString("subCatId");
        }
        apiSubSubCatId = "";


        networkCallProduct();

        if (getArguments().getSerializable("data") != null) {
            PojoHomeShopCategory.MenuList menuList = (PojoHomeShopCategory.MenuList) getArguments().getSerializable("data");
            productFilterAdapter = new ProductFilterAdapter(mContext, menuList.getSubcategories(), customClickFilterCategory);
            expandableListView.setAdapter(productFilterAdapter);
        } else {
            networkCallFilterData();
        }


        return mParentView;

    }


    void initialise() {

        Toolbar mToolbar = (Toolbar) getActivity().findViewById(R.id.toolbar);
        mToolbar.setTitle("Products");

        mRelativeMain = mParentView.findViewById(R.id.relative_main);
        mProgressBar = mParentView.findViewById(R.id.progressbar);

        tv_no_record = mParentView.findViewById(R.id.tv_no_record);
        ivTransparent = mParentView.findViewById(R.id.ivTransparent);
        tvTotalCounter = mParentView.findViewById(R.id.tvTotalCounter);

        llFilter = mParentView.findViewById(R.id.llFilter);
        llFilterData = mParentView.findViewById(R.id.llFilterData);

        rvProduct = mParentView.findViewById(R.id.rvProduct);
        mLayoutManagerHappyHours1 = new GridLayoutManager(getActivity(), 2, LinearLayoutManager.VERTICAL, false);
        rvProduct.setLayoutManager(mLayoutManagerHappyHours1);
        rvProduct.setNestedScrollingEnabled(false);
        rvProduct.setItemAnimator(new DefaultItemAnimator());
        rvProduct.setHasFixedSize(false);

        productListAdapter = new ProductListAdapter(mContext, mArrayProductList, customClickProduct);
        rvProduct.setAdapter(productListAdapter);

        // expandable listview
        expandableListView = (ExpandableListView) mParentView.findViewById(R.id.expandible_listview);
        // OPTIONAL : Show one list at a time
        expandableListView.setOnGroupExpandListener(new ExpandableListView.OnGroupExpandListener() {
            int previousGroup = -1;

            @Override
            public void onGroupExpand(int groupPosition) {
                if (groupPosition != previousGroup)
                    expandableListView.collapseGroup(previousGroup);
                previousGroup = groupPosition;
            }
        });
    }

    void listners() {

        llFilter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                llFilterData.setVisibility(View.VISIBLE);
                ivTransparent.setVisibility(View.VISIBLE);
            }
        });

        ivTransparent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                llFilterData.setVisibility(View.GONE);
                ivTransparent.setVisibility(View.GONE);
            }
        });

        rvProduct.addOnScrollListener(mOnScrollListener);

    }

    RecyclerView.OnScrollListener mOnScrollListener = new RecyclerView.OnScrollListener() {
        @Override
        public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
            super.onScrollStateChanged(recyclerView, newState);
        }

        @Override
        public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
            super.onScrolled(recyclerView, dx, dy);


            int pastVisiblesItems, visibleItemCount, totalItemCount;
            visibleItemCount = mLayoutManagerHappyHours1.getChildCount();
            totalItemCount = mLayoutManagerHappyHours1.getItemCount();
            pastVisiblesItems = mLayoutManagerHappyHours1.findFirstVisibleItemPosition();

            if (totalItemCount < mTotalCount) {
                if (!mIsLoading && (totalItemCount - visibleItemCount)
                        <= (pastVisiblesItems + 1)) {
                    mIsFromLastItem = true;
                    mPage++;
                    networkCallProduct();

                    mIsLoading = true;
                }
            }

        }
    };

    ProductListAdapter.CustomClick customClickProduct = new ProductListAdapter.CustomClick() {
        @Override
        public void itemClick(int id) {

            Fragment fragment = new ProductDetailFragment();
            FragmentTransaction ft = getFragmentManager().beginTransaction();
            ft.replace(R.id.frame_container, fragment).addToBackStack("").commit();
        }

        @Override
        public void cartClick(String code, String price, String id) {

            DatabaseHandler db = new DatabaseHandler(mContext);
            if (db.isProductInCart(code) > 0) {
                Utils.showSnackBar(mRelativeMain, "Product Already Added to Cart");
            } else {

                db.addCart(new CartModel(id, code, price, "1"));

                Toast.makeText(mContext, " Item added to cart", Toast.LENGTH_SHORT).show();
            }
        }
    };


    void networkCallProduct() {
        if (mApplication.isInternetConnected()) {

            if (apiSubCatId.equals("")) {
                mApplication.getRetroFitInterface().ProductListCategory(ConstantCodes.API_ACCESS_TOKEN,
                        mPage,
                        getArguments().getString("catId")).enqueue(mCallbackProductList);
            } else {

                if (apiSubSubCatId.equals("")) {
                    mApplication.getRetroFitInterface().ProductList(ConstantCodes.API_ACCESS_TOKEN,
                            mPage,
                            getArguments().getString("catId"),
                            apiSubCatId).enqueue(mCallbackProductList);
                } else {
                    mApplication.getRetroFitInterface().ProductListSubSUb(ConstantCodes.API_ACCESS_TOKEN,
                            mPage,
                            getArguments().getString("catId"),
                            apiSubCatId,
                            apiSubSubCatId).enqueue(mCallbackProductList);
                }

            }


            mProgressBar.setVisibility(View.VISIBLE);
        } else {
            Utils.showSnackBar(getActivity().findViewById(android.R.id.content), mContext.getResources().getString(R.string
                    .message_connection));
            mProgressBar.setVisibility(View.GONE);
        }
    }

    /**
     * Callback for network call for sub sub category
     */
    private Callback<PojoProductList> mCallbackProductList = new Callback<PojoProductList>() {

        @Override
        public void onResponse(Call<PojoProductList> call, Response<PojoProductList>
                response) {
            if (response != null && response.isSuccessful() && response.body() != null) {
                PojoProductList pojoParticipants = response.body();
                if (isAdded()) {

                    // mArraryLeads.clear();
                    if (pojoParticipants.getStatus()) {

                        mTotalCount = pojoParticipants.getProducts().getTotal();
                        tvTotalCounter.setText(pojoParticipants.getProducts().getTotal() + " Products Found");

                        if (pojoParticipants.getProducts().getData() != null) {

                            if (pojoParticipants.getProducts().getData().size() > 0) {

                                if (mPage == 1) {
                                    mArrayProductList.clear();
                                    mArrayProductList.addAll(pojoParticipants.getProducts().getData());


                                } else if (mIsFromLastItem) {
                                    // mArraryBokSiteVisit.remove(mArraryBokSiteVisit.size()-1);
                                    productListAdapter.notifyItemRemoved(mArrayProductList.size());
                                    mArrayProductList.addAll(pojoParticipants.getProducts().getData());


                                }

                                productListAdapter.notifyDataSetChanged();
                                mIsLoading = false;
                                mIsFromLastItem = false;


                            } else {
                                rvProduct.setAdapter(null);
                                if (!TextUtils.isEmpty(pojoParticipants.getMessage())) {
                                    tv_no_record.setVisibility(View.VISIBLE);
                                    tv_no_record.setText(pojoParticipants.getMessage() + "");

                                } else {
                                    tv_no_record.setVisibility(View.VISIBLE);
                                }
                            }

                        } else {
                            rvProduct.setAdapter(null);
                            if (!TextUtils.isEmpty(pojoParticipants.getMessage())) {
                                tv_no_record.setVisibility(View.VISIBLE);
                                tv_no_record.setText(pojoParticipants.getMessage() + "");

                            } else {
                                tv_no_record.setVisibility(View.VISIBLE);
                            }

                        }


                    } else {
                        rvProduct.setAdapter(null);
                        if (!TextUtils.isEmpty(pojoParticipants.getMessage())) {
                            tv_no_record.setVisibility(View.VISIBLE);
                            tv_no_record.setText(pojoParticipants.getMessage() + "");

                        } else {
                            tv_no_record.setVisibility(View.VISIBLE);
                        }
                    }
                }
            }
            mProgressBar.setVisibility(View.GONE);
        }

        @Override
        public void onFailure(Call<PojoProductList> call, Throwable t) {
            Utils.showSnackBar(mRelativeMain, getString(R.string
                    .message_something_wrong));
            mProgressBar.setVisibility(View.GONE);

        }
    };

    ProductFilterAdapter.CustomClick customClickFilterCategory = new ProductFilterAdapter.CustomClick() {
        @Override
        public void itemClick(int subCatId, int subSubCatId) {

            mPage = 1;

            llFilterData.setVisibility(View.GONE);
            ivTransparent.setVisibility(View.GONE);

            apiSubCatId = "" + subCatId;
            apiSubSubCatId = "" + subSubCatId;
            networkCallProduct();

        }
    };

    void networkCallFilterData() {
        if (mApplication.isInternetConnected()) {
            mApplication.getRetroFitInterface().ShopByCategory(
                    ConstantCodes.API_ACCESS_TOKEN)
                    .enqueue(mCallbackData);


            mProgressBar.setVisibility(View.VISIBLE);

        } else {
            Utils.showSnackBar(getActivity().findViewById(android.R.id.content), mContext.getResources().getString(R.string
                    .message_connection));
            mProgressBar.setVisibility(View.GONE);
        }
    }

    /**
     * Callback for network call for sub sub category
     */
    private Callback<PojoHomeShopCategory> mCallbackData = new Callback<PojoHomeShopCategory>() {

        @Override
        public void onResponse(Call<PojoHomeShopCategory> call, Response<PojoHomeShopCategory>
                response) {
            if (response != null && response.isSuccessful() && response.body() != null) {
                PojoHomeShopCategory pojoParticipants = response.body();
                if (isAdded()) {
                    if (pojoParticipants.getStatus()) {

                        if (pojoParticipants.getMenuList() != null) {
                            if (pojoParticipants.getMenuList().size() > 0) {

                                for (int i = 0; i < pojoParticipants.getMenuList().size(); i++) {
                                    if (pojoParticipants.getMenuList().get(i).getId() == Integer.parseInt(getArguments().getString("catId"))) {
                                        PojoHomeShopCategory.MenuList menuList = pojoParticipants.getMenuList().get(i);
                                        productFilterAdapter = new ProductFilterAdapter(mContext, menuList.getSubcategories(), customClickFilterCategory);
                                        expandableListView.setAdapter(productFilterAdapter);
                                    }
                                }

                            }
                        }


                    }
                }
            }

            mProgressBar.setVisibility(View.GONE);
        }

        @Override
        public void onFailure(Call<PojoHomeShopCategory> call, Throwable t) {
            Utils.showSnackBar(mRelativeMain, getString(R.string
                    .message_something_wrong));
            mProgressBar.setVisibility(View.GONE);

        }
    };


}
