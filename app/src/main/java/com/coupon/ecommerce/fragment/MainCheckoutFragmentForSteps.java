package com.coupon.ecommerce.fragment;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;

import com.coupon.ecommerce.R;
import com.coupon.ecommerce.adapter.MyCheckoutStepperAdapter;
import com.coupon.ecommerce.application.MyApplication;
import com.stepstone.stepper.StepperLayout;


public class MainCheckoutFragmentForSteps extends Fragment {

    private View mParentView;
    MyApplication mApplication;
    private Context mContext;
    private SharedPreferences mSharedPreference;

    public StepperLayout mStepperLayout;
    public MyCheckoutStepperAdapter adapter;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mParentView = inflater.inflate(R.layout.fragment_main_checkout_step, container, false);
        mApplication = (MyApplication) getActivity().getApplicationContext();
        mContext = getActivity();
        setHasOptionsMenu(true);
        mSharedPreference = PreferenceManager.getDefaultSharedPreferences(getActivity());

        Toolbar toolbar;
        toolbar = getActivity().findViewById(R.id.toolbar);
        toolbar.setTitle("Checkout");

        mStepperLayout = (StepperLayout) mParentView.findViewById(R.id.stepperLayout);
        adapter = new MyCheckoutStepperAdapter(getChildFragmentManager(), mContext);
        mStepperLayout.setAdapter(adapter);

        return mParentView;

    }


}
