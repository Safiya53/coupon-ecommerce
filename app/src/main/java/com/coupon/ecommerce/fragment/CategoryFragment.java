package com.coupon.ecommerce.fragment;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ExpandableListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;

import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import com.coupon.ecommerce.R;
import com.coupon.ecommerce.adapter.CategorySecondLevelAdapter;
import com.coupon.ecommerce.application.MyApplication;
import com.coupon.ecommerce.constants.ConstantCodes;
import com.coupon.ecommerce.pojo.PojoHomeShopCategory;
import com.coupon.ecommerce.utils.Utils;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Rp on 8/30/2016.
 */
public class CategoryFragment extends Fragment {

    private View mParentView;

    private Context mContext;
    RelativeLayout mRelativeMain;
    ProgressBar mProgressBar;

    MyApplication mApplication;
    private SharedPreferences mSharedPreference;

    ExpandableListView expandableListView;

    CategorySecondLevelAdapter secondLevelAdapter;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mParentView = inflater.inflate(R.layout.fragment_category, container, false);
        mApplication = (MyApplication) getActivity().getApplicationContext();
        mContext = getActivity();
        setHasOptionsMenu(true);
        mSharedPreference = PreferenceManager.getDefaultSharedPreferences(getActivity());


        initialise();
        listners();

        networkCallData();

        return mParentView;

    }


    void initialise() {

        Toolbar mToolbar = (Toolbar) getActivity().findViewById(R.id.toolbar);
        mToolbar.setTitle("Category");

        mRelativeMain = mParentView.findViewById(R.id.relative_main);
        mProgressBar = mParentView.findViewById(R.id.progressbar);

        // expandable listview
        expandableListView = (ExpandableListView) mParentView.findViewById(R.id.expandible_listview);
        // OPTIONAL : Show one list at a time
        expandableListView.setOnGroupExpandListener(new ExpandableListView.OnGroupExpandListener() {
            int previousGroup = -1;

            @Override
            public void onGroupExpand(int groupPosition) {
                if (groupPosition != previousGroup)
                    expandableListView.collapseGroup(previousGroup);
                previousGroup = groupPosition;
            }
        });


    }

    void listners() {


    }

    CategorySecondLevelAdapter.CustomClick customClick = new CategorySecondLevelAdapter.CustomClick() {
        @Override
        public void itemClick(int catId, int subCatId, PojoHomeShopCategory.MenuList menuList) {
            Fragment fragment = new ProductFragment();
            FragmentTransaction ft = getFragmentManager().beginTransaction();

            Bundle b = new Bundle();
            b.putString("catId", "" + catId);
            b.putString("subCatId", "" + subCatId);
            b.putSerializable("data", menuList);

            fragment.setArguments(b);

            ft.replace(R.id.frame_container, fragment).addToBackStack("").commit();
        }

    };


    void networkCallData() {
        if (mApplication.isInternetConnected()) {
            mApplication.getRetroFitInterface().ShopByCategory(
                    ConstantCodes.API_ACCESS_TOKEN)
                    .enqueue(mCallbackData);


            mProgressBar.setVisibility(View.VISIBLE);

        } else {
            Utils.showSnackBar(getActivity().findViewById(android.R.id.content), mContext.getResources().getString(R.string
                    .message_connection));
            mProgressBar.setVisibility(View.GONE);
        }
    }

    /**
     * Callback for network call for sub sub category
     */
    private Callback<PojoHomeShopCategory> mCallbackData = new Callback<PojoHomeShopCategory>() {

        @Override
        public void onResponse(Call<PojoHomeShopCategory> call, Response<PojoHomeShopCategory>
                response) {
            if (response != null && response.isSuccessful() && response.body() != null) {
                PojoHomeShopCategory pojoParticipants = response.body();
                if (isAdded()) {
                    if (pojoParticipants.getStatus()) {

                        if (pojoParticipants.getMenuList() != null) {
                            if (pojoParticipants.getMenuList().size() > 0) {
                                secondLevelAdapter = new CategorySecondLevelAdapter(mContext, pojoParticipants.getMenuList(), customClick);
                                expandableListView.setAdapter(secondLevelAdapter);
                            }
                        }


                    }
                }
            }

            mProgressBar.setVisibility(View.GONE);
        }

        @Override
        public void onFailure(Call<PojoHomeShopCategory> call, Throwable t) {
            Utils.showSnackBar(mRelativeMain, getString(R.string
                    .message_something_wrong));
            mProgressBar.setVisibility(View.GONE);

        }
    };


}
