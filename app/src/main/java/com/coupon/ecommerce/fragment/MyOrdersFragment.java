package com.coupon.ecommerce.fragment;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;


import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.coupon.ecommerce.R;
import com.coupon.ecommerce.adapter.OrderListAdapter;
import com.coupon.ecommerce.application.MyApplication;
import com.coupon.ecommerce.constants.ConstantCodes;
import com.coupon.ecommerce.pojo.PojoMyOrder;
import com.coupon.ecommerce.utils.Utils;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


/**
 * Created by Rp on 8/30/2016.
 */
public class MyOrdersFragment extends Fragment {

    private View mParentView;

    private Context mContext;
    RelativeLayout mRelativeMain;
    ProgressBar mProgressBar;

    MyApplication mApplication;
    private SharedPreferences mSharedPreference;

    RecyclerView rvOrder;

    OrderListAdapter orderListAdapter;

    List<PojoMyOrder.Datum> mArrayOrderList = new ArrayList<>();

    TextView tv_no_record;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mParentView = inflater.inflate(R.layout.fragment_orders, container, false);
        mApplication = (MyApplication) getActivity().getApplicationContext();
        mContext = getActivity();
        setHasOptionsMenu(true);
        mSharedPreference = PreferenceManager.getDefaultSharedPreferences(getActivity());


        initialise();
        listners();

        networkCallOder();

        return mParentView;

    }


    void initialise() {

        Toolbar mToolbar = (Toolbar) getActivity().findViewById(R.id.toolbar);
        mToolbar.setTitle("My Orders");

        mRelativeMain = mParentView.findViewById(R.id.relative_main);
        mProgressBar = mParentView.findViewById(R.id.progressbar);
        tv_no_record = mParentView.findViewById(R.id.tv_no_record);

        rvOrder = mParentView.findViewById(R.id.rvOrder);
        RecyclerView.LayoutManager mLayoutManagerHappyHours1 = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
        rvOrder.setLayoutManager(mLayoutManagerHappyHours1);
        rvOrder.setNestedScrollingEnabled(false);
        rvOrder.setItemAnimator(new DefaultItemAnimator());
        rvOrder.setHasFixedSize(false);

    }

    void listners() {


    }

    void networkCallOder() {
        if (mApplication.isInternetConnected()) {

            mApplication.getRetroFitInterface().GetMyOrder(ConstantCodes.API_ACCESS_TOKEN,
                    mSharedPreference.getString(ConstantCodes.LOGIN_USER_ID, "")).enqueue(mCallbackData);


            mProgressBar.setVisibility(View.VISIBLE);
        } else {
            Utils.showSnackBar(getActivity().findViewById(android.R.id.content), mContext.getResources().getString(R.string
                    .message_connection));
            mProgressBar.setVisibility(View.GONE);
        }
    }

    /**
     * Callback for network call for sub sub category
     */
    private Callback<PojoMyOrder> mCallbackData = new Callback<PojoMyOrder>() {

        @Override
        public void onResponse(Call<PojoMyOrder> call, Response<PojoMyOrder>
                response) {
            if (response != null && response.isSuccessful() && response.body() != null) {
                PojoMyOrder pojoParticipants = response.body();
                if (isAdded()) {

                    if (pojoParticipants.getStatus()) {

                        if (pojoParticipants.getData() != null) {
                            if (pojoParticipants.getData().size() > 0) {
                                mArrayOrderList.clear();
                                mArrayOrderList.addAll(pojoParticipants.getData());
                                orderListAdapter = new OrderListAdapter(mContext, mArrayOrderList, customClick);
                                rvOrder.setAdapter(orderListAdapter);
                            }
                        }


                    } else {

                        if (!TextUtils.isEmpty(pojoParticipants.getMessage())) {
                            tv_no_record.setText("" + pojoParticipants.getMessage());
                            tv_no_record.setVisibility(View.VISIBLE);
                        } else {
                            tv_no_record.setVisibility(View.VISIBLE);
                        }
                    }

                }
            }
            mProgressBar.setVisibility(View.GONE);
        }

        @Override
        public void onFailure(Call<PojoMyOrder> call, Throwable t) {
            Utils.showSnackBar(mRelativeMain, getString(R.string
                    .message_something_wrong));
            mProgressBar.setVisibility(View.GONE);

        }
    };


    OrderListAdapter.CustomClick customClick = new OrderListAdapter.CustomClick() {
        @Override
        public void itemClick(int id) {

            Fragment fragment = new OrderDetailFragment();
            FragmentTransaction ft = getFragmentManager().beginTransaction();
            ft.replace(R.id.frame_container, fragment).addToBackStack("").commit();
        }
    };

}
