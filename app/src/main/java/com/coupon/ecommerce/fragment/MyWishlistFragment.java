package com.coupon.ecommerce.fragment;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;


import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.coupon.ecommerce.R;
import com.coupon.ecommerce.adapter.WishListAdapter;
import com.coupon.ecommerce.application.MyApplication;
import com.coupon.ecommerce.constants.ConstantCodes;
import com.coupon.ecommerce.pojo.PojoWishlist;
import com.coupon.ecommerce.utils.Utils;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


/**
 * Created by Rp on 8/30/2016.
 */
public class MyWishlistFragment extends Fragment {

    private View mParentView;

    private Context mContext;
    RelativeLayout mRelativeMain;
    ProgressBar mProgressBar;

    MyApplication mApplication;
    private SharedPreferences mSharedPreference;

    RecyclerView rvWishlist;

    WishListAdapter wishListAdapter;

    List<PojoWishlist.Datum> mArrayList = new ArrayList<>();

    TextView tv_no_record;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mParentView = inflater.inflate(R.layout.fragment_wishlist, container, false);
        mApplication = (MyApplication) getActivity().getApplicationContext();
        mContext = getActivity();
        setHasOptionsMenu(true);
        mSharedPreference = PreferenceManager.getDefaultSharedPreferences(getActivity());


        initialise();
        listners();


        return mParentView;

    }


    void initialise() {

        Toolbar mToolbar = (Toolbar) getActivity().findViewById(R.id.toolbar);
        mToolbar.setTitle("My Wishlist");

        mRelativeMain = mParentView.findViewById(R.id.relative_main);
        mProgressBar = mParentView.findViewById(R.id.progressbar);
        tv_no_record = mParentView.findViewById(R.id.tv_no_record);

        rvWishlist = mParentView.findViewById(R.id.rvWishlist);
        RecyclerView.LayoutManager mLayoutManagerHappyHours1 = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
        rvWishlist.setLayoutManager(mLayoutManagerHappyHours1);
        rvWishlist.setNestedScrollingEnabled(false);
        rvWishlist.setItemAnimator(new DefaultItemAnimator());
        rvWishlist.setHasFixedSize(false);


    }

    void listners() {


    }

    WishListAdapter.CustomClick customClick = new WishListAdapter.CustomClick() {
        @Override
        public void itemClick(int id) {

            Fragment fragment = new ProductDetailFragment();
            FragmentTransaction ft = getFragmentManager().beginTransaction();
            ft.replace(R.id.frame_container, fragment).addToBackStack("").commit();
        }
    };


    void networkCallWishlist() {
        if (mApplication.isInternetConnected()) {

            mApplication.getRetroFitInterface().GetWishlist(ConstantCodes.API_ACCESS_TOKEN,
                    mSharedPreference.getString(ConstantCodes.LOGIN_USER_ID, "")).enqueue(mCallbackData);


            mProgressBar.setVisibility(View.VISIBLE);
        } else {
            Utils.showSnackBar(getActivity().findViewById(android.R.id.content), mContext.getResources().getString(R.string
                    .message_connection));
            mProgressBar.setVisibility(View.GONE);
        }
    }

    /**
     * Callback for network call for sub sub category
     */
    private Callback<PojoWishlist> mCallbackData = new Callback<PojoWishlist>() {

        @Override
        public void onResponse(Call<PojoWishlist> call, Response<PojoWishlist>
                response) {
            if (response != null && response.isSuccessful() && response.body() != null) {
                PojoWishlist pojoParticipants = response.body();
                if (isAdded()) {

                    if (pojoParticipants.getStatus()) {

                        if (pojoParticipants.getData()!=null){
                            if (pojoParticipants.getData().size()>0){
                                mArrayList.clear();
                                mArrayList.addAll(pojoParticipants.getData());
                                wishListAdapter = new WishListAdapter(mContext, mArrayList, customClick);
                                rvWishlist.setAdapter(wishListAdapter);
                            }
                        }


                    } else {

                        if (!TextUtils.isEmpty(pojoParticipants.getMessage())) {
                            tv_no_record.setText("" + pojoParticipants.getMessage());
                            tv_no_record.setVisibility(View.VISIBLE);
                        } else {
                            tv_no_record.setVisibility(View.VISIBLE);
                        }
                    }

                }
            }
            mProgressBar.setVisibility(View.GONE);
        }

        @Override
        public void onFailure(Call<PojoWishlist> call, Throwable t) {
            Utils.showSnackBar(mRelativeMain, getString(R.string
                    .message_something_wrong));
            mProgressBar.setVisibility(View.GONE);

        }
    };

}
