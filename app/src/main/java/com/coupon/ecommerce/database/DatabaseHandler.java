package com.coupon.ecommerce.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.coupon.ecommerce.pojo.CartModel;

import java.util.ArrayList;
import java.util.List;


public class DatabaseHandler extends SQLiteOpenHelper {

    private Context context;

    private static final int DATABASE_VERSION = 1;
    private static final String DATABASE_NAME = "listDekho";

    private static final String TABLE_CART = "cart";
    private static final String KEY_PRODUCT_ID = "product_id";
    private static final String KEY_PRODUCT_CODE = "product_code";
    private static final String KEY_PRODUCT_PRICE = "product_price";
    private static final String KEY_PRODUCT_QUANTITY = "product_quantity";


    public DatabaseHandler(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        this.context = context;
        //3rd argument to be passed is CursorFactory instance
    }

    // Creating Tables
    @Override
    public void onCreate(SQLiteDatabase db) {
        String CREATE_CONTACTS_TABLE = "CREATE TABLE " + TABLE_CART + "("
                + KEY_PRODUCT_ID + " TEXT,"
                + KEY_PRODUCT_CODE + " TEXT,"
                + KEY_PRODUCT_PRICE + " TEXT,"
                + KEY_PRODUCT_QUANTITY + " TEXT" + ")";
        db.execSQL(CREATE_CONTACTS_TABLE);

    }

    // Upgrading database
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // Drop older table if existed
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_CART);

        // Create tables again
        onCreate(db);
    }

    public void deleteCartAll() {
        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL("delete from " + TABLE_CART);
        db.close();
    }

    // code to add the new contact
    public void addCart(CartModel cartModel) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(KEY_PRODUCT_ID, cartModel.getProduct_id());
        values.put(KEY_PRODUCT_CODE, cartModel.getProduct_code());
        values.put(KEY_PRODUCT_PRICE, cartModel.getProduct_price());
        values.put(KEY_PRODUCT_QUANTITY, cartModel.getProduct_quantity());


        // Inserting Row
        db.insert(TABLE_CART, null, values);
        //2nd argument is String containing nullColumnHack
        db.close(); // Closing database connection
    }

    // code to get the single contact
    public CartModel getCart(String ProductName) {
        SQLiteDatabase db = this.getReadableDatabase();

        Cursor cursor = db.query(TABLE_CART, new String[]{
                        KEY_PRODUCT_ID, KEY_PRODUCT_CODE, KEY_PRODUCT_PRICE, KEY_PRODUCT_QUANTITY},
                KEY_PRODUCT_CODE + "=?",
                new String[]{ProductName}, null, null, null, null);

        if (cursor != null)
            cursor.moveToFirst();

        CartModel cartModel = new CartModel(cursor.getString(0),
                cursor.getString(1), cursor.getString(2), cursor.getString(3)
                );
        // return contact
        return cartModel;
    }

    // code to get the single contact
    public int isProductInCart(String ProductName) {
        SQLiteDatabase db = this.getReadableDatabase();

        Cursor cursor = db.query(TABLE_CART, new String[]{
                        KEY_PRODUCT_ID, KEY_PRODUCT_CODE, KEY_PRODUCT_PRICE, KEY_PRODUCT_QUANTITY},
                KEY_PRODUCT_CODE + "=?",
                new String[]{ProductName}, null, null, null, null);
        int num = cursor.getCount();
        if (cursor != null)
            cursor.moveToFirst();
        // return contact
        return num;
    }

    // code to get all contacts in a list view
    public List<CartModel> getAllCart() {
        List<CartModel> contactList = new ArrayList<CartModel>();
        // Select All Query
        String selectQuery = "SELECT  * FROM " + TABLE_CART;

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                CartModel cartModel = new CartModel();
                cartModel.setProduct_id(cursor.getString(0));
                cartModel.setProduct_code(cursor.getString(1));
                cartModel.setProduct_price(cursor.getString(2));
                cartModel.setProduct_quantity(cursor.getString(3));

                // Adding contact to list
                contactList.add(cartModel);
            } while (cursor.moveToNext());
        }

        // return contact list
        return contactList;
    }

    // code to update the single contact
    public int updateCart(CartModel cartModel) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(KEY_PRODUCT_PRICE, cartModel.getProduct_price());

        // updating row
        return db.update(TABLE_CART, values, KEY_PRODUCT_ID + " = ?",
                new String[]{String.valueOf(cartModel.getProduct_id())});
    }

    // Deleting single contact
    public void deleteCart(String productId) {
        SQLiteDatabase db = this.getWritableDatabase();
        int cnt = db.delete(TABLE_CART, KEY_PRODUCT_ID + " = ?",
                new String[]{productId});
        db.close();
    }

    // Getting contacts Count
    public int getCartsCount() {
        String countQuery = "SELECT  * FROM " + TABLE_CART;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(countQuery, null);

        // return count
        return cursor.getCount();
    }

}
