package com.coupon.ecommerce.activity;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.app.AppCompatDelegate;
import androidx.appcompat.widget.Toolbar;
import androidx.core.content.ContextCompat;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;

import com.coupon.ecommerce.R;
import com.coupon.ecommerce.application.MyApplication;
import com.coupon.ecommerce.constants.ConstantCodes;
import com.coupon.ecommerce.fragment.CartFragment;
import com.coupon.ecommerce.fragment.MyAccountFragment;
import com.coupon.ecommerce.fragment.MyOrdersFragment;
import com.coupon.ecommerce.fragment.MyWishlistFragment;
import com.coupon.ecommerce.utils.Utils;
import com.google.android.material.navigation.NavigationView;


public class MainActivity extends AppCompatActivity implements NavigationView
        .OnNavigationItemSelectedListener {

    private Context mContext;
    public DrawerLayout mDrawerLayout;
    private ActionBarDrawerToggle mDrawerToggle;
    private Toolbar mToolbar;
    private SharedPreferences mSharedPreference;

    NavigationView navigationView;

    TextView tvLoginName;

    LinearLayout llNav;

    MyApplication mApplication;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);
        setContentView(R.layout.activity_main);
        mContext = this;
        mSharedPreference = PreferenceManager.getDefaultSharedPreferences(this);
        mApplication = (MyApplication) this.getApplicationContext();


        setToolbar();
        initComponents();
        setDrawer();
      //  changeFragment(new HomeFragment(), "HomeFragment");
        setAppTitle("Home");

    }


    private void initComponents() {
        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        navigationView = findViewById(R.id.nav_view);


        View headerView = navigationView.getHeaderView(0);
        tvLoginName = (TextView) headerView.findViewById(R.id.tvName);

        llNav = (LinearLayout) headerView.findViewById(R.id.llNav);

        llNav.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });


        getWindow().setSoftInputMode(WindowManager.
                LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

        if (mSharedPreference.getBoolean(ConstantCodes.IS_LOGIN, false)) {

            tvLoginName.setText(mSharedPreference.getString(ConstantCodes.LOGIN_USER_NAME, ""));

            navigationView.getMenu().clear();
            navigationView.inflateMenu(R.menu.activity_main_drawer_with_login);
        } else {

            navigationView.getMenu().clear();
            navigationView.inflateMenu(R.menu.activity_main_drawer_without_login);
        }

        navigationView.setNavigationItemSelectedListener(this);


    }

    private void setToolbar() {
        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        mToolbar.setTitle(getString(R.string.app_name));
        mToolbar.setTitleTextColor(ContextCompat.getColor(mContext, android.R.color.white));
        setSupportActionBar(mToolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayShowTitleEnabled(false);
        }
    }

    private void setDrawer() {
        mDrawerToggle = new ActionBarDrawerToggle(this, mDrawerLayout, mToolbar, R.string
                .navigation_drawer_open, R.string.navigation_drawer_close) {
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
                Utils.hideKeyboard(MainActivity.this);
            }
        };

        updateDrawerToggle();
        mDrawerToggle.setToolbarNavigationClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Utils.hideKeyboard(MainActivity.this);
                onBackPressed();
            }
        });
        mDrawerLayout.addDrawerListener(mDrawerToggle);
    }

    @Override
    protected void onResume() {
        super.onResume();
        getSupportFragmentManager().addOnBackStackChangedListener(mBackStackChangedListener);
    }

    @Override
    protected void onPause() {
        super.onPause();
        getSupportFragmentManager().removeOnBackStackChangedListener(mBackStackChangedListener);
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        mDrawerToggle.syncState();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        mDrawerToggle.onConfigurationChanged(newConfig);
    }

    /***
     * Listener for toolbar's back button
     */
    private FragmentManager.OnBackStackChangedListener mBackStackChangedListener =
            new FragmentManager.OnBackStackChangedListener() {
                @Override
                public void onBackStackChanged() {
                    updateDrawerToggle();
                }
            };

    /***
     * Update drawer toggle
     */
    private void updateDrawerToggle() {
        if (mDrawerToggle == null) {
            return;
        }
        boolean isRoot = getSupportFragmentManager().getBackStackEntryCount() == 0;
        mDrawerToggle.setDrawerIndicatorEnabled(isRoot);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayShowHomeEnabled(!isRoot);
            getSupportActionBar().setDisplayHomeAsUpEnabled(!isRoot);
            getSupportActionBar().setHomeButtonEnabled(!isRoot);
        }
        if (isRoot) {
            mDrawerToggle.syncState();
        }
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {

        String title = "";
        int id = item.getItemId();
        Fragment targetFragment = null;
        title = "Home";

        mToolbar.setVisibility(View.VISIBLE);


        if (id == R.id.drawer_home) {
          //  targetFragment = new HomeFragment();
            title = "Home";
        }

        if (id == R.id.drawer_logout) {

            mSharedPreference.edit().putBoolean(ConstantCodes.IS_LOGIN, false).apply();
            mSharedPreference.edit().putBoolean(ConstantCodes.SIGN_IN_WITH_GOOGLE, false).apply();
            mSharedPreference.edit().putBoolean(ConstantCodes.SIGN_IN_WITH_FACEBOOK, false).apply();

            mSharedPreference.edit().putString(ConstantCodes.LOGIN_USER_ID, "").apply();
            mSharedPreference.edit().putString(ConstantCodes.LOGIN_USER_NAME, "").apply();
            mSharedPreference.edit().putString(ConstantCodes.LOGIN_USER_EMAIL, "").apply();
            mSharedPreference.edit().putString(ConstantCodes.LOGIN_USER_PHONE, "").apply();

            Intent i = new Intent(MainActivity.this, LoginActivity.class);
            startActivity(i);
            finish();

           /* if (mSharedPreference.getBoolean(ConstantCodes.SIGN_IN_WITH_FACEBOOK, false)) {


            } else if (mSharedPreference.getBoolean(ConstantCodes.SIGN_IN_WITH_GOOGLE, false)) {

                GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                        .requestEmail()
                        .build();

                GoogleSignInClient mGoogleSignInClient = GoogleSignIn.getClient(MainActivity.this, gso);

                mGoogleSignInClient.signOut().addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        if (task.isSuccessful()) {
                            // There's immediate result available.

                            mSharedPreference.edit().putBoolean(ConstantCodes.IS_LOGIN, false).apply();
                            mSharedPreference.edit().putBoolean(ConstantCodes.SIGN_IN_WITH_GOOGLE, false).apply();
                            mSharedPreference.edit().putBoolean(ConstantCodes.SIGN_IN_WITH_FACEBOOK, false).apply();

                            mSharedPreference.edit().putBoolean(ConstantCodes.IS_LOGIN, false).apply();

                            mSharedPreference.edit().putString(ConstantCodes.LOGIN_USER_ID, "").apply();
                            mSharedPreference.edit().putString(ConstantCodes.LOGIN_USER_NAME, "").apply();
                            mSharedPreference.edit().putString(ConstantCodes.LOGIN_USER_EMAIL, "").apply();
                            mSharedPreference.edit().putString(ConstantCodes.LOGIN_USER_PHONE, "").apply();

                            Intent i = new Intent(MainActivity.this, LoginActivity.class);
                            startActivity(i);
                            finish();

                        }


                    }
                });
            } else {*/


            // }
        }

        if (id == R.id.drawer_login) {

            Intent i = new Intent(MainActivity.this, LoginActivity.class);
            startActivity(i);
            finish();

        }

        if (id == R.id.drawer_account) {
            targetFragment = new MyAccountFragment();
            title = "My Account";

        }

        if (id == R.id.drawer_orders) {
            targetFragment = new MyOrdersFragment();
            title = "My Orders";
        }

        if (id == R.id.drawer_wishlist) {
            targetFragment = new MyWishlistFragment();
            title = "My Wishlist";
        }

        if (id == R.id.drawer_cart) {
            targetFragment = new CartFragment();
            title = "Cart";
        }
        if (targetFragment != null) {
            setAppTitle(title);
            closeDrawer();
            changeFragment(targetFragment, title);
        }

        return true;
    }

    public void changeFragment(Fragment targetFragment, String tag) {


        for (int i = 0; i < getSupportFragmentManager().getBackStackEntryCount(); ++i) {
            getSupportFragmentManager().popBackStack();
        }
        getSupportFragmentManager().beginTransaction()
                .replace(R.id.frame_container, targetFragment, tag)
                .commit();
    }


    public void setAppTitle(String title) {
        mToolbar.setTitle(title);
    }

    private void showExitDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
        builder.setTitle(getString(R.string.message_alert));
        builder.setMessage(getString(R.string.message_exitapp));
        builder.setIcon(R.mipmap.ic_launcher);
        builder.setCancelable(false);
        builder.setPositiveButton(
                getString(R.string.message_yes),
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        finish();

                    }
                });
        builder.setNegativeButton(getResources().getString(R.string.message_no), new
                DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
        builder.create().show();
    }

    @Override
    public void onBackPressed() {

        if (getSupportFragmentManager().getBackStackEntryCount() > 0) {
            getSupportFragmentManager().popBackStack();
            mToolbar.setVisibility(View.VISIBLE);


        } else {
            if (mDrawerLayout.isDrawerOpen(GravityCompat.START)) {
                showExitDialog();
            } else if (!mDrawerLayout.isDrawerOpen(GravityCompat.START)) {
                mDrawerLayout.openDrawer(GravityCompat.START);
            }
        }

    }

    public void closeDrawer() {
        mDrawerLayout.closeDrawer(GravityCompat.START);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.option_menu, menu);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if (item.getItemId() == R.id.action_cart) {

            changeFragment(new CartFragment(), "Cart");
        }

        return super.onOptionsItemSelected(item);


    }
}
