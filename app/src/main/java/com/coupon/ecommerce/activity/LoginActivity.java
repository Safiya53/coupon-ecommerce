package com.coupon.ecommerce.activity;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.text.SpannableString;
import android.text.TextUtils;
import android.text.style.UnderlineSpan;
import android.util.Base64;
import android.util.Log;
import android.util.Patterns;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.coupon.ecommerce.R;
import com.coupon.ecommerce.application.MyApplication;
import com.coupon.ecommerce.constants.ConstantCodes;
import com.coupon.ecommerce.utils.Utils;
import com.google.android.material.textfield.TextInputLayout;

import org.json.JSONException;
import org.json.JSONObject;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class LoginActivity extends AppCompatActivity {

    TextView tvRegister, btnLogin, tvForgotPwd;
    EditText etMobile, etPassword;
    TextInputLayout tlMobile, tlPassword;
    RelativeLayout mRelativeMain;
    private ProgressBar mProgressBar;
    private SharedPreferences mSharedPreferences;

    MyApplication mApplication;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        mApplication = (MyApplication) this.getApplicationContext();
        mSharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        initailize();
        listners();

    }


    void initailize() {

        mProgressBar = findViewById(R.id.progressbar);
        mRelativeMain = findViewById(R.id.relative_main);

        etMobile = findViewById(R.id.etMobile);
        etPassword = findViewById(R.id.etPassword);
        tvRegister = findViewById(R.id.register);
        SpannableString content = new SpannableString("New Customer ? Register");
        content.setSpan(new UnderlineSpan(), 0, content.length(), 0);
        tvRegister.setText(content);

        btnLogin = findViewById(R.id.login);


        tlMobile = findViewById(R.id.tlMobile);
        tlPassword = findViewById(R.id.tlPassword);

        tvForgotPwd = findViewById(R.id.tvForgotPwd);

    }

    void listners() {

        tvRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(LoginActivity.this, RegisterActivity.class);
                startActivity(i);
            }
        });

        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (validation()) {

                    //networkCallLogin();

                    mSharedPreferences.edit().putBoolean(ConstantCodes.IS_LOGIN, true).apply();

                    Intent i = new Intent(LoginActivity.this, MainActivity.class);
                    startActivity(i);

                    finish();
                }


            }
        });

        tvForgotPwd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(LoginActivity.this, ForgotPassowordActivity.class);
                startActivity(i);

                finish();
            }
        });
    }


    /*private void networkCallLogin() {
        if (mApplication.isInternetConnected()) {

            mProgressBar.setVisibility(View.VISIBLE);

            mApplication.getRetroFitInterface().Login(etMobile.getText().toString() + "",
                    etPassword.getText().toString() + "").enqueue(mCallbackLogin);


        } else {
            Utils.showSnackBar(mRelativeMain, getResources().getString(R.string
                    .message_connection));
            mProgressBar.setVisibility(View.GONE);
        }
    }


    private Callback<PojoLogin> mCallbackLogin = new Callback<PojoLogin>() {
        @Override
        public void onResponse(Call<PojoLogin> call, Response<PojoLogin> response) {
            if (response != null && response.isSuccessful() && response.body() != null) {


                PojoLogin pojoParticipants = response.body();
                if (pojoParticipants.getStatus() == 1) {

                    mSharedPreferences.edit().putBoolean(ConstantCodes.IS_LOGIN, true).apply();

                    mSharedPreferences.edit().putString(ConstantCodes.LOGIN_USER_ID, "" + pojoParticipants.getData().getUserDetails().getId()).apply();
                    mSharedPreferences.edit().putString(ConstantCodes.LOGIN_USER_NAME, "" + pojoParticipants.getData().getUserDetails().getName()).apply();
                    mSharedPreferences.edit().putString(ConstantCodes.LOGIN_USER_EMAIL, "" + pojoParticipants.getData().getUserDetails().getEmail()).apply();
                    mSharedPreferences.edit().putString(ConstantCodes.LOGIN_USER_PHONE, "" + pojoParticipants.getData().getUserDetails().getPhoneNo()).apply();

                    mSharedPreferences.edit().putBoolean(ConstantCodes.SIGN_IN_WITH_GOOGLE, false).apply();
                    mSharedPreferences.edit().putBoolean(ConstantCodes.SIGN_IN_WITH_FACEBOOK, false).apply();

                    Intent i = new Intent(LoginActivity.this, MainActivity.class);
                    startActivity(i);

                    finish();

                } else {
                    if (!TextUtils.isEmpty(pojoParticipants.getMessage())) {
                        Utils.showSnackBar(mRelativeMain, pojoParticipants.getMessage());

                    } else {
                        Utils.showSnackBar(mRelativeMain, getString(R.string
                                .message_something_wrong));
                    }

                }
            } else {
                Utils.showSnackBar(mRelativeMain, getString(R.string.message_something_wrong));
                mProgressBar.setVisibility(View.GONE);
            }
            mProgressBar.setVisibility(View.GONE);

        }

        @Override
        public void onFailure(Call<PojoLogin> call, Throwable t) {
            Utils.showSnackBar(mRelativeMain, getString(R.string
                    .message_something_wrong));
            mProgressBar.setVisibility(View.GONE);
        }
    };

    void networkCallSocialLoginGoogle(String username, String token, String email) {
        if (mApplication.isInternetConnected()) {
            mApplication.getRetroFitInterface().GoogleLogin(email,
                    token, username).enqueue(mCallbackSocialLogin);

            mProgressBar.setVisibility(View.VISIBLE);
        } else {
            Utils.showSnackBar(mRelativeMain, getResources().getString(R.string
                    .message_connection));
            mProgressBar.setVisibility(View.GONE);
        }
    }

    *//**
     * Callback for network call for sub sub category
     *//*
    private Callback<PojoLogin> mCallbackSocialLogin = new Callback<PojoLogin>() {

        @Override
        public void onResponse(Call<PojoLogin> call, Response<PojoLogin>
                response) {
            if (response != null && response.isSuccessful() && response.body() != null) {

                PojoLogin pojoParticipants = response.body();

                Utils.showSnackBar(mRelativeMain, pojoParticipants.getMessage() + "");

                if (pojoParticipants.getStatus() == 1) {


                    mSharedPreferences.edit().putBoolean(ConstantCodes.IS_LOGIN, true).apply();

                    mSharedPreferences.edit().putString(ConstantCodes.LOGIN_USER_ID, "" + pojoParticipants.getData().getUserDetails().getId()).apply();
                    mSharedPreferences.edit().putString(ConstantCodes.LOGIN_USER_NAME, "" + pojoParticipants.getData().getUserDetails().getName()).apply();
                    mSharedPreferences.edit().putString(ConstantCodes.LOGIN_USER_EMAIL, "" + pojoParticipants.getData().getUserDetails().getEmail()).apply();
                    mSharedPreferences.edit().putString(ConstantCodes.LOGIN_USER_PHONE, "" + pojoParticipants.getData().getUserDetails().getPhoneNo()).apply();

                    mSharedPreferences.edit().putBoolean(ConstantCodes.SIGN_IN_WITH_GOOGLE, true).apply();
                    mSharedPreferences.edit().putBoolean(ConstantCodes.SIGN_IN_WITH_FACEBOOK, false).apply();

                    Intent i = new Intent(LoginActivity.this, MainActivity.class);
                    startActivity(i);

                    finish();

                }


                mProgressBar.setVisibility(View.GONE);
            }
        }

        @Override
        public void onFailure(Call<PojoLogin> call, Throwable t) {
            Utils.showSnackBar(mRelativeMain, getString(R.string
                    .message_something_wrong));
            mProgressBar.setVisibility(View.GONE);

        }
    };


    void networkCallSocialLoginFacebook(String username, String token, String email) {
        if (mApplication.isInternetConnected()) {
            mApplication.getRetroFitInterface().FacebookLogin(email,
                    token, username).enqueue(mCallbackSocialLoginFacebook);

            mProgressBar.setVisibility(View.VISIBLE);
        } else {
            Utils.showSnackBar(mRelativeMain, getResources().getString(R.string
                    .message_connection));
            mProgressBar.setVisibility(View.GONE);
        }
    }

    *//**
     * Callback for network call for sub sub category
     *//*
    private Callback<PojoLogin> mCallbackSocialLoginFacebook = new Callback<PojoLogin>() {

        @Override
        public void onResponse(Call<PojoLogin> call, Response<PojoLogin>
                response) {
            if (response != null && response.isSuccessful() && response.body() != null) {

                PojoLogin pojoParticipants = response.body();

                Utils.showSnackBar(mRelativeMain, pojoParticipants.getMessage() + "");

                if (pojoParticipants.getStatus() == 1) {


                    mSharedPreferences.edit().putBoolean(ConstantCodes.IS_LOGIN, true).apply();

                    mSharedPreferences.edit().putString(ConstantCodes.LOGIN_USER_ID, "" + pojoParticipants.getData().getUserDetails().getId()).apply();
                    mSharedPreferences.edit().putString(ConstantCodes.LOGIN_USER_NAME, "" + pojoParticipants.getData().getUserDetails().getName()).apply();
                    mSharedPreferences.edit().putString(ConstantCodes.LOGIN_USER_EMAIL, "" + pojoParticipants.getData().getUserDetails().getEmail()).apply();
                    mSharedPreferences.edit().putString(ConstantCodes.LOGIN_USER_PHONE, "" + pojoParticipants.getData().getUserDetails().getPhoneNo()).apply();

                    mSharedPreferences.edit().putBoolean(ConstantCodes.SIGN_IN_WITH_FACEBOOK, true).apply();

                    mSharedPreferences.edit().putBoolean(ConstantCodes.SIGN_IN_WITH_GOOGLE, false).apply();

                    Intent i = new Intent(LoginActivity.this, MainActivity.class);
                    startActivity(i);

                    finish();

                }


                mProgressBar.setVisibility(View.GONE);
            }
        }

        @Override
        public void onFailure(Call<PojoLogin> call, Throwable t) {
            Utils.showSnackBar(mRelativeMain, getString(R.string
                    .message_something_wrong));
            mProgressBar.setVisibility(View.GONE);

        }
    };*/

    boolean validation() {

        boolean name = validateMobile();
        boolean pwd = false;
        if (name) {
            pwd = validatePassword();
        }

        return name && pwd;

    }

    boolean validateMobile() {
        if (TextUtils.isEmpty(etMobile.getText().toString())) {
            tlMobile.setErrorEnabled(true);
            tlMobile.setError("Enter Phone No");
            return false;
        } else {
            tlMobile.setErrorEnabled(false);
            return true;
        }
    }

    boolean validatePassword() {
        if (TextUtils.isEmpty(etPassword.getText().toString())) {
            tlPassword.setErrorEnabled(true);
            tlPassword.setError("Enter Password");
            return false;
        } else {
            tlPassword.setErrorEnabled(false);
            return true;
        }
    }


}
