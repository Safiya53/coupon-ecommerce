package com.coupon.ecommerce.activity;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.text.TextUtils;
import android.util.Base64;
import android.util.Log;
import android.util.Patterns;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;


import androidx.appcompat.app.AppCompatActivity;

import com.coupon.ecommerce.R;
import com.coupon.ecommerce.application.MyApplication;
import com.coupon.ecommerce.constants.ConstantCodes;
import com.coupon.ecommerce.utils.Utils;
import com.google.android.material.textfield.TextInputLayout;

import org.json.JSONException;
import org.json.JSONObject;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;


public class RegisterActivity extends AppCompatActivity {


    private ProgressBar mProgressBar;
    private SharedPreferences mSharedPreferences;
    MyApplication mApplication;


    RelativeLayout mRelativeMain;

    EditText etFName, etLName, etCName, etEmail, etMobile, etPassword, etConfirmPassword;
    TextInputLayout tlFName, tlLName, tlCName, tlEmail, tlMobile, tlPassword, tlConfirmPassword;

    TextView tvRegister;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        mSharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        mApplication = (MyApplication) this.getApplicationContext();


        initailise();
        listners();


    }

    void initailise() {

        mProgressBar = findViewById(R.id.progressbar);
        mRelativeMain = findViewById(R.id.relative_main);


        etFName = findViewById(R.id.etFName);
        etLName = findViewById(R.id.etLName);
        etCName = findViewById(R.id.etCName);
        etEmail = findViewById(R.id.etEmail);
        etMobile = findViewById(R.id.etMobile);
        etPassword = findViewById(R.id.etPassword);
        etConfirmPassword = findViewById(R.id.etConfirmPassword);

        tvRegister = findViewById(R.id.register);

        tlFName = findViewById(R.id.tlFName);
        tlLName = findViewById(R.id.tlLName);
        tlCName = findViewById(R.id.tlCName);
        tlEmail = findViewById(R.id.tlEmail);
        tlMobile = findViewById(R.id.tlMobile);
        tlPassword = findViewById(R.id.tlPassword);
        tlConfirmPassword = findViewById(R.id.tlConfirmPassword);


    }

    void listners() {

        tvRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (validation()) {
                    mSharedPreferences.edit().putBoolean(ConstantCodes.IS_LOGIN, true).apply();

                    Intent i = new Intent(RegisterActivity.this, MainActivity.class);
                    startActivity(i);

                    finish();
                }

            }
        });


    }


   /* void networkCallRegisterOTP() {
        if (mApplication.isInternetConnected()) {
            mApplication.getRetroFitInterface().RegisterOtp(etMobile.getText().toString() + "",
                    etEmail.getText().toString() + "").
                    enqueue(mCallbackRegisterOTP);

            mProgressBar.setVisibility(View.VISIBLE);
        } else {
            Utils.showSnackBar(mRelativeMain, getResources().getString(R.string
                    .message_connection));
            mProgressBar.setVisibility(View.GONE);
        }
    }

    *//**
     * Callback for network call for sub sub category
     *//*
    private Callback<PojoRegisterOTP> mCallbackRegisterOTP = new Callback<PojoRegisterOTP>() {

        @Override
        public void onResponse(Call<PojoRegisterOTP> call, Response<PojoRegisterOTP>
                response) {
            if (response != null && response.isSuccessful() && response.body() != null) {

                PojoRegisterOTP pojoParticipants = response.body();


                if (pojoParticipants.getStatus() == 1) {

                    apiOtp = pojoParticipants.getOtp() + "";

                    tvRegister.setVisibility(View.VISIBLE);
                    tlOTP.setVisibility(View.VISIBLE);

                    tvRegisterOTP.setVisibility(View.GONE);
                    tlName.setVisibility(View.GONE);
                    tlEmail.setVisibility(View.GONE);
                    tlMobile.setVisibility(View.GONE);
                    tlPassword.setVisibility(View.GONE);
                    tlConfirmPassword.setVisibility(View.GONE);


                } else {
                    Utils.showSnackBar(mRelativeMain, "Email/Mobile is already taken");
                }


                mProgressBar.setVisibility(View.GONE);
            }
        }

        @Override
        public void onFailure(Call<PojoRegisterOTP> call, Throwable t) {
            Utils.showSnackBar(mRelativeMain, getString(R.string
                    .message_something_wrong));
            mProgressBar.setVisibility(View.GONE);

        }
    };*/

    /*void networkCallRegisterUser() {
        if (mApplication.isInternetConnected()) {
            mApplication.getRetroFitInterface().RegisterUser(etName.getText().toString() + "", etMobile.getText().toString() + "",
                    etEmail.getText().toString() + "", etPassword.getText().toString() + "",
                    etConfirmPassword.getText().toString() + "").
                    enqueue(mCallbackRegisterUser);

            mProgressBar.setVisibility(View.VISIBLE);
        } else {
            Utils.showSnackBar(mRelativeMain, getResources().getString(R.string
                    .message_connection));
            mProgressBar.setVisibility(View.GONE);
        }
    }

    */

    /**
     * Callback for network call for sub sub category
     *//*
    private Callback<PojoRegistration> mCallbackRegisterUser = new Callback<PojoRegistration>() {

        @Override
        public void onResponse(Call<PojoRegistration> call, Response<PojoRegistration>
                response) {
            if (response != null && response.isSuccessful() && response.body() != null) {

                PojoRegistration pojoParticipants = response.body();

                Utils.showSnackBar(mRelativeMain, pojoParticipants.getMessage() + "");

                if (pojoParticipants.getStatus() == 1) {

                    Intent i = new Intent(RegisterActivity.this, LoginActivity.class);
                    startActivity(i);

                    finish();

                }


                mProgressBar.setVisibility(View.GONE);
            }
        }

        @Override
        public void onFailure(Call<PojoRegistration> call, Throwable t) {
            Utils.showSnackBar(mRelativeMain, getString(R.string
                    .message_something_wrong));
            mProgressBar.setVisibility(View.GONE);

        }
    };
*/

    boolean validation() {
/*
        boolean name = va();
        boolean email = false;
        boolean mobile = false;
        boolean pwd = false;
        boolean confimPwd = false;
        if (name) {
            email = validateEmail();
            if (email) {
                mobile = validateMobile();
                if (mobile) {
                    pwd = validatePassword();
                    if (pwd) {
                        confimPwd = validateConfirmPassword();
                    }
                }
            }
        }

        return name && email && mobile && pwd && confimPwd;*/


        return validateFName() && validateLName() && validateCName() && validateEmail()
                && validatePassword() && validateConfirmPassword() && validateMobile();

    }

    boolean validateFName() {
        if (TextUtils.isEmpty(etFName.getText().toString())) {
            tlFName.setErrorEnabled(true);
            tlFName.setError("Enter First Name");
            return false;
        } else {
            tlFName.setErrorEnabled(false);
            return true;
        }
    }

    boolean validateLName() {
        if (TextUtils.isEmpty(etLName.getText().toString())) {
            tlLName.setErrorEnabled(true);
            tlLName.setError("Enter Last Name");
            return false;
        } else {
            tlLName.setErrorEnabled(false);
            return true;
        }
    }

    boolean validateCName() {
        if (TextUtils.isEmpty(etCName.getText().toString())) {
            tlCName.setErrorEnabled(true);
            tlCName.setError("Enter Company Name");
            return false;
        } else {
            tlCName.setErrorEnabled(false);
            return true;
        }
    }

    boolean validateEmail() {
        if (TextUtils.isEmpty(etEmail.getText().toString())) {
            tlEmail.setErrorEnabled(true);
            tlEmail.setError("Enter Email");
            return false;
        } else if (!Patterns.EMAIL_ADDRESS.matcher(etEmail.getText().toString().trim()).matches()) {
            tlEmail.setErrorEnabled(true);
            tlEmail.setError("Enter Valid Email");
            return false;
        } else {
            tlEmail.setErrorEnabled(false);
            return true;
        }
    }

    boolean validateMobile() {
        if (TextUtils.isEmpty(etMobile.getText().toString())) {
            tlMobile.setErrorEnabled(true);
            tlMobile.setError("Enter Mobile");
            return false;
        } else {
            tlMobile.setErrorEnabled(false);
            return true;
        }
    }

    boolean validatePassword() {
        if (TextUtils.isEmpty(etPassword.getText().toString())) {
            tlPassword.setErrorEnabled(true);
            tlPassword.setError("Enter Password");
            return false;
        } else {
            tlPassword.setErrorEnabled(false);
            return true;
        }
    }

    boolean validateConfirmPassword() {
        if (TextUtils.isEmpty(etPassword.getText().toString())) {
            tlConfirmPassword.setErrorEnabled(true);
            tlConfirmPassword.setError("Enter Confirm Password");
            return false;
        } else if (!etConfirmPassword.getText().toString().equals(etPassword.getText().toString())) {
            tlConfirmPassword.setErrorEnabled(true);
            tlConfirmPassword.setError("Password Does Not Match");
            return false;
        } else {
            tlConfirmPassword.setErrorEnabled(false);
            return true;
        }
    }


}
