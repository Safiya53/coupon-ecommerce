package com.coupon.ecommerce.activity;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.coupon.ecommerce.R;
import com.coupon.ecommerce.application.MyApplication;
import com.coupon.ecommerce.utils.Utils;


/**
 * Created by jaydeep on 26/6/18.
 */

public class ForgotPassowordActivity extends AppCompatActivity {

    ProgressBar mProgressBar;

    TextView btnSubmit;
    EditText etEmail;

    MyApplication mApplication;
    private SharedPreferences mSharedPreference;

    private Context mContext;
    private RelativeLayout mRelativeMain;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgot_password);
        mApplication = (MyApplication) this.getApplicationContext();

        mContext = ForgotPassowordActivity.this;

        mProgressBar = findViewById(R.id.progressbar);

        mRelativeMain = findViewById(R.id.activity_main);

        etEmail = findViewById(R.id.etEmail);

        btnSubmit = findViewById(R.id.dialogSubmit);


        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (validateEmail()) {

                    Intent i = new Intent(ForgotPassowordActivity.this, LoginActivity.class);
                    startActivity(i);

                    finish();

                    //networkCallForgotPassword();
                }

            }
        });
    }

    boolean validateEmail() {
        if (TextUtils.isEmpty(etEmail.getText().toString())) {
            Utils.showSnackBar(mRelativeMain, "Enter Phone No");
            return false;
        } else {
            return true;
        }
    }

   /* private void networkCallForgotPassword() {
        if (mApplication.isInternetConnected()) {
            mProgressBar.setVisibility(View.VISIBLE);
            mApplication.getRetroFitInterface().ForgotPassword(etEmail.getText().toString()).enqueue(mCallbackNew);

        } else {
            Utils.showSnackBar(mRelativeMain, mContext.getResources().getString(R.string
                    .message_connection));
            mProgressBar.setVisibility(View.GONE);
        }
    }


    private Callback<PojoForgotPwd> mCallbackNew = new Callback<PojoForgotPwd>() {

        @Override
        public void onResponse(Call<PojoForgotPwd> call, Response<PojoForgotPwd>
                response) {
            Log.e("successsss", "sucesssss");
            if (response != null && response.isSuccessful() && response.body() != null) {
                PojoForgotPwd pojoParticipants = response.body();
                Log.e("successsss11", "sucesssss");

                if (pojoParticipants.getStatus() == 1) {

                    showDialog(pojoParticipants.getMessage() + "", true);


                } else {
                    showDialog(pojoParticipants.getMessage() + "", false);
                }
            }

            mProgressBar.setVisibility(View.GONE);
        }

        @Override
        public void onFailure(Call<PojoForgotPwd> call, Throwable t) {
            Utils.showSnackBar(mRelativeMain, getString(R.string
                    .message_something_wrong));
            Log.e("faillll", "failll");
            mProgressBar.setVisibility(View.GONE);

        }
    };

*/
   /* private void showDialog(String msg, boolean status) {
        AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
        builder.setCancelable(false);
        builder.setMessage("" + msg);

        if (status) {
            builder.setTitle("Congratulations");
            builder.setIcon(R.drawable.ic_thumb_up);
            builder.setPositiveButton(
                    "Okay",
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();

                            Intent i = new Intent(ForgotPassowordActivity.this, LoginActivity.class);
                            startActivity(i);

                            finish();

                        }
                    });
        } else {
            builder.setTitle("Sorry");
            builder.setIcon(R.drawable.ic_thumb_down);
            builder.setPositiveButton(
                    "Try Again",
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();

                        }
                    });

        }
        builder.create().show();
    }*/

}
