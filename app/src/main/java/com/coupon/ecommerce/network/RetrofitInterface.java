package com.coupon.ecommerce.network;


import com.coupon.ecommerce.constants.ConstantCodes;
import com.coupon.ecommerce.pojo.PojoAddWishlist;
import com.coupon.ecommerce.pojo.PojoCity;
import com.coupon.ecommerce.pojo.PojoGetUserAddress;
import com.coupon.ecommerce.pojo.PojoHomeShopCategory;
import com.coupon.ecommerce.pojo.PojoMyOrder;
import com.coupon.ecommerce.pojo.PojoProductList;
import com.coupon.ecommerce.pojo.PojoSaveUserAddress;
import com.coupon.ecommerce.pojo.PojoSubmitOrder;
import com.coupon.ecommerce.pojo.PojoWishlist;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;
import retrofit2.http.Path;

/**
 * Retrofit interface for ws.wolfsoft.propertyplanetapp.network calls
 */
public interface RetrofitInterface {

    @FormUrlEncoded
    @POST(ConstantCodes.METHOD_SHOP_BY_CATEGORY)
    Call<PojoHomeShopCategory> ShopByCategory(@Field("api_token") String api_token);

    @FormUrlEncoded
    @POST(ConstantCodes.METHOD_PRODUCT_LIST + "/{catType}" + "/{catSubtype}")
    Call<PojoProductList> ProductList(@Field("api_token") String api_token,
                                      @Field("page") int page,
                                      @Path("catType") String catType,
                                      @Path("catSubtype") String catSubtype);


    @FormUrlEncoded
    @POST(ConstantCodes.METHOD_PRODUCT_LIST + "/{catType}" + "/{catSubtype}" + "/{catSubSubtype}")
    Call<PojoProductList> ProductListSubSUb(@Field("api_token") String api_token,
                                            @Field("page") int page,
                                            @Path("catType") String catType,
                                            @Path("catSubtype") String catSubtype,
                                            @Path("catSubSubtype") String catSubSubtype);

    @FormUrlEncoded
    @POST(ConstantCodes.METHOD_PRODUCT_LIST + "/{catType}")
    Call<PojoProductList> ProductListCategory(@Field("api_token") String api_token,
                                              @Field("page") int page,
                                              @Path("catType") String catType);

    @FormUrlEncoded
    @POST(ConstantCodes.METHOD_ADD_TO_WISHLIST)
    Call<PojoAddWishlist> AddToWishlist(@Field("api_token") String api_token,
                                        @Field("user_id") String user_id,
                                        @Field("product_id") String product_id);

    @FormUrlEncoded
    @POST(ConstantCodes.METHOD_REMOVE_TO_WISHLIST)
    Call<PojoAddWishlist> RemoveToWishlist(@Field("api_token") String api_token,
                                           @Field("user_id") String user_id,
                                           @Field("product_id") String product_id);

    @FormUrlEncoded
    @POST(ConstantCodes.METHOD_GET_ADDRESS)
    Call<PojoGetUserAddress> GetUserAddress(@Field("api_token") String api_token,
                                            @Field("user_id") String user_id);

    @FormUrlEncoded
    @POST(ConstantCodes.METHOD_SAVE_ADDRESS)
    Call<PojoSaveUserAddress> SaveUserAddress(@Field("api_token") String api_token,
                                              @Field("user_id") String user_id,
                                              @Field("city_id") String city_id,
                                              @Field("landmark") String landmark,
                                              @Field("address") String address,
                                              @Field("zipcode") String zipcode,
                                              @Field("address_type") String address_type,
                                              @Field("default_address_set") String default_address_set,
                                              @Field("phone") String phone,
                                              @Field("address_id") String address_id,
                                              @Field("title") String title);

    @FormUrlEncoded
    @POST(ConstantCodes.METHOD_GET_CITY)
    Call<PojoCity> City(@Field("api_token") String api_token);

    @FormUrlEncoded
    @POST(ConstantCodes.METHOD_ORDER_SUBMIT)
    Call<PojoSubmitOrder> OrderSubmit(@Field("api_token") String api_token,
                                      @Field("user_id") String user_id,
                                      @Field("method") String method,
                                      @Field("sub_total") String sub_total,
                                      @Field("grand_total") String grand_total,
                                      @Field("discount") String discount,
                                      @Field("promocode_id") String promocode_id,
                                      @Field("name") String name,
                                      @Field("email") String email,
                                      @Field("default_address") String default_address,
                                      @Field("product_list") String product_list);

    @FormUrlEncoded
    @POST(ConstantCodes.METHOD_WISHLIST)
    Call<PojoWishlist> GetWishlist(@Field("api_token") String api_token,
                                   @Field("id") String userId);

    @FormUrlEncoded
    @POST(ConstantCodes.METHOD_MY_ORDER)
    Call<PojoMyOrder> GetMyOrder(@Field("api_token") String api_token,
                                 @Field("id") String userId);

   /* @FormUrlEncoded
    @POST(ConstantCodes.METHOD_HOME_SECTION1)
    Call<PojoHomeSection1> HomeSection1(@Field("api_token") String api_token);

    @FormUrlEncoded
    @POST(ConstantCodes.METHOD_HOME_SECTION2)
    Call<PojoHomeSection2> HomeSection2(@Field("api_token") String api_token);

    @FormUrlEncoded
    @POST(ConstantCodes.METHOD_HOME_SECTION3)
    Call<PojoHomeSection3> HomeSection3(@Field("api_token") String api_token);

    @FormUrlEncoded
    @POST(ConstantCodes.METHOD_SHOP_BY_CATEGORY)
    Call<PojoHomeShopCategory> ShopByCategory(@Field("api_token") String api_token);

    @FormUrlEncoded
    @POST(ConstantCodes.METHOD_PRODUCT_LIST + "/{catType}" + "/{catSubtype}")
    Call<PojoProductList> ProductList(@Field("api_token") String api_token,
                                      @Field("page") int page,
                                      @Path("catType") String catType,
                                      @Path("catSubtype") String catSubtype);


    @FormUrlEncoded
    @POST(ConstantCodes.METHOD_PRODUCT_LIST + "/{catType}" + "/{catSubtype}" + "/{catSubSubtype}")
    Call<PojoProductList> ProductListSubSUb(@Field("api_token") String api_token,
                                            @Field("page") int page,
                                            @Path("catType") String catType,
                                            @Path("catSubtype") String catSubtype,
                                            @Path("catSubSubtype") String catSubSubtype);

    @FormUrlEncoded
    @POST(ConstantCodes.METHOD_PRODUCT_LIST + "/{catType}")
    Call<PojoProductList> ProductListCategory(@Field("api_token") String api_token,
                                              @Field("page") int page,
                                              @Path("catType") String catType);

    @FormUrlEncoded
    @POST(ConstantCodes.METHOD_PRODUCT_DETAIL + "/{product_id}" + "/{user_id}")
    Call<PojoProductDetails> ProductDetails(@Field("api_token") String api_token,
                                            @Path("product_id") String product_id,
                                            @Path("user_id") String user_id);


    @FormUrlEncoded
    @POST(ConstantCodes.METHOD_LOGIN)
    Call<PojoLogin> Login(@Field("phone") String phone,
                          @Field("password") String password);

    @FormUrlEncoded
    @POST(ConstantCodes.METHOD_GOOGLE_LOGIN)
    Call<PojoLogin> GoogleLogin(@Field("email") String email,
                                @Field("token") String token,
                                @Field("name") String name);

    @FormUrlEncoded
    @POST(ConstantCodes.METHOD_FACEBOOK_LOGIN)
    Call<PojoLogin> FacebookLogin(@Field("email") String email,
                                  @Field("token") String token,
                                  @Field("name") String name);

    @FormUrlEncoded
    @POST(ConstantCodes.METHOD_REGISTER_OTP)
    Call<PojoRegisterOTP> RegisterOtp(@Field("phone") String phone,
                                      @Field("email") String email);

    @FormUrlEncoded
    @POST(ConstantCodes.METHOD_USER_REGISTER)
    Call<PojoRegistration> RegisterUser(@Field("name") String name,
                                        @Field("phone") String phone,
                                        @Field("email") String email,
                                        @Field("password") String password,
                                        @Field("password_confirmation") String password_confirmation);

    @FormUrlEncoded
    @POST(ConstantCodes.METHOD_ADD_TO_WISHLIST)
    Call<PojoAddWishlist> AddToWishlist(@Field("api_token") String api_token,
                                        @Field("user_id") String user_id,
                                        @Field("product_id") String product_id);

    @FormUrlEncoded
    @POST(ConstantCodes.METHOD_REMOVE_TO_WISHLIST)
    Call<PojoAddWishlist> RemoveToWishlist(@Field("api_token") String api_token,
                                           @Field("user_id") String user_id,
                                           @Field("product_id") String product_id);

    @FormUrlEncoded
    @POST(ConstantCodes.METHOD_CHECK_PROMO_CODE)
    Call<PojoCheckPromo> CheckPromoCode(@Field("api_token") String api_token,
                                        @Field("promocode") String promocode,
                                        @Field("cart") String cart);

    @FormUrlEncoded
    @POST(ConstantCodes.METHOD_GET_ADDRESS)
    Call<PojoGetUserAddress> GetUserAddress(@Field("api_token") String api_token,
                                            @Field("user_id") String user_id);

    @FormUrlEncoded
    @POST(ConstantCodes.METHOD_SAVE_ADDRESS)
    Call<PojoSaveUserAddress> SaveUserAddress(@Field("api_token") String api_token,
                                              @Field("user_id") String user_id,
                                              @Field("city_id") String city_id,
                                              @Field("landmark") String landmark,
                                              @Field("address") String address,
                                              @Field("zipcode") String zipcode,
                                              @Field("address_type") String address_type,
                                              @Field("default_address_set") String default_address_set,
                                              @Field("phone") String phone,
                                              @Field("address_id") String address_id,
                                              @Field("title") String title);

    @FormUrlEncoded
    @POST(ConstantCodes.METHOD_GET_CITY)
    Call<PojoCity> City(@Field("api_token") String api_token);

    @FormUrlEncoded()
    @POST(ConstantCodes.METHOD_PAYTM_GENERATE_CHECKSUM)
    Call<CallbackChecksum> generateChecksum(@Field("api_token") String api_token,
                                            @Field("MID") String merchantId,
                                            @Field("ORDER_ID") String orderId,
                                            @Field("CUST_ID") String customerId,
                                            @Field("CHANNEL_ID") String channelId,
                                            @Field("TXN_AMOUNT") String taxAmount,
                                            @Field("WEBSITE") String website,
                                            @Field("CALLBACK_URL") String callbackUrl,
                                            @Field("INDUSTRY_TYPE_ID") String industryType);

    @FormUrlEncoded
    @POST(ConstantCodes.METHOD_WISHLIST)
    Call<PojoWishlist> GetWishlist(@Field("api_token") String api_token,
                                   @Field("id") String userId);

    @FormUrlEncoded
    @POST(ConstantCodes.METHOD_NEWSLETTER_SUBSCRIPTION)
    Call<PojoSubscribe> NewsletterSubscription(@Field("api_token") String api_token,
                                               @Field("email") String email);

    @FormUrlEncoded
    @POST(ConstantCodes.METHOD_MY_ORDER)
    Call<PojoMyOrder> GetMyOrder(@Field("api_token") String api_token,
                                 @Field("id") String userId);

    @FormUrlEncoded
    @POST(ConstantCodes.METHOD_BRAND_WISE_PRODUCT + "/{brand_id}")
    Call<PojoProductList> HomeBrandProduct(@Field("api_token") String api_token,
                                           @Field("page") int page,
                                           @Path("brand_id") String brandId);

    @FormUrlEncoded
    @POST(ConstantCodes.METHOD_ORDER_SUBMIT)
    Call<PojoSubmitOrder> OrderSubmit(@Field("api_token") String api_token,
                                      @Field("user_id") String user_id,
                                      @Field("method") String method,
                                      @Field("sub_total") String sub_total,
                                      @Field("grand_total") String grand_total,
                                      @Field("discount") String discount,
                                      @Field("promocode_id") String promocode_id,
                                      @Field("name") String name,
                                      @Field("email") String email,
                                      @Field("default_address") String default_address,
                                      @Field("product_list") String product_list);
*/
}