package com.coupon.ecommerce.pojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class PojoHomeSection1 {

    @SerializedName("status")
    @Expose
    private Boolean status;
    @SerializedName("data")
    @Expose
    private Data data;

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }

    public class Banner {

        @SerializedName("id")
        @Expose
        private Integer id;
        @SerializedName("banner_image")
        @Expose
        private String bannerImage;
        @SerializedName("is_featured")
        @Expose
        private String isFeatured;

        public Integer getId() {
            return id;
        }

        public void setId(Integer id) {
            this.id = id;
        }

        public String getBannerImage() {
            return bannerImage;
        }

        public void setBannerImage(String bannerImage) {
            this.bannerImage = bannerImage;
        }

        public String getIsFeatured() {
            return isFeatured;
        }

        public void setIsFeatured(String isFeatured) {
            this.isFeatured = isFeatured;
        }

    }


    public class Data {

        @SerializedName("banners")
        @Expose
        private List<Banner> banners = null;
        @SerializedName("latest_products")
        @Expose
        private List<LatestProduct> latestProducts = null;
        @SerializedName("offer_banners")
        @Expose
        private List<OfferBanner> offerBanners = null;

        public List<Banner> getBanners() {
            return banners;
        }

        public void setBanners(List<Banner> banners) {
            this.banners = banners;
        }

        public List<LatestProduct> getLatestProducts() {
            return latestProducts;
        }

        public void setLatestProducts(List<LatestProduct> latestProducts) {
            this.latestProducts = latestProducts;
        }

        public List<OfferBanner> getOfferBanners() {
            return offerBanners;
        }

        public void setOfferBanners(List<OfferBanner> offerBanners) {
            this.offerBanners = offerBanners;
        }

    }

    public class LatestProduct {

        @SerializedName("id")
        @Expose
        private Integer id;
        @SerializedName("name")
        @Expose
        private String name;
        @SerializedName("product_image")
        @Expose
        private String productImage;
        @SerializedName("product_price")
        @Expose
        private String productPrice;
        @SerializedName("selling_price")
        @Expose
        private String sellingPrice;

        public Integer getId() {
            return id;
        }

        public void setId(Integer id) {
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getProductImage() {
            return productImage;
        }

        public void setProductImage(String productImage) {
            this.productImage = productImage;
        }

        public String getProductPrice() {
            return productPrice;
        }

        public void setProductPrice(String productPrice) {
            this.productPrice = productPrice;
        }

        public String getSellingPrice() {
            return sellingPrice;
        }

        public void setSellingPrice(String sellingPrice) {
            this.sellingPrice = sellingPrice;
        }

    }

    public class OfferBanner {

        @SerializedName("id")
        @Expose
        private Integer id;
        @SerializedName("banner_image")
        @Expose
        private String bannerImage;

        public Integer getId() {
            return id;
        }

        public void setId(Integer id) {
            this.id = id;
        }

        public String getBannerImage() {
            return bannerImage;
        }

        public void setBannerImage(String bannerImage) {
            this.bannerImage = bannerImage;
        }

    }

}
