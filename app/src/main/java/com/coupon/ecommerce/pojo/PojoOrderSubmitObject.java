package com.coupon.ecommerce.pojo;

import java.util.ArrayList;

public class PojoOrderSubmitObject {

    String api_token, user_id, method, sub_total, grand_total, discount, promocode_id, name, email, default_address;
    ArrayList<CartModel> product_list;

    public PojoOrderSubmitObject(String api_token, String user_id, String method, String sub_total, String grand_total,
                                 String discount, String promocode_id, String name, String email, String default_address,
                                 ArrayList<CartModel> product_list) {

        this.api_token = api_token;
        this.user_id = user_id;
        this.method = method;
        this.sub_total = sub_total;
        this.grand_total = grand_total;
        this.discount = discount;
        this.promocode_id = promocode_id;
        this.name = name;
        this.email = email;
        this.default_address = default_address;
        this.product_list = product_list;
    }

}
