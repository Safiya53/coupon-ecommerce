package com.coupon.ecommerce.pojo;

public class CartModel {

    String product_id;
    String product_code;
    String product_price;
    String product_quantity;

    public CartModel() {
    }

    public CartModel(String product_id, String product_code, String product_price, String product_quantity) {

        this.product_id = product_id;
        this.product_code = product_code;
        this.product_price = product_price;
        this.product_quantity = product_quantity;

    }

    public String getProduct_quantity() {
        return product_quantity;
    }

    public void setProduct_quantity(String product_quantity) {
        this.product_quantity = product_quantity;
    }


    public String getProduct_id() {
        return product_id;
    }

    public void setProduct_id(String product_id) {
        this.product_id = product_id;
    }

    public String getProduct_code() {
        return product_code;
    }

    public void setProduct_code(String product_code) {
        this.product_code = product_code;
    }

    public String getProduct_price() {
        return product_price;
    }

    public void setProduct_price(String product_price) {
        this.product_price = product_price;
    }
}
