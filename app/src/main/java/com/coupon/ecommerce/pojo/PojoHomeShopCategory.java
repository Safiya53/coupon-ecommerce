package com.coupon.ecommerce.pojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class PojoHomeShopCategory {


    @SerializedName("status")
    @Expose
    private Boolean status;
    @SerializedName("menu_list")
    @Expose
    private List<MenuList> menuList = null;

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public List<MenuList> getMenuList() {
        return menuList;
    }

    public void setMenuList(List<MenuList> menuList) {
        this.menuList = menuList;
    }


    public class Chilcategory {

        @SerializedName("id")
        @Expose
        private Integer id;
        @SerializedName("brand_id")
        @Expose
        private String brandId;
        @SerializedName("cat_id")
        @Expose
        private String catId;
        @SerializedName("subcat_id")
        @Expose
        private String subcatId;
        @SerializedName("child_category_name")
        @Expose
        private String childCategoryName;
        @SerializedName("child_category_image")
        @Expose
        private String childCategoryImage;
        @SerializedName("is_child_featured")
        @Expose
        private String isChildFeatured;
        @SerializedName("status")
        @Expose
        private String status;
        @SerializedName("created_at")
        @Expose
        private String createdAt;
        @SerializedName("updated_at")
        @Expose
        private String updatedAt;
        @SerializedName("deleted_at")
        @Expose
        private Object deletedAt;
        @SerializedName("cgst")
        @Expose
        private String cgst;
        @SerializedName("sgst")
        @Expose
        private String sgst;
        @SerializedName("igst")
        @Expose
        private String igst;

        public Integer getId() {
            return id;
        }

        public void setId(Integer id) {
            this.id = id;
        }

        public String getBrandId() {
            return brandId;
        }

        public void setBrandId(String brandId) {
            this.brandId = brandId;
        }

        public String getCatId() {
            return catId;
        }

        public void setCatId(String catId) {
            this.catId = catId;
        }

        public String getSubcatId() {
            return subcatId;
        }

        public void setSubcatId(String subcatId) {
            this.subcatId = subcatId;
        }

        public String getChildCategoryName() {
            return childCategoryName;
        }

        public void setChildCategoryName(String childCategoryName) {
            this.childCategoryName = childCategoryName;
        }

        public String getChildCategoryImage() {
            return childCategoryImage;
        }

        public void setChildCategoryImage(String childCategoryImage) {
            this.childCategoryImage = childCategoryImage;
        }

        public String getIsChildFeatured() {
            return isChildFeatured;
        }

        public void setIsChildFeatured(String isChildFeatured) {
            this.isChildFeatured = isChildFeatured;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public String getCreatedAt() {
            return createdAt;
        }

        public void setCreatedAt(String createdAt) {
            this.createdAt = createdAt;
        }

        public String getUpdatedAt() {
            return updatedAt;
        }

        public void setUpdatedAt(String updatedAt) {
            this.updatedAt = updatedAt;
        }

        public Object getDeletedAt() {
            return deletedAt;
        }

        public void setDeletedAt(Object deletedAt) {
            this.deletedAt = deletedAt;
        }

        public String getCgst() {
            return cgst;
        }

        public void setCgst(String cgst) {
            this.cgst = cgst;
        }

        public String getSgst() {
            return sgst;
        }

        public void setSgst(String sgst) {
            this.sgst = sgst;
        }

        public String getIgst() {
            return igst;
        }

        public void setIgst(String igst) {
            this.igst = igst;
        }

    }



    public class MenuList implements Serializable {

        @SerializedName("id")
        @Expose
        private Integer id;
        @SerializedName("brand_id")
        @Expose
        private String brandId;
        @SerializedName("category_name")
        @Expose
        private String categoryName;
        @SerializedName("category_image")
        @Expose
        private String categoryImage;
        @SerializedName("isfeatured")
        @Expose
        private String isfeatured;
        @SerializedName("status")
        @Expose
        private String status;
        @SerializedName("created_at")
        @Expose
        private String createdAt;
        @SerializedName("updated_at")
        @Expose
        private String updatedAt;
        @SerializedName("deleted_at")
        @Expose
        private Object deletedAt;
        @SerializedName("subcategories")
        @Expose
        private List<Subcategory> subcategories = null;

        public Integer getId() {
            return id;
        }

        public void setId(Integer id) {
            this.id = id;
        }

        public String getBrandId() {
            return brandId;
        }

        public void setBrandId(String brandId) {
            this.brandId = brandId;
        }

        public String getCategoryName() {
            return categoryName;
        }

        public void setCategoryName(String categoryName) {
            this.categoryName = categoryName;
        }

        public String getCategoryImage() {
            return categoryImage;
        }

        public void setCategoryImage(String categoryImage) {
            this.categoryImage = categoryImage;
        }

        public String getIsfeatured() {
            return isfeatured;
        }

        public void setIsfeatured(String isfeatured) {
            this.isfeatured = isfeatured;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public String getCreatedAt() {
            return createdAt;
        }

        public void setCreatedAt(String createdAt) {
            this.createdAt = createdAt;
        }

        public String getUpdatedAt() {
            return updatedAt;
        }

        public void setUpdatedAt(String updatedAt) {
            this.updatedAt = updatedAt;
        }

        public Object getDeletedAt() {
            return deletedAt;
        }

        public void setDeletedAt(Object deletedAt) {
            this.deletedAt = deletedAt;
        }

        public List<Subcategory> getSubcategories() {
            return subcategories;
        }

        public void setSubcategories(List<Subcategory> subcategories) {
            this.subcategories = subcategories;
        }

    }

    public class Subcategory {

        @SerializedName("id")
        @Expose
        private Integer id;
        @SerializedName("brand_id")
        @Expose
        private String brandId;
        @SerializedName("cat_id")
        @Expose
        private String catId;
        @SerializedName("subcategory_name")
        @Expose
        private String subcategoryName;
        @SerializedName("subcategory_image")
        @Expose
        private String subcategoryImage;
        @SerializedName("isfeatured")
        @Expose
        private String isfeatured;
        @SerializedName("status")
        @Expose
        private String status;
        @SerializedName("created_at")
        @Expose
        private String createdAt;
        @SerializedName("updated_at")
        @Expose
        private String updatedAt;
        @SerializedName("deleted_at")
        @Expose
        private Object deletedAt;
        @SerializedName("chilcategories")
        @Expose
        private List<Chilcategory> chilcategories = null;

        public Integer getId() {
            return id;
        }

        public void setId(Integer id) {
            this.id = id;
        }

        public String getBrandId() {
            return brandId;
        }

        public void setBrandId(String brandId) {
            this.brandId = brandId;
        }

        public String getCatId() {
            return catId;
        }

        public void setCatId(String catId) {
            this.catId = catId;
        }

        public String getSubcategoryName() {
            return subcategoryName;
        }

        public void setSubcategoryName(String subcategoryName) {
            this.subcategoryName = subcategoryName;
        }

        public String getSubcategoryImage() {
            return subcategoryImage;
        }

        public void setSubcategoryImage(String subcategoryImage) {
            this.subcategoryImage = subcategoryImage;
        }

        public String getIsfeatured() {
            return isfeatured;
        }

        public void setIsfeatured(String isfeatured) {
            this.isfeatured = isfeatured;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public String getCreatedAt() {
            return createdAt;
        }

        public void setCreatedAt(String createdAt) {
            this.createdAt = createdAt;
        }

        public String getUpdatedAt() {
            return updatedAt;
        }

        public void setUpdatedAt(String updatedAt) {
            this.updatedAt = updatedAt;
        }

        public Object getDeletedAt() {
            return deletedAt;
        }

        public void setDeletedAt(Object deletedAt) {
            this.deletedAt = deletedAt;
        }

        public List<Chilcategory> getChilcategories() {
            return chilcategories;
        }

        public void setChilcategories(List<Chilcategory> chilcategories) {
            this.chilcategories = chilcategories;
        }

    }
}
