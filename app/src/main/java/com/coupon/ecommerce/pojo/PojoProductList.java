package com.coupon.ecommerce.pojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class PojoProductList {

    @SerializedName("status")
    @Expose
    private Boolean status;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("products")
    @Expose
    private Products products;

    public Boolean getStatus() {
        return status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public Products getProducts() {
        return products;
    }

    public void setProducts(Products products) {
        this.products = products;
    }


    public class Category {

        @SerializedName("id")
        @Expose
        private Integer id;
        @SerializedName("brand_id")
        @Expose
        private String brandId;
        @SerializedName("category_name")
        @Expose
        private String categoryName;
        @SerializedName("category_image")
        @Expose
        private String categoryImage;
        @SerializedName("isfeatured")
        @Expose
        private String isfeatured;
        @SerializedName("status")
        @Expose
        private String status;
        @SerializedName("created_at")
        @Expose
        private String createdAt;
        @SerializedName("updated_at")
        @Expose
        private String updatedAt;
        @SerializedName("deleted_at")
        @Expose
        private Object deletedAt;

        public Integer getId() {
            return id;
        }

        public void setId(Integer id) {
            this.id = id;
        }

        public String getBrandId() {
            return brandId;
        }

        public void setBrandId(String brandId) {
            this.brandId = brandId;
        }

        public String getCategoryName() {
            return categoryName;
        }

        public void setCategoryName(String categoryName) {
            this.categoryName = categoryName;
        }

        public String getCategoryImage() {
            return categoryImage;
        }

        public void setCategoryImage(String categoryImage) {
            this.categoryImage = categoryImage;
        }

        public String getIsfeatured() {
            return isfeatured;
        }

        public void setIsfeatured(String isfeatured) {
            this.isfeatured = isfeatured;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public String getCreatedAt() {
            return createdAt;
        }

        public void setCreatedAt(String createdAt) {
            this.createdAt = createdAt;
        }

        public String getUpdatedAt() {
            return updatedAt;
        }

        public void setUpdatedAt(String updatedAt) {
            this.updatedAt = updatedAt;
        }

        public Object getDeletedAt() {
            return deletedAt;
        }

        public void setDeletedAt(Object deletedAt) {
            this.deletedAt = deletedAt;
        }

    }


    public class Childsubcategory {

        @SerializedName("id")
        @Expose
        private Integer id;
        @SerializedName("brand_id")
        @Expose
        private String brandId;
        @SerializedName("cat_id")
        @Expose
        private String catId;
        @SerializedName("subcat_id")
        @Expose
        private String subcatId;
        @SerializedName("child_category_name")
        @Expose
        private String childCategoryName;
        @SerializedName("child_category_image")
        @Expose
        private String childCategoryImage;
        @SerializedName("is_child_featured")
        @Expose
        private String isChildFeatured;
        @SerializedName("status")
        @Expose
        private String status;
        @SerializedName("created_at")
        @Expose
        private String createdAt;
        @SerializedName("updated_at")
        @Expose
        private String updatedAt;
        @SerializedName("deleted_at")
        @Expose
        private Object deletedAt;
        @SerializedName("cgst")
        @Expose
        private String cgst;
        @SerializedName("sgst")
        @Expose
        private String sgst;
        @SerializedName("igst")
        @Expose
        private String igst;

        public Integer getId() {
            return id;
        }

        public void setId(Integer id) {
            this.id = id;
        }

        public String getBrandId() {
            return brandId;
        }

        public void setBrandId(String brandId) {
            this.brandId = brandId;
        }

        public String getCatId() {
            return catId;
        }

        public void setCatId(String catId) {
            this.catId = catId;
        }

        public String getSubcatId() {
            return subcatId;
        }

        public void setSubcatId(String subcatId) {
            this.subcatId = subcatId;
        }

        public String getChildCategoryName() {
            return childCategoryName;
        }

        public void setChildCategoryName(String childCategoryName) {
            this.childCategoryName = childCategoryName;
        }

        public String getChildCategoryImage() {
            return childCategoryImage;
        }

        public void setChildCategoryImage(String childCategoryImage) {
            this.childCategoryImage = childCategoryImage;
        }

        public String getIsChildFeatured() {
            return isChildFeatured;
        }

        public void setIsChildFeatured(String isChildFeatured) {
            this.isChildFeatured = isChildFeatured;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public String getCreatedAt() {
            return createdAt;
        }

        public void setCreatedAt(String createdAt) {
            this.createdAt = createdAt;
        }

        public String getUpdatedAt() {
            return updatedAt;
        }

        public void setUpdatedAt(String updatedAt) {
            this.updatedAt = updatedAt;
        }

        public Object getDeletedAt() {
            return deletedAt;
        }

        public void setDeletedAt(Object deletedAt) {
            this.deletedAt = deletedAt;
        }

        public String getCgst() {
            return cgst;
        }

        public void setCgst(String cgst) {
            this.cgst = cgst;
        }

        public String getSgst() {
            return sgst;
        }

        public void setSgst(String sgst) {
            this.sgst = sgst;
        }

        public String getIgst() {
            return igst;
        }

        public void setIgst(String igst) {
            this.igst = igst;
        }

    }


    public class Datum {

        @SerializedName("id")
        @Expose
        private Integer id;
        @SerializedName("brand_id")
        @Expose
        private String brandId;
        @SerializedName("cat_id")
        @Expose
        private String catId;
        @SerializedName("subcat_id")
        @Expose
        private String subcatId;
        @SerializedName("child_cat_id")
        @Expose
        private String childCatId;
        @SerializedName("product_name")
        @Expose
        private String productName;
        @SerializedName("product_slug")
        @Expose
        private String productSlug;
        @SerializedName("short_desc")
        @Expose
        private String shortDesc;
        @SerializedName("long_desc")
        @Expose
        private String longDesc;
        @SerializedName("available_from")
        @Expose
        private Object availableFrom;
        @SerializedName("available_to")
        @Expose
        private Object availableTo;
        @SerializedName("display_type")
        @Expose
        private Object displayType;
        @SerializedName("product_type")
        @Expose
        private String productType;
        @SerializedName("status")
        @Expose
        private String status;
        @SerializedName("created_at")
        @Expose
        private String createdAt;
        @SerializedName("updated_at")
        @Expose
        private String updatedAt;
        @SerializedName("deleted_at")
        @Expose
        private Object deletedAt;
        @SerializedName("meta_title")
        @Expose
        private Object metaTitle;
        @SerializedName("meta_keywords")
        @Expose
        private Object metaKeywords;
        @SerializedName("meta_description")
        @Expose
        private Object metaDescription;
        @SerializedName("products_inventories")
        @Expose
        private List<ProductsInventory> productsInventories = null;
        @SerializedName("category")
        @Expose
        private Category category;
        @SerializedName("subcategory")
        @Expose
        private Subcategory subcategory;
        @SerializedName("childsubcategory")
        @Expose
        private Childsubcategory childsubcategory;

        public Integer getId() {
            return id;
        }

        public void setId(Integer id) {
            this.id = id;
        }

        public String getBrandId() {
            return brandId;
        }

        public void setBrandId(String brandId) {
            this.brandId = brandId;
        }

        public String getCatId() {
            return catId;
        }

        public void setCatId(String catId) {
            this.catId = catId;
        }

        public String getSubcatId() {
            return subcatId;
        }

        public void setSubcatId(String subcatId) {
            this.subcatId = subcatId;
        }

        public String getChildCatId() {
            return childCatId;
        }

        public void setChildCatId(String childCatId) {
            this.childCatId = childCatId;
        }

        public String getProductName() {
            return productName;
        }

        public void setProductName(String productName) {
            this.productName = productName;
        }

        public String getProductSlug() {
            return productSlug;
        }

        public void setProductSlug(String productSlug) {
            this.productSlug = productSlug;
        }

        public String getShortDesc() {
            return shortDesc;
        }

        public void setShortDesc(String shortDesc) {
            this.shortDesc = shortDesc;
        }

        public String getLongDesc() {
            return longDesc;
        }

        public void setLongDesc(String longDesc) {
            this.longDesc = longDesc;
        }

        public Object getAvailableFrom() {
            return availableFrom;
        }

        public void setAvailableFrom(Object availableFrom) {
            this.availableFrom = availableFrom;
        }

        public Object getAvailableTo() {
            return availableTo;
        }

        public void setAvailableTo(Object availableTo) {
            this.availableTo = availableTo;
        }

        public Object getDisplayType() {
            return displayType;
        }

        public void setDisplayType(Object displayType) {
            this.displayType = displayType;
        }

        public String getProductType() {
            return productType;
        }

        public void setProductType(String productType) {
            this.productType = productType;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public String getCreatedAt() {
            return createdAt;
        }

        public void setCreatedAt(String createdAt) {
            this.createdAt = createdAt;
        }

        public String getUpdatedAt() {
            return updatedAt;
        }

        public void setUpdatedAt(String updatedAt) {
            this.updatedAt = updatedAt;
        }

        public Object getDeletedAt() {
            return deletedAt;
        }

        public void setDeletedAt(Object deletedAt) {
            this.deletedAt = deletedAt;
        }

        public Object getMetaTitle() {
            return metaTitle;
        }

        public void setMetaTitle(Object metaTitle) {
            this.metaTitle = metaTitle;
        }

        public Object getMetaKeywords() {
            return metaKeywords;
        }

        public void setMetaKeywords(Object metaKeywords) {
            this.metaKeywords = metaKeywords;
        }

        public Object getMetaDescription() {
            return metaDescription;
        }

        public void setMetaDescription(Object metaDescription) {
            this.metaDescription = metaDescription;
        }

        public List<ProductsInventory> getProductsInventories() {
            return productsInventories;
        }

        public void setProductsInventories(List<ProductsInventory> productsInventories) {
            this.productsInventories = productsInventories;
        }

        public Category getCategory() {
            return category;
        }

        public void setCategory(Category category) {
            this.category = category;
        }

        public Subcategory getSubcategory() {
            return subcategory;
        }

        public void setSubcategory(Subcategory subcategory) {
            this.subcategory = subcategory;
        }

        public Childsubcategory getChildsubcategory() {
            return childsubcategory;
        }

        public void setChildsubcategory(Childsubcategory childsubcategory) {
            this.childsubcategory = childsubcategory;
        }

    }


    public class ProductImage {

        @SerializedName("id")
        @Expose
        private Integer id;
        @SerializedName("product_id")
        @Expose
        private String productId;
        @SerializedName("product_inventory_id")
        @Expose
        private String productInventoryId;
        @SerializedName("product_image")
        @Expose
        private String productImage;
        @SerializedName("is_default")
        @Expose
        private String isDefault;
        @SerializedName("created_at")
        @Expose
        private String createdAt;
        @SerializedName("updated_at")
        @Expose
        private String updatedAt;
        @SerializedName("deleted_at")
        @Expose
        private Object deletedAt;

        public Integer getId() {
            return id;
        }

        public void setId(Integer id) {
            this.id = id;
        }

        public String getProductId() {
            return productId;
        }

        public void setProductId(String productId) {
            this.productId = productId;
        }

        public String getProductInventoryId() {
            return productInventoryId;
        }

        public void setProductInventoryId(String productInventoryId) {
            this.productInventoryId = productInventoryId;
        }

        public String getProductImage() {
            return productImage;
        }

        public void setProductImage(String productImage) {
            this.productImage = productImage;
        }

        public String getIsDefault() {
            return isDefault;
        }

        public void setIsDefault(String isDefault) {
            this.isDefault = isDefault;
        }

        public String getCreatedAt() {
            return createdAt;
        }

        public void setCreatedAt(String createdAt) {
            this.createdAt = createdAt;
        }

        public String getUpdatedAt() {
            return updatedAt;
        }

        public void setUpdatedAt(String updatedAt) {
            this.updatedAt = updatedAt;
        }

        public Object getDeletedAt() {
            return deletedAt;
        }

        public void setDeletedAt(Object deletedAt) {
            this.deletedAt = deletedAt;
        }

    }


    public class Products {

        @SerializedName("current_page")
        @Expose
        private Integer currentPage;
        @SerializedName("data")
        @Expose
        private List<Datum> data = null;
        @SerializedName("first_page_url")
        @Expose
        private String firstPageUrl;
        @SerializedName("from")
        @Expose
        private Integer from;
        @SerializedName("last_page")
        @Expose
        private Integer lastPage;
        @SerializedName("last_page_url")
        @Expose
        private String lastPageUrl;
        @SerializedName("next_page_url")
        @Expose
        private Object nextPageUrl;
        @SerializedName("path")
        @Expose
        private String path;
        @SerializedName("per_page")
        @Expose
        private Integer perPage;
        @SerializedName("prev_page_url")
        @Expose
        private String prevPageUrl;
        @SerializedName("to")
        @Expose
        private Integer to;
        @SerializedName("total")
        @Expose
        private Integer total;

        public Integer getCurrentPage() {
            return currentPage;
        }

        public void setCurrentPage(Integer currentPage) {
            this.currentPage = currentPage;
        }

        public List<Datum> getData() {
            return data;
        }

        public void setData(List<Datum> data) {
            this.data = data;
        }

        public String getFirstPageUrl() {
            return firstPageUrl;
        }

        public void setFirstPageUrl(String firstPageUrl) {
            this.firstPageUrl = firstPageUrl;
        }

        public Integer getFrom() {
            return from;
        }

        public void setFrom(Integer from) {
            this.from = from;
        }

        public Integer getLastPage() {
            return lastPage;
        }

        public void setLastPage(Integer lastPage) {
            this.lastPage = lastPage;
        }

        public String getLastPageUrl() {
            return lastPageUrl;
        }

        public void setLastPageUrl(String lastPageUrl) {
            this.lastPageUrl = lastPageUrl;
        }

        public Object getNextPageUrl() {
            return nextPageUrl;
        }

        public void setNextPageUrl(Object nextPageUrl) {
            this.nextPageUrl = nextPageUrl;
        }

        public String getPath() {
            return path;
        }

        public void setPath(String path) {
            this.path = path;
        }

        public Integer getPerPage() {
            return perPage;
        }

        public void setPerPage(Integer perPage) {
            this.perPage = perPage;
        }

        public String getPrevPageUrl() {
            return prevPageUrl;
        }

        public void setPrevPageUrl(String prevPageUrl) {
            this.prevPageUrl = prevPageUrl;
        }

        public Integer getTo() {
            return to;
        }

        public void setTo(Integer to) {
            this.to = to;
        }

        public Integer getTotal() {
            return total;
        }

        public void setTotal(Integer total) {
            this.total = total;
        }

    }

    public class ProductsInventory {

        @SerializedName("id")
        @Expose
        private Integer id;
        @SerializedName("product_id")
        @Expose
        private String productId;
        @SerializedName("product_code")
        @Expose
        private String productCode;
        @SerializedName("sku")
        @Expose
        private String sku;
        @SerializedName("description")
        @Expose
        private String description;
        @SerializedName("product_qty")
        @Expose
        private String productQty;
        @SerializedName("product_price")
        @Expose
        private String productPrice;
        @SerializedName("selling_price")
        @Expose
        private String sellingPrice;
        @SerializedName("attribute_id")
        @Expose
        private Object attributeId;
        @SerializedName("attribute_value_id")
        @Expose
        private Object attributeValueId;
        @SerializedName("in_stock")
        @Expose
        private String inStock;
        @SerializedName("is_published")
        @Expose
        private String isPublished;
        @SerializedName("is_default")
        @Expose
        private Object isDefault;
        @SerializedName("created_at")
        @Expose
        private String createdAt;
        @SerializedName("updated_at")
        @Expose
        private String updatedAt;
        @SerializedName("deleted_at")
        @Expose
        private Object deletedAt;
        @SerializedName("product_images")
        @Expose
        private List<ProductImage> productImages = null;

        public Integer getId() {
            return id;
        }

        public void setId(Integer id) {
            this.id = id;
        }

        public String getProductId() {
            return productId;
        }

        public void setProductId(String productId) {
            this.productId = productId;
        }

        public String getProductCode() {
            return productCode;
        }

        public void setProductCode(String productCode) {
            this.productCode = productCode;
        }

        public String getSku() {
            return sku;
        }

        public void setSku(String sku) {
            this.sku = sku;
        }

        public String getDescription() {
            return description;
        }

        public void setDescription(String description) {
            this.description = description;
        }

        public String getProductQty() {
            return productQty;
        }

        public void setProductQty(String productQty) {
            this.productQty = productQty;
        }

        public String getProductPrice() {
            return productPrice;
        }

        public void setProductPrice(String productPrice) {
            this.productPrice = productPrice;
        }

        public String getSellingPrice() {
            return sellingPrice;
        }

        public void setSellingPrice(String sellingPrice) {
            this.sellingPrice = sellingPrice;
        }

        public Object getAttributeId() {
            return attributeId;
        }

        public void setAttributeId(Object attributeId) {
            this.attributeId = attributeId;
        }

        public Object getAttributeValueId() {
            return attributeValueId;
        }

        public void setAttributeValueId(Object attributeValueId) {
            this.attributeValueId = attributeValueId;
        }

        public String getInStock() {
            return inStock;
        }

        public void setInStock(String inStock) {
            this.inStock = inStock;
        }

        public String getIsPublished() {
            return isPublished;
        }

        public void setIsPublished(String isPublished) {
            this.isPublished = isPublished;
        }

        public Object getIsDefault() {
            return isDefault;
        }

        public void setIsDefault(Object isDefault) {
            this.isDefault = isDefault;
        }

        public String getCreatedAt() {
            return createdAt;
        }

        public void setCreatedAt(String createdAt) {
            this.createdAt = createdAt;
        }

        public String getUpdatedAt() {
            return updatedAt;
        }

        public void setUpdatedAt(String updatedAt) {
            this.updatedAt = updatedAt;
        }

        public Object getDeletedAt() {
            return deletedAt;
        }

        public void setDeletedAt(Object deletedAt) {
            this.deletedAt = deletedAt;
        }

        public List<ProductImage> getProductImages() {
            return productImages;
        }

        public void setProductImages(List<ProductImage> productImages) {
            this.productImages = productImages;
        }

    }

    public class Subcategory {

        @SerializedName("id")
        @Expose
        private Integer id;
        @SerializedName("brand_id")
        @Expose
        private String brandId;
        @SerializedName("cat_id")
        @Expose
        private String catId;
        @SerializedName("subcategory_name")
        @Expose
        private String subcategoryName;
        @SerializedName("subcategory_image")
        @Expose
        private String subcategoryImage;
        @SerializedName("isfeatured")
        @Expose
        private String isfeatured;
        @SerializedName("status")
        @Expose
        private String status;
        @SerializedName("created_at")
        @Expose
        private String createdAt;
        @SerializedName("updated_at")
        @Expose
        private String updatedAt;
        @SerializedName("deleted_at")
        @Expose
        private Object deletedAt;

        public Integer getId() {
            return id;
        }

        public void setId(Integer id) {
            this.id = id;
        }

        public String getBrandId() {
            return brandId;
        }

        public void setBrandId(String brandId) {
            this.brandId = brandId;
        }

        public String getCatId() {
            return catId;
        }

        public void setCatId(String catId) {
            this.catId = catId;
        }

        public String getSubcategoryName() {
            return subcategoryName;
        }

        public void setSubcategoryName(String subcategoryName) {
            this.subcategoryName = subcategoryName;
        }

        public String getSubcategoryImage() {
            return subcategoryImage;
        }

        public void setSubcategoryImage(String subcategoryImage) {
            this.subcategoryImage = subcategoryImage;
        }

        public String getIsfeatured() {
            return isfeatured;
        }

        public void setIsfeatured(String isfeatured) {
            this.isfeatured = isfeatured;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public String getCreatedAt() {
            return createdAt;
        }

        public void setCreatedAt(String createdAt) {
            this.createdAt = createdAt;
        }

        public String getUpdatedAt() {
            return updatedAt;
        }

        public void setUpdatedAt(String updatedAt) {
            this.updatedAt = updatedAt;
        }

        public Object getDeletedAt() {
            return deletedAt;
        }

        public void setDeletedAt(Object deletedAt) {
            this.deletedAt = deletedAt;
        }

    }

}
