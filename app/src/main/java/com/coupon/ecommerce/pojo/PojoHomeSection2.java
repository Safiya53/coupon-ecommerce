package com.coupon.ecommerce.pojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class PojoHomeSection2 {

    @SerializedName("status")
    @Expose
    private Boolean status;
    @SerializedName("data")
    @Expose
    private Data data;

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }
    public class Data {

        @SerializedName("top_categories")
        @Expose
        private List<TopCategory> topCategories = null;

        @SerializedName("special_offer_images")
        @Expose
        private List<SpecialOfferImage> specialOfferImages = null;

        public List<TopCategory> getTopCategories() {
            return topCategories;
        }

        public void setTopCategories(List<TopCategory> topCategories) {
            this.topCategories = topCategories;
        }


        public List<SpecialOfferImage> getSpecialOfferImages() {
            return specialOfferImages;
        }

        public void setSpecialOfferImages(List<SpecialOfferImage> specialOfferImages) {
            this.specialOfferImages = specialOfferImages;
        }

    }



    public class SpecialOfferImage {

        @SerializedName("id")
        @Expose
        private Integer id;
        @SerializedName("offer_image")
        @Expose
        private String offerImage;

        public Integer getId() {
            return id;
        }

        public void setId(Integer id) {
            this.id = id;
        }

        public String getOfferImage() {
            return offerImage;
        }

        public void setOfferImage(String offerImage) {
            this.offerImage = offerImage;
        }

    }


    public class TopCategory {

        @SerializedName("id")
        @Expose
        private Integer id;
        @SerializedName("category_image")
        @Expose
        private String categoryImage;
        @SerializedName("category_name")
        @Expose
        private String categoryName;

        public Integer getId() {
            return id;
        }

        public void setId(Integer id) {
            this.id = id;
        }

        public String getCategoryImage() {
            return categoryImage;
        }

        public void setCategoryImage(String categoryImage) {
            this.categoryImage = categoryImage;
        }

        public String getCategoryName() {
            return categoryName;
        }

        public void setCategoryName(String categoryName) {
            this.categoryName = categoryName;
        }

    }



}
