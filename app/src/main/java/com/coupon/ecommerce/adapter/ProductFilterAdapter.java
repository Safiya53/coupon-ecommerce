package com.coupon.ecommerce.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ImageView;
import android.widget.TextView;


import com.coupon.ecommerce.R;
import com.coupon.ecommerce.pojo.PojoHomeShopCategory;

import java.util.List;



public class ProductFilterAdapter extends BaseExpandableListAdapter {

    private Context context;


    //List<String[]> data;

    // String[] headers;

    CustomClick customClick;

    List<PojoHomeShopCategory.Subcategory> mArrayData;

    public ProductFilterAdapter(Context context, List<PojoHomeShopCategory.Subcategory> mArrayData, CustomClick customClick) {
        this.context = context;
        this.mArrayData = mArrayData;
        this.customClick = customClick;
    }

    @Override
    public Object getGroup(int groupPosition) {

        return mArrayData.get(groupPosition);
    }

    @Override
    public int getGroupCount() {

        return mArrayData.size();
    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {


        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        convertView = inflater.inflate(R.layout.row_second, null);
        TextView text = (TextView) convertView.findViewById(R.id.rowSecondText);
        String groupText = getGroup(groupPosition).toString();
        text.setText(mArrayData.get(groupPosition).getSubcategoryName());

        ImageView expImg = (ImageView) convertView.findViewById(R.id.expImg);
        ImageView colImg = (ImageView) convertView.findViewById(R.id.colImg);

        if (isExpanded) {
            expImg.setVisibility(View.GONE);
            colImg.setVisibility(View.VISIBLE);
        } else {

            expImg.setVisibility(View.VISIBLE);
            colImg.setVisibility(View.GONE);
        }

        return convertView;
    }

    @Override
    public Object getChild(int groupPosition, int childPosition) {


        return mArrayData.get(groupPosition).getChilcategories();
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }

    @Override
    public View getChildView(final int groupPosition, final int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {


        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        convertView = inflater.inflate(R.layout.row_third, null);
        TextView textView = (TextView) convertView.findViewById(R.id.rowThirdText);
        convertView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                customClick.itemClick(mArrayData.get(groupPosition).getId(),
                        mArrayData.get(groupPosition).getChilcategories().get(childPosition).getId());
            }
        });
        String text = mArrayData.get(groupPosition).getChilcategories().get(childPosition).getChildCategoryName();


        textView.setText(text);

        return convertView;
    }


    @Override
    public int getChildrenCount(int groupPosition) {


        return mArrayData.get(groupPosition).getChilcategories().size();
    }

    @Override
    public boolean hasStableIds() {
        return true;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return true;
    }

    public interface CustomClick {

        void itemClick(int catId, int subCatId);

    }
}