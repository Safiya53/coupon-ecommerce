package com.coupon.ecommerce.adapter;

import android.content.Context;
import android.os.Build;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.RequiresApi;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.coupon.ecommerce.R;
import com.coupon.ecommerce.pojo.PojoHomeSection2;

import java.util.ArrayList;
import java.util.List;



/**
 * Created by Rp on 6/14/2016.
 */
public class HomeOurCategoryAdapter extends RecyclerView.Adapter<HomeOurCategoryAdapter.MyViewHolder> {
    Context context;


    CustomClick listner;

    private List<PojoHomeSection2.TopCategory> mArrayProductList = new ArrayList<>();
    Context mContext;
    boolean check = false;

    public class MyViewHolder extends RecyclerView.ViewHolder {


        TextView tvName;
        ImageView ivproductImage;

        public MyViewHolder(View view) {
            super(view);

            ivproductImage = view.findViewById(R.id.imageview_grid_row_product_image);
            tvName = view.findViewById(R.id.textview_grid_row_product_name);
        }

    }

    public HomeOurCategoryAdapter(Context context, List<PojoHomeSection2.TopCategory> mArrayProductList, CustomClick listner) {
        this.mContext = context;
        this.mArrayProductList = mArrayProductList;
        this.listner = listner;
    }


    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_home_our_category, parent, false);


        return new MyViewHolder(itemView);

    }

    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {

        holder.tvName.setText(mArrayProductList.get(position).getCategoryName());

        if (mArrayProductList.get(position).getCategoryImage() != null) {
            if (!mArrayProductList.get(position).getCategoryImage().equals("")) {
                Glide.with(mContext).load(mArrayProductList.get(position).getCategoryImage()).into(holder.ivproductImage);
            }
        }

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                listner.itemClick(mArrayProductList.get(position).getId());

            }
        });
    }

    @Override
    public int getItemCount() {
        return mArrayProductList.size();
    }

    public interface CustomClick {

        void itemClick(int id);

    }

}


