package com.coupon.ecommerce.adapter;

import android.content.Context;
import android.os.Build;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;


import androidx.annotation.RequiresApi;
import androidx.recyclerview.widget.RecyclerView;

import com.coupon.ecommerce.R;
import com.coupon.ecommerce.pojo.PojoWishlist;

import java.util.ArrayList;
import java.util.List;



/**
 * Created by Rp on 6/14/2016.
 */
public class WishListAdapter extends RecyclerView.Adapter<WishListAdapter.MyViewHolder> {
    Context context;


    CustomClick listner;

    private List<PojoWishlist.Datum> mArrayProductList = new ArrayList<>();
    Context mContext;
    boolean check = false;

    public class MyViewHolder extends RecyclerView.ViewHolder {


        TextView tvName, tvPrice, tvSize, tvAddToCart, tvRemove, tvPlus, tvMinus, tvQty;
        ImageView ivproductImage;

        public MyViewHolder(View view) {
            super(view);

            ivproductImage = view.findViewById(R.id.ivproductImage);
            tvSize = view.findViewById(R.id.tvSize);
            tvName = view.findViewById(R.id.tvName);
            tvPrice = view.findViewById(R.id.tvPrice);
            tvPlus = view.findViewById(R.id.textview_product_detail_plus);
            tvQty = view.findViewById(R.id.textview_product_detail_quantity);
            tvMinus = view.findViewById(R.id.textview_product_detail_minus);
            tvAddToCart = view.findViewById(R.id.tvAddToCart);
            tvRemove = view.findViewById(R.id.tvRemove);
        }

    }

    public WishListAdapter(Context context, List<PojoWishlist.Datum> mArrayProductList, CustomClick listner) {
        this.mContext = context;
        this.mArrayProductList = mArrayProductList;
        this.listner = listner;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_my_wishlist, parent, false);


        return new MyViewHolder(itemView);

    }

    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {



        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                listner.itemClick(position);

            }
        });
    }

    @Override
    public int getItemCount() {
        return mArrayProductList.size();
    }

    public interface CustomClick {

        void itemClick(int id);

    }

}


