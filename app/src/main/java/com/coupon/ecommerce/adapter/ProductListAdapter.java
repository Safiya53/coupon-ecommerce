package com.coupon.ecommerce.adapter;

import android.content.Context;
import android.graphics.Paint;
import android.os.Build;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.RequiresApi;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.coupon.ecommerce.R;
import com.coupon.ecommerce.constants.ConstantCodes;
import com.coupon.ecommerce.pojo.PojoProductList;

import java.util.ArrayList;
import java.util.List;



/**
 * Created by Rp on 6/14/2016.
 */
public class ProductListAdapter extends RecyclerView.Adapter<ProductListAdapter.MyViewHolder> {

    CustomClick listner;

    private List<PojoProductList.Datum> mArrayProductList = new ArrayList<>();
    Context mContext;
    boolean check = false;

    public class MyViewHolder extends RecyclerView.ViewHolder {


        TextView tvName, tvPrice, tvPriceStrike, tvAddToCart;
        ImageView ivproductImage, ivWishlist;

        public MyViewHolder(View view) {
            super(view);

            ivproductImage = view.findViewById(R.id.imageview_grid_row_product_image);
            ivWishlist = view.findViewById(R.id.imageview_grid_row_wishlist);
            tvName = view.findViewById(R.id.textview_grid_row_product_name);
            tvPrice = view.findViewById(R.id.textview_grid_row_product_price);
            tvPriceStrike = view.findViewById(R.id.textview_grid_row_product_price_strike);
            tvAddToCart = view.findViewById(R.id.tvAddToCart);
        }

    }

    public ProductListAdapter(Context context, List<PojoProductList.Datum> mArrayProductList, CustomClick listner) {
        this.mContext = context;
        this.mArrayProductList = mArrayProductList;
        this.listner = listner;
    }


    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_product_list, parent, false);


        return new MyViewHolder(itemView);

    }

    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {

        holder.tvName.setText(mArrayProductList.get(position).getProductName());

        if (mArrayProductList.get(position).getProductsInventories() != null) {
            if (mArrayProductList.get(position).getProductsInventories().size() > 0) {

                if (mArrayProductList.get(position).getProductsInventories().size() == 1) {

                    holder.tvPrice.setText("\u20B9 " + mArrayProductList.get(position).getProductsInventories().get(0).getSellingPrice());
                    holder.tvPriceStrike.setText("\u20B9 " + mArrayProductList.get(position).getProductsInventories().get(0).getProductPrice());
                    holder.tvPriceStrike.setPaintFlags(holder.tvPriceStrike.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);

                    if (mArrayProductList.get(position).getProductsInventories().get(0).getProductImages() != null) {
                        if (mArrayProductList.get(position).getProductsInventories().get(0).getProductImages().size() > 0) {
                            if (mArrayProductList.get(position).getProductsInventories().get(0).getProductImages().size() == 1) {
                                if (!mArrayProductList.get(position).getProductsInventories().get(0).getProductImages().get(0).getProductImage().equals("")) {
                                    Glide.with(mContext).load(ConstantCodes.BASE_URL_PRODUCT_IMAGE + mArrayProductList.get(position).getProductsInventories().get(0).getProductImages().get(0).getProductImage()).into(holder.ivproductImage);
                                }

                            } else {

                                for (int j = 0; j < mArrayProductList.get(position).getProductsInventories().get(0).getProductImages().size(); j++) {
                                    if (mArrayProductList.get(position).getProductsInventories().get(0).getProductImages().get(j).getIsDefault() != null) {
                                        if (mArrayProductList.get(position).getProductsInventories().get(0).getProductImages().get(j).getIsDefault().equals("1")) {
                                            if (!mArrayProductList.get(position).getProductsInventories().get(0).getProductImages().get(j).getProductImage().equals("")) {
                                                Glide.with(mContext).load(ConstantCodes.BASE_URL_PRODUCT_IMAGE + mArrayProductList.get(position).getProductsInventories().get(0).getProductImages().get(j).getProductImage()).into(holder.ivproductImage);
                                            }
                                        }
                                    }
                                }

                            }

                        }
                    }

                } else {
                    for (int i = 0; i < mArrayProductList.get(position).getProductsInventories().size(); i++) {

                        if (mArrayProductList.get(position).getProductsInventories().get(i).getIsDefault() != null) {
                            if (mArrayProductList.get(position).getProductsInventories().get(i).getIsDefault().equals("1")) {

                                for (int j = 0; j < mArrayProductList.get(position).getProductsInventories().get(i).getProductImages().size(); j++) {
                                    if (mArrayProductList.get(position).getProductsInventories().get(i).getProductImages().get(j).getIsDefault() != null) {
                                        if (mArrayProductList.get(position).getProductsInventories().get(i).getProductImages().get(j).getIsDefault().equals("1")) {
                                            if (!mArrayProductList.get(position).getProductsInventories().get(i).getProductImages().get(j).getProductImage().equals("")) {
                                                Glide.with(mContext).load(ConstantCodes.BASE_URL_PRODUCT_IMAGE + mArrayProductList.get(position).getProductsInventories().get(i).getProductImages().get(j).getProductImage()).into(holder.ivproductImage);
                                            }
                                        }
                                    }
                                }

                                holder.tvPrice.setText("\u20B9 " + mArrayProductList.get(position).getProductsInventories().get(i).getSellingPrice());
                                holder.tvPriceStrike.setText("\u20B9 " + mArrayProductList.get(position).getProductsInventories().get(i).getProductPrice());
                                holder.tvPriceStrike.setPaintFlags(holder.tvPriceStrike.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
                            }
                        }


                    }
                }


            }
        }


        holder.ivWishlist.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (check) {

                    holder.ivWishlist.setImageResource(R.drawable.unlike);
                    check = false;


                } else {
                    holder.ivWishlist.setImageResource(R.drawable.like);
                    check = true;
                }
            }
        });

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                listner.itemClick(position);

            }
        });

        holder.tvAddToCart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String code = "", price = "";

                if (mArrayProductList.get(position).getProductsInventories() != null) {
                    if (mArrayProductList.get(position).getProductsInventories().size() > 0) {

                        if (mArrayProductList.get(position).getProductsInventories().size() == 1) {
                            code = mArrayProductList.get(position).getProductsInventories().get(0).getProductCode();
                            price = mArrayProductList.get(position).getProductsInventories().get(0).getSellingPrice();
                        } else {

                            for (int i = 0; i < mArrayProductList.get(position).getProductsInventories().size(); i++) {

                                if (mArrayProductList.get(position).getProductsInventories().get(i).getIsDefault() != null) {
                                    if (mArrayProductList.get(position).getProductsInventories().get(i).getIsDefault().equals("1")) {
                                        code = mArrayProductList.get(position).getProductsInventories().get(i).getProductCode();
                                        price = mArrayProductList.get(position).getProductsInventories().get(i).getSellingPrice();
                                    }
                                }

                            }

                        }
                    }
                }

                listner.cartClick(code, price, mArrayProductList.get(position).

                        getId() + "");


            }
        });
    }

    @Override
    public int getItemCount() {
        return mArrayProductList.size();
    }

    public interface CustomClick {

        void itemClick(int id);

        void cartClick(String code, String price, String id);

    }

}


