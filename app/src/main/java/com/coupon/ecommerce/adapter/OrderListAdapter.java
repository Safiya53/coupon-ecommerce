package com.coupon.ecommerce.adapter;

import android.content.Context;
import android.os.Build;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;


import androidx.annotation.RequiresApi;
import androidx.recyclerview.widget.RecyclerView;

import com.coupon.ecommerce.R;
import com.coupon.ecommerce.pojo.PojoMyOrder;

import java.util.ArrayList;
import java.util.List;



/**
 * Created by Rp on 6/14/2016.
 */
public class OrderListAdapter extends RecyclerView.Adapter<OrderListAdapter.MyViewHolder> {
    Context context;


    CustomClick listner;

    private List<PojoMyOrder.Datum> mArrayProductList = new ArrayList<>();
    Context mContext;
    boolean check = false;

    public class MyViewHolder extends RecyclerView.ViewHolder {


        TextView tvOrderedOn, tvOrderedStatus, tvDeliveryStatus, tvPaymentStatus, tvShippingCharges, tvPrice;

        public MyViewHolder(View view) {
            super(view);

            tvOrderedOn = view.findViewById(R.id.tvOrderedOn);
            tvOrderedStatus = view.findViewById(R.id.tvOrderedStatus);
            tvDeliveryStatus = view.findViewById(R.id.tvDeliveryStatus);
            tvOrderedOn = view.findViewById(R.id.tvOrderedOn);
            tvPaymentStatus = view.findViewById(R.id.tvPaymentStatus);
            tvShippingCharges = view.findViewById(R.id.tvShippingCharges);
            tvPrice = view.findViewById(R.id.tvPrice);

        }

    }

    public OrderListAdapter(Context context, List<PojoMyOrder.Datum> mArrayProductList, CustomClick listner) {
        this.mContext = context;
        this.mArrayProductList = mArrayProductList;
        this.listner = listner;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_my_orders, parent, false);


        return new MyViewHolder(itemView);

    }

    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {

        holder.tvPrice.setText("\u20B9 " + mArrayProductList.get(position).getAmount());
        holder.tvOrderedOn.setText("" + mArrayProductList.get(position).getCreatedAt().substring(0, mArrayProductList.get(position).getCreatedAt().indexOf(" ")));

        if (mArrayProductList.get(position).getOrderStatus().equals("1")) {
            holder.tvOrderedStatus.setText("Pending");
        } else if (mArrayProductList.get(position).getOrderStatus().equals("2")) {
            holder.tvOrderedStatus.setText("Processing");
        } else if (mArrayProductList.get(position).getOrderStatus().equals("3")) {
            holder.tvOrderedStatus.setText("In Transit");
        } else if (mArrayProductList.get(position).getOrderStatus().equals("4")) {
            holder.tvOrderedStatus.setText("Delivery");
        } else if (mArrayProductList.get(position).getOrderStatus().equals("5")) {
            holder.tvOrderedStatus.setText("Failed");
        }

        //holder.tvDeliveryStatus.setText("" +);
        holder.tvPaymentStatus.setText("" + mArrayProductList.get(position).getPaymentStatus());
        holder.tvShippingCharges.setText("\u20B9 " + mArrayProductList.get(position).getShipping());


        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                listner.itemClick(position);

            }
        });
    }

    @Override
    public int getItemCount() {
        return mArrayProductList.size();
    }

    public interface CustomClick {

        void itemClick(int id);

    }

}


