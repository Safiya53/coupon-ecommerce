package com.coupon.ecommerce.adapter;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;


import androidx.recyclerview.widget.RecyclerView;

import com.coupon.ecommerce.R;
import com.coupon.ecommerce.database.DatabaseHandler;
import com.coupon.ecommerce.pojo.CartModel;

import java.util.List;


public class CartRecyclerviewAdapter extends RecyclerView.Adapter<CartRecyclerviewAdapter.ViewHolder> {

    private List<CartModel> cartModelList;
    private Context mContext;
    private RecyclerView mRecyclerV;
    DatabaseHandler db;
    ItemClickListener itemClickListener;


    public class ViewHolder extends RecyclerView.ViewHolder {
        // each data item is just a string in this case
        public ImageView productImage;

        public TextView product_name;
        public TextView product_brand;
        public TextView product_code;
        public TextView product_price;
        public TextView product_quantity;
        public TextView product_total_price;


        public LinearLayout remove_from_cart;


        public View layout;

        public ViewHolder(View v) {
            super(v);
            layout = v;
            productImage = (ImageView) v.findViewById(R.id.image);

            product_name = (TextView) v.findViewById(R.id.name);
            product_brand = (TextView) v.findViewById(R.id.brand);
            product_code = (TextView) v.findViewById(R.id.code);
            product_price = (TextView) v.findViewById(R.id.price);
            product_total_price = (TextView) v.findViewById(R.id.total);
            product_quantity = (TextView) v.findViewById(R.id.quantity);

            remove_from_cart = v.findViewById(R.id.llDelete);
        }

    }

    // Provide a suitable constructor (depends on the kind of dataset)
    public CartRecyclerviewAdapter(List<CartModel> myDataset, Context context, RecyclerView recyclerView, ItemClickListener itemClickListener) {
        cartModelList = myDataset;
        mContext = context;
        mRecyclerV = recyclerView;
        this.itemClickListener = itemClickListener;
    }

    // Create new views (invoked by the layout manager)
    @Override
    public CartRecyclerviewAdapter.ViewHolder onCreateViewHolder(ViewGroup parent,
                                                                 int viewType) {
        // create a new view
        LayoutInflater inflater = LayoutInflater.from(
                parent.getContext());
        View v =
                inflater.inflate(R.layout.list_cart_detail, parent, false);
        // set the view's size, margins, paddings and layout parameters
        ViewHolder vh = new ViewHolder(v);
        return vh;
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        // - get element from your dataset at this position
        // - replace the contents of the view with that element

        final CartModel cartModel = cartModelList.get(position);
        holder.product_name.setText(cartModel.getProduct_code());
        holder.product_price.setText(cartModel.getProduct_price());
        holder.product_quantity.setText("Qty " + cartModel.getProduct_quantity());

        int price, quantity;
        String priceSub, qtySub;
        priceSub = cartModel.getProduct_price();
        price = Integer.parseInt(priceSub);

        qtySub = String.valueOf(cartModel.getProduct_quantity());
        Log.e("qty", " 6" + qtySub);
        quantity = Integer.parseInt(qtySub);
        int total = price * quantity;
        holder.product_total_price.setText("Total Rs." + total);

        /*holder.remove_from_cart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                db = new DatabaseHandler(mContext);
                CartModel cartModel1 = db.getCart(cartModel.getProduct_name(), cartModel.getProduct_brand());
                db.deleteCart(cartModel1.getBrand_id(), cartModel1.getProduct_id());
                notifyItemRemoved(position);
                Toast.makeText(mContext, "Item removed from cart", Toast.LENGTH_SHORT).show();
                cartModelList.remove(position);
                notifyDataSetChanged();

                int totalPrice = 0;
                for (int i = 0; i < cartModelList.size(); i++) {
                    totalPrice += Integer.parseInt(cartModelList.get(i).getProduct_price().substring(4));
                }
                SharedPreferences mSharedPreference = PreferenceManager.getDefaultSharedPreferences(mContext);
                mSharedPreference.getInt("totalPrice", totalPrice);

            }
        });*/

        holder.remove_from_cart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                itemClickListener.onItemClicked(position);
            }
        });

    }

    public interface ItemClickListener {

        void onItemClicked(int pos);
    }

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return cartModelList.size();
    }

}
