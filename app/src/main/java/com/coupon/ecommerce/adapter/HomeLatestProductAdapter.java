package com.coupon.ecommerce.adapter;

import android.content.Context;
import android.graphics.Paint;
import android.os.Build;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.RequiresApi;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.coupon.ecommerce.R;
import com.coupon.ecommerce.pojo.PojoHomeSection1;

import java.util.ArrayList;
import java.util.List;



/**
 * Created by Rp on 6/14/2016.
 */
public class HomeLatestProductAdapter extends RecyclerView.Adapter<HomeLatestProductAdapter.MyViewHolder> {
    Context context;


    CustomClick listner;

    private List<PojoHomeSection1.LatestProduct> mArrayProductList = new ArrayList<>();
    Context mContext;
    boolean check = false;

    public class MyViewHolder extends RecyclerView.ViewHolder {


        TextView tvName, tvPrice, tvPriceStrike, tvAddToCart;
        ImageView ivproductImage, ivWishlist;

        public MyViewHolder(View view) {
            super(view);

            ivproductImage = view.findViewById(R.id.imageview_grid_row_product_image);
            ivWishlist = view.findViewById(R.id.imageview_grid_row_wishlist);
            tvName = view.findViewById(R.id.textview_grid_row_product_name);
            tvPrice = view.findViewById(R.id.textview_grid_row_product_price);
            tvPriceStrike = view.findViewById(R.id.textview_grid_row_product_price_strike);
            tvAddToCart = view.findViewById(R.id.tvAddToCart);
        }

    }

    public HomeLatestProductAdapter(Context context, List<PojoHomeSection1.LatestProduct> mArrayProductList, CustomClick listner) {
        this.mContext = context;
        this.mArrayProductList = mArrayProductList;
        this.listner = listner;
    }


    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_home_latest_product, parent, false);


        return new MyViewHolder(itemView);

    }

    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {

        holder.tvName.setText(mArrayProductList.get(position).getName());
        holder.tvPrice.setText("\u20B9 " + mArrayProductList.get(position).getSellingPrice());
        holder.tvPriceStrike.setText("\u20B9" + mArrayProductList.get(position).getProductPrice());
        holder.tvPriceStrike.setPaintFlags(holder.tvPriceStrike.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);

        if (!mArrayProductList.get(position).getProductImage().equals("")) {
            Glide.with(mContext).load(mArrayProductList.get(position).getProductImage()).into(holder.ivproductImage);
        }

        holder.ivWishlist.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (check) {

                    holder.ivWishlist.setImageResource(R.drawable.unlike);
                    check = false;


                } else {
                    holder.ivWishlist.setImageResource(R.drawable.like);
                    check = true;
                }
            }
        });

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                listner.itemClick(mArrayProductList.get(position).getId() + "", 0);

            }
        });

        holder.tvAddToCart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listner.itemClick(mArrayProductList.get(position).getId() + "", 1);
            }
        });
    }

    @Override
    public int getItemCount() {
        return mArrayProductList.size();
    }

    public interface CustomClick {

        void itemClick(String productId, int type); //type = 0 product details and type= 1 cart add

    }

}


