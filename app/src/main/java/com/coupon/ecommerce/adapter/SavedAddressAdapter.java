package com.coupon.ecommerce.adapter;

import android.content.Context;
import android.os.Build;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;


import androidx.annotation.RequiresApi;
import androidx.recyclerview.widget.RecyclerView;

import com.coupon.ecommerce.R;
import com.coupon.ecommerce.pojo.PojoGetUserAddress;

import java.util.ArrayList;
import java.util.List;



/**
 * Created by Rp on 6/14/2016.
 */
public class SavedAddressAdapter extends RecyclerView.Adapter<SavedAddressAdapter.MyViewHolder> {
    Context context;


    CustomClick listner;

    private List<PojoGetUserAddress.Datum> mArrayProductList = new ArrayList<>();
    Context mContext;
    boolean check = false;

    public class MyViewHolder extends RecyclerView.ViewHolder {


        TextView tvTitle, tvAddress;

        public MyViewHolder(View view) {
            super(view);

            tvTitle = view.findViewById(R.id.tvTitle);
            tvAddress = view.findViewById(R.id.tvAddress);

        }

    }

    public SavedAddressAdapter(Context context, List<PojoGetUserAddress.Datum> mArrayProductList, CustomClick listner) {
        this.mContext = context;
        this.mArrayProductList = mArrayProductList;
        this.listner = listner;
    }


    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_save_address, parent, false);


        return new MyViewHolder(itemView);

    }

    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {

        holder.tvTitle.setText(mArrayProductList.get(position).getTitle());
        holder.tvAddress.setText("" + mArrayProductList.get(position).getAddress() + ", " +
                mArrayProductList.get(position).getLandmark() + ", " + mArrayProductList.get(position).getCityName() + "-"
                + mArrayProductList.get(position).getZipcode());


        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                listner.itemClick(position);

            }
        });
    }

    @Override
    public int getItemCount() {
        return mArrayProductList.size();
    }

    public interface CustomClick {

        void itemClick(int id);

    }

}


