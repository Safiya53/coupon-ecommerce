package com.coupon.ecommerce.adapter;

import android.content.Context;
import android.os.Bundle;

import androidx.annotation.IntRange;
import androidx.annotation.NonNull;
import androidx.fragment.app.FragmentManager;

import com.coupon.ecommerce.fragment.Step1ShoppingCartFragment;
import com.coupon.ecommerce.fragment.Step2DeliveryMethodFragment;
import com.coupon.ecommerce.fragment.Step3PaymentMethodFragment;
import com.coupon.ecommerce.fragment.Step4ConfirmationFragment;
import com.stepstone.stepper.Step;
import com.stepstone.stepper.adapter.AbstractFragmentStepAdapter;
import com.stepstone.stepper.viewmodel.StepViewModel;



public class MyCheckoutStepperAdapter extends AbstractFragmentStepAdapter {

    public MyCheckoutStepperAdapter(FragmentManager fm, Context context) {
        super(fm, context);
    }

    @Override
    public Step createStep(int position) {

        if (position == 0) {
            final Step1ShoppingCartFragment step = new Step1ShoppingCartFragment();
            Bundle b = new Bundle();
            b.putInt("1", position);
            step.setArguments(b);
            return step;

        } else if (position == 1) {
            final Step2DeliveryMethodFragment step = new Step2DeliveryMethodFragment();
            Bundle b = new Bundle();
            b.putInt("1", position);
            step.setArguments(b);
            return step;

        } else if (position == 2) {
            final Step3PaymentMethodFragment step = new Step3PaymentMethodFragment();
            Bundle b = new Bundle();
            b.putInt("1", position);
            step.setArguments(b);
            return step;

        } else if (position == 3) {
            final Step4ConfirmationFragment step = new Step4ConfirmationFragment();
            Bundle b = new Bundle();
            b.putInt("1", position);
            step.setArguments(b);
            return step;
        } else {
            return null;
        }
    }

    @Override
    public int getCount() {
        return 4;
    }

    @NonNull
    @Override
    public StepViewModel getViewModel(@IntRange(from = 0) int position) {
        //Override this method to set Step title for the Tabs, not necessary for other stepper types

        if (position == 0) {
            return new StepViewModel.Builder(context)
                    .setTitle("Shopping Cart") //can be a CharSequence instead
                    .create();
        } else if (position == 1) {
            return new StepViewModel.Builder(context)
                    .setTitle("Delivery Methods") //can be a CharSequence instead
                    .create();
        } else if (position == 2) {
            return new StepViewModel.Builder(context)
                    .setTitle("Payment Methods") //can be a CharSequence instead
                    .create();
        } else if (position == 3) {
            return new StepViewModel.Builder(context)
                    .setTitle("Confirmation") //can be a CharSequence instead
                    .create();
        } else {
            return null;
        }
    }

}
