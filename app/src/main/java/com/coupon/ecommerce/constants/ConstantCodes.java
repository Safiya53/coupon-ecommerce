package com.coupon.ecommerce.constants;

/**
 * Created by smit_shah on 04-Apr-17.
 * <p>
 * Constants for the ws.wolfsoft.propertyplanetapp.application
 */

public class ConstantCodes {

    public static final int TIMEOUT_SECONDS = 60;

    public static final String BASE_URL = "http://gravitymegamarkets.com/public/api/";
    public static final String BASE_URL_PRODUCT_IMAGE = "http://gravitymegamarkets.com/public/uploads/";

    public static final String API_ACCESS_TOKEN = "";

    public static final String METHOD_SHOP_BY_CATEGORY = "home/menu";
    public static final String METHOD_PRODUCT_LIST = "getProduct";
    public static final String METHOD_PRODUCT_DETAIL = "product_detail";
    public static final String METHOD_HOME_SECTION1 = "landing/data/1";
    public static final String METHOD_HOME_SECTION2 = "landing/data/2";
    public static final String METHOD_HOME_SECTION3 = "landing/data/3";
    public static final String METHOD_LOGIN = "login";
    public static final String METHOD_REGISTER_OTP = "register";
    public static final String METHOD_USER_REGISTER = "verify-register";
    public static final String METHOD_GOOGLE_LOGIN = "social-login/google";
    public static final String METHOD_FACEBOOK_LOGIN = "social-login/facebook";
    public static final String METHOD_ADD_TO_WISHLIST = "addToWishList";
    public static final String METHOD_REMOVE_TO_WISHLIST = "removeFromWishList";
    public static final String METHOD_CHECK_PROMO_CODE = "checkPromoCode";
    public static final String METHOD_GET_ADDRESS = "getUserAddress";
    public static final String METHOD_SAVE_ADDRESS = "saveUserAddress";
    public static final String METHOD_GET_CITY = "getCityList";
    public static final String METHOD_WISHLIST = "wishlist";
    public static final String METHOD_NEWSLETTER_SUBSCRIPTION = "subscribe";
    public static final String METHOD_MY_ORDER = "myOrder";
    public static final String METHOD_BRAND_WISE_PRODUCT = "getProductBrandWise";
    public static final String METHOD_ORDER_SUBMIT = "processOrder";
    public static final String METHOD_PAYTM_GENERATE_CHECKSUM = "paytm_genrateHash";

    public static final String SIGN_IN_WITH_GOOGLE = "sign_in_google";
    public static final String SIGN_IN_WITH_FACEBOOK = "sign_in_facebook";

    public static final String IS_LOGIN = "is_login";
    public static final String LOGIN_USER_ID = "login_user_id";
    public static final String LOGIN_USER_NAME = "login_user_name";
    public static final String LOGIN_USER_EMAIL = "login_user_email";
    public static final String LOGIN_USER_PHONE = "login_user_phone";

    public static final String ORDER_SUB_TOTAL = "order_subTotal";
    public static final String ORDER_GRAND_TOTAL = "order_grandTotal";
    public static final String ORDER_DISCOUNT = "order_discount";
    public static final String ORDER_PROMO_CODE = "order_promocodeId";
    public static final String ORDER_ADDRESS = "order_address";

}
